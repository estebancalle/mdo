-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-09-2018 a las 19:46:59
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mdo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `idarticulo` int(11) NOT NULL,
  `nombre_articulo` varchar(200) NOT NULL,
  `descripcion_articulo` varchar(10000) NOT NULL,
  `proceso_idProceso` int(11) NOT NULL,
  `imagen_idimagen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idarticulo`, `nombre_articulo`, `descripcion_articulo`, `proceso_idProceso`, `imagen_idimagen`) VALUES
(24, 'Paso 1.)Ejecución desde portal CRM', 'Para realizar la ejecución vamos al portal CRM staff.tigo.com.co/CRM/Portal/auth/portal/default/Postventa y nos ubicamos en Billing Adjustment y vamos Ajustes Pospago luego Aplicación de Ajustes, aparece un botón aplicar ajustes (solo se ejecuta una sola vez).', 102, 81),
(25, 'Paso 2.)Validación de correo', 'Cuando inicia el proceso se busca un correo con el siguiente asunto: “Inicio Aplicación de Ajustes” se pega el pantallazo del inicio y el monitoreo de los ajustes y se envía a Edilson y Soporte App1', 102, 82),
(26, 'Paso 3.) Monitoreo', 'Para monitorear el proceso vamos a la siguiente dirección: 10.69.32.157:8080/Operaciones/priv/frames.jsf luego de ingresar vamos Operaciones > CRM > Ajustes.\r\nPara desbloquear el proceso vamos a Actualización de Ajustes > vamos a desbloquear.', 102, 83),
(27, 'En caso de error...', 'En caso de lentitud o problemas se escala al soporte CRM y al operador cedritos es la 1229; cuando el proceso finaliza llega un correo con el siguiente asunto: “Información Ajustes Aplicados”.\r\nBuscamos en los enviados Ajustes Portal CRM y ponemos el número  de ejecución en romanos (I, II, III) pegamos la Ejecución, la confirmación que es el correo y el monitoreo y lo enviamos.\r\n', 102, 84),
(28, 'Paso 1.)Monitoreo', 'Se ingresa al portal Billing (10.69.32.14:82/bo/inicio.php), con el usuario está en el plan de producción, nos dirigimos a la sección Cartera/Monitoreo pagos y Reconexiones , habrán dos colores (Amarillo y Rojo)', 103, 85),
(29, 'En caso de error...', 'Si lleva más de 30 minutos en color Rojo se escala a Soporte Colombia móvil portal CRM.', 103, 86),
(30, 'Paso 1.)Cargar Eventos', 'Para poder mirar los eventos que se cargan por el proceso, nos debemos ir a la terminal en la ruta /data/rel/retail/,por lo cual luego se procede a ejecutar el job', 104, 87),
(31, 'Paso 1.)Ejecución del job', 'Después de haber cargado los eventos,  se procede a ejecutar el job y luego se verifica en el portal de direcciones u operaciones y se validen las pestaña \"Validación servicios\".', 104, 88),
(32, 'Paso 1.)Archivos generados', 'para ver los archivos generador, Abrimos la conexión en WINSSP con el servidor 230 y vamos a la ruta: brm/pin/7.4/millicom_custom/scripts/Reportes/reporte_tarifas, actualizamos en la ruta y se crean 2 archivos con los siguientes nombres: reporte_tarifas_YYYY/MM/DD.txt y log_reporte_tarifas.txt', 105, 89),
(33, 'Paso 2.)Correo', 'Al ver los archivos generados que mostraron en la ruta mencionada, se debe redactar un correo y adjuntar los dos archivos, junto con la hora y fin de la ejecución del proceso.', 105, 90),
(34, 'Para tener en que...', 'En caso de haber reportes de tasación pendientes se debe esperar a que finalice el proceso de Pre-ajustes, Ejemplo: si se están ejecutando los pre-ajustes del día 2 o ciclo2 se debe esperar a que termine para poder ejecutar los reportes de tasación del ciclo 3 o día 3', 106, 91),
(35, 'Pre requisitos', 'ESTAR AL DÌA EN LOS SERVICIOS y bajar el pipeline de Retail Tasacion; es incompatible con IREL, Pre-ajustes de ciclo; se debe ejecutar antes del proceso de Pre-ajustes del mismo ciclo', 106, 92),
(36, 'El correo', ' para ejecutar el proceso vamos a Control M en la malla de Facturación Diario buscamos el job que se llama reporte de tasación, lo ejecutamos y lo monitoreamos desde el Portal Billing en el módulo Tasación > Rep. Tasación Día; cuando finaliza el proceso llega un correo con el siguiente asunto: “Reporte de Tasación MM/DD/YYYY” vamos al WINSSP y en el servidor 230 en la siguiente ruta: brm/pin/7.4/millicom_custom/scripts/Reportes/Tasación actualizamos y nos debe generar el siguiente archivo: REPTASACION_MM/DD/YYYY.log este archivo se adjunta al correo', 106, 93),
(37, 'Para tener en cuenta', 'En caso de haber reportes de tasación pendientes se debe esperar a que finalice el proceso de Pre-ajustes, Ejemplo: si se están ejecutando los pre-ajustes del día 2 o ciclo2 se debe esperar a que termine para poder ejecutar los reportes de tasación del ciclo 3 o día 3', 107, 94),
(38, 'Pre requisitos', 'ESTAR AL DÌA EN LOS SERVICIOS y bajar el pipeline de Retail Tasacion; es incompatible con IREL, Pre-ajustes de ciclo; se debe ejecutar antes del proceso de Pre-ajustes del mismo ciclo', 107, 95),
(39, 'Correo que se envía', 'para ejecutar el proceso vamos a Control M en la malla de Facturación Diario buscamos el job que se llama reporte de tasación, lo ejecutamos y lo monitoreamos desde el Portal Billing en el módulo Tasación > Rep. Tasación Día; cuando finaliza el proceso llega un correo con el siguiente asunto: “Reporte de Tasación MM/DD/YYYY” vamos al WINSSP y en el servidor 230 en la siguiente ruta: brm/pin/7.4/millicom_custom/scripts/Reportes/Tasación actualizamos y nos debe generar el siguiente archivo: REPTASACION_MM/DD/YYYY.log este archivo se adjunta al correo', 107, 96),
(40, 'Ejecución', 'En los elementos enviados  buscamos el correo con el siguiente asunto: “Verificación Reporte de Tasación” en el portal Billing buscamos el monitoreo del reporte de tasación mensual, pegamos el pantallazo y enviamos el correo.', 109, 97),
(41, 'Ejecución y correo', 'Cuando el proceso termina llega un correo con el siguiente asunto: “Proceso de Alocación de Pagos con fecha YYYY/MM/DD y proceso XXXXXXXX termino OK Informe consolidado” se verifica el adjunto del correo; en caso de falla se escala al soporte de cartera disponible.', 110, 98),
(42, 'Nota', 'En caso de que haya atraso con el Billing de ciclo y se ejecute en la mañana después de las 7AM se debe realizar la ejecución de NEW ALLOCATE PAYMENTS ATRASO BILLING', 110, 99),
(43, 'Ejecución', 'Verificamos que se encuentre arriba por lo siguiente: /brm/pin/ifw/conf/mediación.reg\r\nbrm/pin/ifw/conf/retail.reg\r\nLos comandos para consultar el estado de los Pipeline:\r\nPs – fea | grep ifw \r\n', 111, 100),
(44, 'Nota', 'Si el Foto Reporte falla es posible que el Saldo por Item también”; Abrimos el WINSSP en la conexión 230 BD BRM y vamos a la ruta: fs_arch_01/utlfile; se verifica que se estén escribiendo los archivos, cuando finaliza el proceso llega un correo con el siguiente asunto: “Proceso Cartera Diario con fecha YYYY/MM/DD ok informe consolidado” se revisa el adjunto en la última línea dice que el proceso termino exitoso.\r\nAbrimos el WINSSP en la conexión 230 BD BRM y vamos a la ruta: fs_arch_01/utlfile y validamos que se hayan creado los siguientes archivos:\r\n1.	Hijas_con_saldos_YYYYMMDD_SNA.csv\r\n2.	CARTERA_WRT_YYYYMMDD.log\r\n3.	Desconciliados_YYYYMMDD.csv\r\n4.	N_REPCARTERA_SIN_ALLOCATE_YYYYMMDD.csv\r\n5.	N_ REPCARTERA_YYYYMMDD.csv\r\n6.	Conciliacion_YYYYMMDD.log', 112, 101),
(45, 'Ejecución', 'Los archivos antes mencionados,los descargamos  a la maquina local, se comprimen con el siguiente nombre: “N_REPCARTERA_YYYYMMDD.zip”; en el WINSSP abrimos una nueva conexión esta con el nombre Reportes CM el servidor es el 10.65.32.12 y vamos a la ruta pospagocm/reportes/Elizabeth en opciones de transferenci	a seleccionamos Binario, los archivos que son .zip .rar y .gz', 112, 102),
(46, 'El correo', 'Luego vamos a la carpeta enviados en el correo y buscamos el siguiente asunto: “Proceso Cartera Diario con fecha YYYY/MM/DD ok informe consolidado” se envían dos correos, en el primero va la generación de los archivos y transferencia de los archivos y se adjunta el log que llega en el correo; en el segundo correo se realiza la transferencia de los archivos hijas con saldos y Desconcialiados, existen 2 maneras de transferir los archivos una es abrir la conexión del servidor Billing 10.69.34.91 en la siguiente ruta: repositorio/dump/desconciliados_cartera; se transfieren y se pega pantallazo del proceso en el segundo correo y la segunda forma es transferir los archivos de servidor a servidor como los archivos dump; no se adjuntan archivos.', 112, 103),
(47, 'Importante ', 'En caso de presentar vacíos o se encuentren incompletos se escala a soporte disponible cartera.\r\nCuando llega un correo de la dirección repCarteradiario@Tigo.com.co y con el asunto: “Termina con error proceso día” y con adjunto cartera diario y dentro del adjunto encontramos “Error: Aun no termina la foto repcartera, ejecute sh SALDO_ITEM cuando finalice el proceso de Foto Reporte”', 112, 104),
(48, 'Ejecución', 'Cuando finaliza llega un correo con el siguiente asunto: “Collections Suspend” que nos informa cuantos registros se actualizaron. ', 113, 105),
(49, 'Ejecución', ' Cuando finaliza el proceso llega un correo con el siguiente asunto: “Reporte Pagos Diarios para el día MM/DD/YYYY” se adjunta el archivo y abrimos el WINSSP en el servidor 10.69.32.230 BDBRM y nos vamos a la siguiente ruta: brm/pin/7.4/millicom_custom/scripts/reporte-pagos/out allí se genera un archivo con extensión .gz', 114, 106),
(50, 'En caso de error...', 'Reporte Pagos 20160729_20160729_2 (Madrugada); en caso de error se debe validar con el soporte disponible de BA.', 114, 107),
(51, 'Ejecución', 'Se verifica en el servidor 10.69.32.2.29 en la siguiente ruta Data/Rel/Error/Retail\r\nSi quedan errores vamos a Control M y en la malla Facturación Diario buscamos el job PROCESO DE REPROCESAMIENTODE IREL RETAIL TODOS LOS SERVICIOS y lo ejecutamos.', 115, 108),
(52, 'En caso de error.', 'Si continua con error se escala a soporte BRM indicando que se ejecutó el Job de reprocesamiento y continúan los errores; y se informa a través de Email.\r\nNOTA: Por ser proceso IREL es incompatible con los antes mencionados.\r\n', 115, 109),
(53, 'Ejecución', 'Vamos a enviados y buscamos el correo con el asunto: “EJECUTAR VALIDACIÓN DE CUENTAS BILLING DD/MM/YYYY” se pega la Ejecución, abrimos el WINSSP en el servidor 10.69.32.229 en la ruta brm/pin/7.4/millicom_custom/scripts/Validacuentas actualizamos la ruta y vemos que los archivos conteo_cuentas.txt y detalle_cuentas.txt validando que se haya realizado la consulta del día; tomamos pantallazo para enviar en el correo.', 116, 110),
(54, 'Correo y ejecución', 'El proceso tiene varias fases de ejecución la primera es cuando nos dice que podemos validar los bancos; el continua ejecutando las demás fases; cuando finaliza la ejecución llega un correo con el siguiente asunto: “Resultado Proceso Validación de Saldos3 para el día YYYYMMDD” unas veces llega el correo con adjunto y otras no.', 117, 111),
(55, 'Posibles errores', 'Cuando llega sin adjunto indica que al día siguiente no hay suspensiones de ciclo en algunas ocasiones, se debe tener en cuenta que no haya errores de base de datos, uno de los errores “ORA-03113:end-of-file on comunication cannel Process ID:” si tiene este error se debe escalar al soporte disponible de Base de Datos, otro error es: “ORA-00060: deadlock detected while waiting for resource” se debe esperar a que finalice el proceso Valida saldos y ejecutarlo nuevamente, si mantiene el error se escala al soporte disponible de Base de Datos; cuando llega con el adjunto son las líneas esperadas a suspender por el ciclo.', 117, 112),
(56, 'En caso de error', 'Si el correo llega con los siguientes errores ERRXXXXX AAJUHeAHqAAAXXXXXXX son normales del sistema y no hay porque alarmarse.\r\n', 117, 113),
(57, 'Ejecución', 'vamos al correo en la carpeta de enviados y buscamos el siguiente asunto: “Archivos Recargas de Ciclo DD/MM/YYYY”; Vamos al WINSSP en la conexión 230 y vamos a la ruta brm/pin/7.4/millicom_custom/scripts/recargas_ciclo/out acá encontramos las siguientes carpetas:\r\n1.	Control Datos.\r\n2.	Control datos ciclo.\r\n3.	Control ciclo.\r\n4.	Pospago_rtb.\r\n5.	Pospago datos rtb.\r\n6.	Control sponsored.\r\n7.	Cba.\r\n8.	Control.\r\nValidamos en cada una de las carpetas que se hayan creado los archivos del día; estos archivos se descargan a la maquina local, tomamos el pantallazo y se comprime con el nombre: “Archivos_Recarga_Ciclo_DD/MM/YYYY” y se adjunta al correo para enviarlo.\r\n', 118, 114),
(58, 'Ejecución', 'para bajar el pipeline en Control M en la malla de Facturación Pipeline se ejecuta el Job BAJADA PIPELINE RETAIL TASACIÓN y validamos que se encuentre abajo en el Putty.', 119, 115),
(59, 'El envío', 'en el correo se adjuntan los pantallazos de las ejecuciones y de los diferentes bancos WS y solo se adjuntan las ejecuciones que se lleven comprimiendo el archivo que se nombra “Asobancarias_DD/MM/YYYY”', 120, 116),
(60, 'Ejecución', 'Validamos en control M el job Generación Arch Clausula en el output que haya finalizado de manera correcta en la última línea y que haya creado un archivo detalle clausulas_YYYY/MM/DD.txt\r\nAbrimos una conexión en el WINSSP en el servidor 10.69.32.228 en la ruta: brm/pin/7.4/millicom_custom/scripts/clausulas/out y verificamos que se encuentre el archivo del día. ', 121, 117),
(61, 'En caso de error', 'En caso de que haya un error, se debe notificar o escalar al soporte de operaciones', 121, 118),
(62, '¿De que consta?', 'validar que se encuentren ejecutados de manera correcta en el output, en caso de fallar se informa a la Sra. Edna Perilla.\r\nTambién se valida el job SP_GET_VENT_DETALLE_GESTORES', 122, 119),
(63, 'En caso de error', 'en caso de fallar se realiza escalamiento a Yeimy Carolina Rodríguez; Validamos en el output que termine de forma correcta.', 123, 120),
(64, 'Ejecución', 'verificamos el output que no tenga errores y que haya procesado de forma correcta y cuando finaliza el proceso (EN EL MOMENTO NO ESTA LLEGANDO EL CORREO)', 124, 121),
(65, 'En caso de error', 'en caso de que llegue el correo informando que se generó sin registros no se debe reprocesar ni escalar a ninguna área. Si llega termino con error y el código ORA 08103 object no longer exists se debe esperar 60 minutos y volver a ejecutar.', 124, 122),
(66, 'Ejecución', 'verificamos el output que no tenga errores y que haya procesado de forma correcta y cuando finaliza el proceso llega un correo con el siguiente asunto: “CDV REP AJUSTES CM YYYY/MM/DD”', 125, 123),
(67, 'En caso de error', 'En caso de que llegue el correo informando que se generó sin registros no se debe reprocesar ni escalar a ninguna área. Si llega termino con error y el código ORA 08103 object no longer exists se debe esperar 60 minutos y volver a ejecutar.', 125, 124),
(68, 'Ejecución', 'Verificamos el output que no tenga errores y que haya procesado de forma correcta y cuando finaliza el proceso llega un correo con el siguiente asunto: “Reporte Recaudo CDV CM YYYY/MM/DD”', 126, 125),
(69, 'En caso de error', 'en caso de que llegue el correo informando que se generó sin registros no se debe reprocesar ni escalar a ninguna área. Si llega termino con error y el código ORA 08103 object no longer exists se debe esperar 60 minutos y volver a ejecutar.', 126, 126),
(70, 'Ejecución', 'A través del correo llega el asunto “Se han encontrado Archivos” en el 10.69.32.229 (MEDBRMPI) validamos la ruta Data/ielpagos/working.\r\nPara verificar IEL se ejecuta el siguiente comando: ps –fea | grep iel\r\nCuando está arriba el IEL muestra lo siguiente: ./iel  iel_fes\r\nCuando esta abajo solo muestra el grep iel, en este caso hay que reportarlo a soporte BRM para que lo inicien.', 127, 127),
(71, 'Ejecución', 'Vamos a la carpeta de elementos enviados buscamos un correo con el siguiente asunto: “Verificación Pagos IEL DD/MM/YYYY” vamos al WINSSP y abrimos una conexión en la 229 en la siguiente ruta: \r\nData/iel/pagos/success se valida lo que se haya generado en la fecha; también se valida en la ruta: Data/iel/pagos/working Data/iel/pagos/error\r\n', 128, 128),
(72, '¿Qué se debe de hacer para ejecutar?', 'Para poder ejecutar debe haber finalizado de manera correcta Valida Saldos y que hayan finalizado las suspensiones', 129, 129),
(73, 'Ejecución', 'En la generación de un archivo con líneas que fueron suspendidas; para todo tipo de re activación siempre hay que esperar a que finalice las suspensiones; en control M ejecutamos el Job que se llama Re activaciones_light; abrimos una conexión en el WINSSP en la 229 y vamos a la ruta:\r\nbrm/pin/7.4/millicom_custom/scripts/cartera/reactiva_cartera, allí verificamos que se haya creado el siguiente archivo: “Collection YYYY/MM/DD_99B.txt” descargamos el archivo en la maquina local y luego lo cargamos al portal CRM staff.tigo.com.co/CRM/Portal/auth/portal/default/Postventa nos ubicamos en el módulo Transacciones > Modificación de Línea >  Transacciones masivas > Estado servicio: ARECC seleccionamos el archivo, ', 129, 130),
(74, 'Ejecución', 'Nos vamos al portal Billing en el módulo tasación > validación servicios.\r\nLos servicios que no se tienen en cuenta para iniciar el Billing son: (MMSB, SMC1, SOF4, SOF9 y SOFA) el resto de servicios debe estar al día, después de las 00:00; estando al día en los servicios vamos a la carpeta de enviados y buscamos el siguiente correo: “Estado de los servicios DD/MM/YYYY” validamos que no haya archivos por cargar en las rutas mencionadas y pegamos los pantallazos y enviamos el correo.\r\n', 130, 131),
(75, 'Ejecución', 'Si tiene una de las siguientes caracteristicas ambas la primera es “Existen pagos CRM sin aplicar aun, por favor validar los siguientes pagos no aplicados” y la segunda es “Existen pagos pendientes de gestión por parte de CRM (pagos sin aplicar)” se escala con ticket.\r\nGuardamos el correo en la maquina local, en enviados buscamos un correo con el asunto: “Control Interno P4SC8”; abrimos el service desk (netvm-psdmsta02/CAisd/pdmweb.exe)\r\nNivel 1 NOC\r\nFile > New Incident\r\n%Operador. Postpago\r\n%Movil.servicios de aplicación.Portal CRM.Falla en proceso de pagos\r\nStatus: Abierto\r\nTipo: Incidente\r\nUrgencia: Alta\r\nImpacto: Alto\r\nRegional: Noroccidente Medellín\r\nGrupo: Soporte Apps Movil\r\nSummary: Control Interno P4SC8\r\nDescription: ponemos el correo y adjuntamos\r\nSe envía el correo con el pantallazo del incidente creado.', 131, 132),
(76, 'Ejecución', 'Cuando finaliza el Job llega un correo con el siguiente asunto: “Proceso Día YYYY/MM/DD OK Informe consolidado” de la dirección: rtbilling@tigo.com.co con el adjunto: “UpdateRTBILL_YYYY/MM/DD” se valida el adjunto de que haya finalizado exitosamente.', 132, 133),
(77, 'Ejecución', 'Cuando finaliza el Job llega un correo con el siguiente asunto: “Proceso Día YYYY/MM/DD OK Informe consolidado” de la dirección: rtbilling@tigo.com.co con el adjunto: “UpdateRTBILL_YYYY/MM/DD” se valida el adjunto de que haya finalizado exitosamente.', 133, 134),
(78, 'Primera vista', 'cuando finaliza el Job llega un correo con el siguiente asunto: “Proceso Día YYYY/MM/DD OK Informe consolidado” de la dirección: rtbilling@tigo.com.co con el adjunto: “UpdateRTBILL_YYYY/MM/DD” se valida el adjunto de que haya finalizado exitosamente.', 134, 135),
(79, 'Ejecución', 'En WINSSP en la 229 en la siguiente ruta DATA/IEL/RTB/SUCCESS validamos que se haya creado uno de los 2 archivos:\r\n1.	RTB no service YYYY/MM/DD.txt\r\n2.	RTB service YYYY/MM/DD.txt\r\nLuego vamos a la carpeta enviados y buscamos el correo con el siguiente asunto: “Verificación Archivos RTBilling DD/MM/YYYY” pegamos los pantallazos de las siguientes rutas: DATA/IEL/RTB/SUCCESS, DATA/IEL/RTB/DONE y DATA/IEL/RTB/ERROR y se envía el correo.\r\n', 134, 136),
(80, 'Ejecución', 'Cuando finaliza llega un correo con el siguiente asunto: “Conexiones del IVR”, se verifica el correo para las diferentes consultas y no puede tener más de 3 segundos para la consulta, en caso de tener más segundos se escala con el soporte BRM.', 135, 137),
(81, 'Ejecución', 'Verificamos el output que no tenga errores y que haya procesado de forma correcta y cuando finaliza el proceso llega un correo con el siguiente asunto: “CDV REP CUENTAS ABIERTAS YYYY/MM/DD” en caso de que llegue el correo informando que se generó sin registros no se debe reprocesar ni escalar a ninguna área', 136, 138),
(82, 'En caso de error', 'Si llega termino con error y el código ORA 08103 object no longer exists se debe esperar 60 minutos y volver a ejecutar.', 136, 139),
(83, 'Ejecución,ruta y comandos', 'Verificamos que se encuentre arriba por lo siguiente: /brm/pin/ifw/conf/mediación.reg\r\nbrm/pin/ifw/conf/retail.reg\r\nLos comandos para consultar el estado de los Pipeline:\r\nPs – fea | grep ifw ', 137, 140),
(84, 'Ejecución', 'El correo, se descarga en la carpeta y se descomprime, vamos al portal CRM para subir los archivos; en el módulo postventa > Modificación de Línea  > Transacciones Masivas, se cargan los archivos que lleguen, se toman los pantallazos de dichos archivos y se envía el correo', 138, 141),
(85, 'En caso de error', 'En caso de que llegue el correo sin adjunto o si llega malo se le informa a la Señora Luz Fandiño.', 138, 142),
(86, 'Ejecución y Output', 'verificar que haya terminado correctamente en el output.', 139, 143),
(87, 'Su ejecución', 'Buscamos un correo con el asunto: “Generación Archivo Límite de Consumo (Saldo Cuentas)” ponemos el pantallazo del Output validando que haya terminado de forma correcta, y en el WINSSP en el servidor 229 a la ruta: /brm/pin/7.4/millicom_custom/scripts/saldo_cuentas y validamos el log que se llama Saldo_cuentas_db.log tomamos el pantallazo y adjuntamos el log al correo.', 140, 144),
(88, 'Ejecución', 'Se valida que no haya proceso de REL ya que es incompatible con el ajuste y vamos al plan de producción para desmarcar la tabla, validamos en el portal que se desmarque la tabla y en control M buscamos el job de ajuste ambiente; en el output validamos las fechas de ciclo y la fecha de límite de pago.\r\nCuando se terminen de ajustar los ambientes se vuelven a marcar las tablas en el plan de producción.\r\n', 141, 145),
(89, 'Ejecución', 'vamos al WINSSP y en la ruta: brm/pin/7.4/millicom_custom/scripts/cartera/archivos_asobank y el archivo lo vamos a mover a esta ruta en modo texto; en control M ejecutamos el job que se llama Valida Bancos.\r\nPor el momento si el archivo genera reactivaciones, se montan al portal CRM y llega un correo de confirmación; el archivo que se monta en el portal CRM se renombra según el manual y se transfiere al servidor Siebel a la ruta in.', 142, 146),
(90, 'LLegada del correo', 'en la ruta donde se aplican los bancos,en el WINSSP vamos a la ruta: brm/pin/7.4/millicom_custom/scripts/cartera/archivos_asobank y el archivo lo vamos a mover a esta ruta en modo texto; en control M ejecutamos el job que se llama Valida Bancos.\r\nCuando llega con el adjunto de reactivaciones que se llama: “Collection_YYYY/MM/DD_XXXXb.txt” este archivo lo vamos a cargar en el portal CRM y en la carpeta enviados buscamos el correo con el siguiente asunto: “Reactivaciones por CRM”  ', 143, 147),
(91, 'En caso de error', 'Cuando llegue el correo con el siguiente asunto: “Se han encontrado archivos antiguos” se debe escalar al soporte de BRM respondemos el correo con los pantallazos de la ruta donde se encuentra el archivo que no ha sido procesado, la validación del IEL que se encuentra arriba y enviamos el correo, esto es para el caso en el que no se procesen los archivos de los bancos; se llama al de soporte para informarle.\r\n', 143, 148),
(92, 'Ejecución y correo', 'Llega un correo con el siguiente asunto: “COLOCACIÓN CUENTAS DE COBRO PYME EN FTP_1er envió” llega con archivos adjuntos, estos los guardamos en la maquina local; en el WINSSP en el servidor (172.24.32.181) FTP_PYMES los archivos los transferimos a la carpeta que nos indica el correo, tomamos el pantallazo donde se montan los archivos para pegarlo en el correo, le damos responder a todos, pegamos el pantallazo y se envía el correo.', 144, 149),
(93, 'Ejecución', 'En el Output validamos la cantidad de cuentas a comprar y las promociones  compradas “Termina proceso de compra de promociones, se compraron XXX promociones”.; debemos tener en cuenta que no debe haber mucha diferencia entre las promociones que aplicaban y las que se compraban.', 145, 150),
(94, 'Validación del archivo día del ciclo', 'Se encuentra en la ruta (/data/iel/act_tax_bill/success) de la maquina 10.69.32.229; en la ruta debe estar el archivo correspondiente al día del ciclo y que se cargue el archivo del día siguiente.', 146, 151),
(95, 'Validación', 'Para verificar abrimos el WINSSP en el servidor 229 en la ruta: /data/iel/RTB/success estando allí validamos que haya uno o varios archivos creados de la siguiente forma “FACTURACION_YYYY/MM/DD_01.txt” abrimos el archivo y validamos que en la 3 columna se encuentre el número del ciclo; validamos en la ruta /data/iel/error/ verificamos que no hayan archivos con error; abrimos un Putty en la siguiente ruta: /brm/pin/7.4/millicom_custom/scripts/monitoreos_billing/contsum_credivalores/log listamos y validamos que se encuentre el siguiente log “monitoreo_credivalores.YYYY/MM/DD.log”', 147, 152),
(96, 'Ejecución', 'Ejecutamos more para validar que no haya errores, en la última línea del log me dice la fecha del día anterior, no debe haber diferencias o registros duplicados.', 147, 153),
(97, 'Ejecución', 'Nos vamos al portal Billing en el módulo tasación > validación servicios; Los servicios que no se tienen en cuenta para iniciar el Billing son: (MMSB, SMC1, SMB1, SOF4, y SOFA) el resto de servicios debe estar al día, después de las 00:00; estando al día en los servicios vamos a la carpeta de enviados y buscamos el siguiente correo: “Estado de los servicios DD/MM/YYYY” validamos que no haya archivos por cargar en las rutas mencionadas', 148, 154),
(98, 'Verificación del output', 'Validamos en el output que no tenga errores en el final aparece la confirmación de que se ejecutó de manera correcta.', 149, 155),
(99, 'Ejecución', 'Debemos validar la fecha límite de pago, fecha de ciclo a facturar y el Delay 0 que nos indica que se toma la fecha actual, en caso de que estemos atrasados el Delay será 1 o 2 de acuerdo a los días de atraso en el ciclo.', 150, 156),
(100, 'Ejecución', ' En control M le ponemos candado. OJO: “En caso de fallar no se debe volver a ejecutar hasta que nos den la confirmación y se debe escalar al soporte de billing”. Cuando termina llega un correo con el siguiente asunto: “Pre-Billing ciclo YYYY/MM/DD” se debe cumplir que todas las reglas deben estar OK en el correo, que no tenga errores y que las fechas correspondan.', 151, 157),
(101, 'Ejecución', 'Cuando finaliza el Billing en la ejecución se debe validar que no queden cuentas pendientes (En caso de quedar cuentas pendientes informar inmediatamente al soporte de Billing), en el output del Job se valida la fecha del ciclo, fecha límite de pago, “FECHA_BILL_T_REGISTROS” debe estar la fecha límite de pagos, “FECHA_ITEM_T_REGISTROS” “Com_Change_Due_date”.', 152, 158),
(102, 'En caso de error', 'En caso de no quedar con las fechas de límite de pago en ninguno de las 3 se le informa al soporte de Billing y no se continua con los procesos.', 152, 159),
(103, 'Ejecución', 'En control M en la malla de Ciclo el job REAL_TIME_BILLING_CICLO se ejecuta. Cuando finaliza el job dice Fin Exitoso para YYYY/MM/DD, en el output validamos la fecha del ciclo y al final aparecen unos errores ORA-24247, ORA-06512, ORA-06512, ORA-06512, ORA-06512, ORA-06512 estos errores son normales de la ejecución.', 153, 160),
(104, 'El correo que llega', 'Si llega sin adjuntos no se debe escalar ya que no hay ajustes para ese ciclo; debemos validar que el IEL se encuentre arriba', 154, 161),
(105, 'Descarga y verificación', 'Descargamos los archivos a la maquina local, abrimos el WINSSP en el servidor 229 y pasamos el archivo a la siguiente ruta: /data/iel/ajustes/temp y abrimos el putty en la misma ruta; verificamos que en la ruta: /data/iel/ajustes/success no hayan archivos y que en la ruta /data/iel/ajustes/working no hayan archivos y en la ruta /data/iel/ajustes/error no hayan archivos; y se mueve a la ruta: /data/iel/ajustes/in; Respondemos el correo y allí pegamos el pantallazo de la ejecución del putty, el pantallazo de success y el de error.', 154, 162),
(106, 'Ejecución', 'Cuando finaliza el job llega un correo con el siguiente asunto: “Pos-Billing Ciclo YYYY/MM/DD” se valida que el proceso termine OK y que todas las reglas hayan sido aplicadas', 155, 163),
(107, 'Ejecución', 'Se valida el output que termine de manera correcta; cuando finaliza el proceso llega un correo con el siguiente asunto: “Item vs Bill ciclo YYYY/MM/DD” verificamos que tenga el adjunto, que se hayan aplicado las reglas y que termine OK. “OJO: Se confirma que los procesos de Item Bill  y de Súper Saldos fueron realizados de forma correcta, para el ciclo YYYY/MM/DD. Se puede dar inicio al Invoice cuando se termine el proceso de aplicación de los pre-ajustes.”', 156, 164),
(108, 'Ejecución', 'Se debe validar el output que finalice de manera correcta, en caso de fallar se escala al soporte billing.', 157, 165),
(109, 'Ejecución', 'Es validar la fecha límite de pago en las hojas o en la entrega.', 158, 166),
(110, 'Ejecución', 'Cuando finaliza llega un correo con el siguiente asunto: “Fin Cifras de Control Ciclo MM/DD/YYYY Antes de Billing” en el portal billing en el módulo Cifras Control > Cifras Billing en esta parte aparecen las cuentas que se enviaron en el correo.', 159, 167),
(111, 'Ejecución', 'En Control M se encuentra en la malla Varios Automáticos.\r\nBuscamos el job de nombre White List Roaming; Validamos en el Output que el proceso haya finalizado de manera correcta.\r\nEn caso de falla se escala vía Email a la persona encargada de BRM (soporte Infranet BRM)\r\nEn el conexión Manager Servidor 10.65.145.9 (BOGRTB02) accedemos a la ruta/cba1/log y verificamos la creación del archivo.\r\n', 160, 168),
(112, 'Ejecución', 'Cuando el proceso finaliza llega un correo con el siguiente asunto “Proceso Info Inscritos Billing FECHA DEL CICLO”.\r\n', 161, 169),
(113, 'Incompatibilidades', 'Este proceso es incompatible con los IREL, Reportes de Tasación, Billing de Ciclo y si hay ventana de BRM.', 162, 170),
(114, 'Factor a conocer', 'Los ambientes deben ejecutarse de manera correcta para poder realizar la recarga de los recursos a los usuarios en BRM correspondiente a los ciclos, también realiza el recaudo de los recursos solicitados por los usuarios postpago a BRM, por eso es una Pre facturación.', 162, 171),
(115, 'Ejecución', 'Se debe enviar un correo con el siguiente asunto:\r\nPara los días 29, 30 y 31 “Billing Diario para el día 2016/07/29”\r\nPara los días 1 al 28 “Billing Diario para el día 2016/07/28 “Noche de Ciclo”', 162, 172),
(116, 'Ejecución', 'Cuando finaliza este proceso llega un correo con el siguiente asunto: “Cancelación Promociones REQ 1007 Infranet Siebel”; Se verifica que no haya errores en la ejecución del proceso en el output, se responde el correo con copia a Menestys.\r\nEn caso de fallar el proceso se escala a soporte BRM.\r\n', 163, 173),
(117, 'Ejecución', 'Cuando finaliza el proceso se valida el output, cuando finalizan los 2 procesos (el primero es de voz y el segundo de datos) vamos a la carpeta enviados y buscamos un correo con el siguiente asunto: “Monitoreo y Ejecución Recargas Control con Cambio Ciclo DD/MM/YYYY” se pegan los pantallazos del output de los dos procesos el de voz y el de datos y se envía el correo.\r\n', 164, 174),
(118, 'Primera Fase', 'Ingresamos a Pay-U con usuario y contraseña que se encuentra en el plan de producción en la pestaña de información.\r\nTransacciones / Reporte de Transacciones \r\nEn fecha inicio se pone el día anterior 2016-07-29\r\nEstado: Aprobada\r\nTipo de salida: Excel y buscamos, el archivo que genera es 21127 el cual se renombra transactions_21127_YYYYMMDD.csv se toma pantallazo para el correo Aplicación Conciliación pagos TOL', 165, 175),
(119, 'Segunda Fase', 'Cerramos sesión e ingresamos con el usuario: alexander.prada.102749 y la clave: Tigoune.1011 el archivo que genera es 21131 y se renombra transactions_21131_YYYYMMDD.csv\r\n 	Ingresamos a Pay U con usuario alexander.prada@tigo.com.co y la clave: Tigo.2016\r\nMódulo: Reportes > Transacciones \r\nCuenta: 527424\r\nFecha inicial DD/MM/YYYY\r\nEstado: aprobado\r\nFormato: Excel\r\nEl archivo descargado se renombra transactions_YYYYMMDD.cvs\r\nLos archivos los pasamos a la conexión de conciliación Tol\r\nVamos al WINSSP en el servidor 10.69.32.229 y vamos a la ruta brm/pin/7.4/millicom_custom/scripts/cartera/ws_tol\r\nSe suben los archivos y se toma pantallazo para el correo\r\nLuego vamos a Control M en la malla de Facturación Diario y ejecutamos el job que se llama CONCILIACION TOL', 165, 176),
(120, 'Finalización y correo', 'uando finaliza el job llega un correo con el siguiente asunto: “Conciliación Pagos Online del 2016/07/29” se reenvia a los destinatarios con copia a Menestys.\r\nLuego vamos a elementos enviados en el correo y buscamos el siguiente asunto: “Pagos Tigo Online” y adjuntamos los archivos y se envía a los destinatarios\r\nLos pantallazos tomados a los archivos', 165, 177),
(128, 'Paso 1.) Ingreso al Servidor', 'Para iniciar se debe ingresar al servidor MEDPLANPROD02, mediante escritorio remoto.', 173, 185),
(129, 'Paso 2.)Credenciales', 'Para continuar con el proceso, se debe de ingresar las credenciales correspondientes al servidor, y poder iniciar sesión en el equipo remoto', 173, 186),
(130, 'Paso 3.)Intérprete PL SQL Developer', 'Ya dentro del equipo remoto, se debe buscar el acceso directo a la herramienta PL SQL Developer y se procede a abrir', 173, 187),
(131, 'Paso 4.)Logueo con credenciales', 'Dentro del archivo: Conexion.php se deben ingresar las credenciales o datos correspondientes para que la conexión funcione, cuando ingrese las conexiones debe solicitar cambio de contraseña y mostrar ventana donde debe ingresar la nueva contraseña', 173, 188),
(132, 'Paso 5.)Finalización', '5.	Cuando se realice el cambio y se ingrese con las nuevas credenciales, cambiar la contraseña en el archivo mencionado anteriormente (conexion.php) ¡solo la contraseña!', 173, 189),
(133, 'Paso 1.)El inicio', 'Para iniciar, debemos ir a la máquina pin4@10.69.34.60 ubicarse en la ruta: /infranet/pin4/7.3/sys/test, para poder así empezar el proceso', 174, 190),
(134, 'Paso 2.)Ejecución', 'Ejecutan el comando stop_all, para bajar la instancia.', 174, 191),
(135, 'Paso 3.)Segundos Comandos', 'Luego ejecutan los siguientes comandos:\r\nPrimero este: kill -9 `ps -ef | grep -i cm | awk {\'print $2\'}`\r\nDespués este: kill -9 `ps -ef | grep -i dm | awk {\'print $2\'}`\r\n', 174, 192),
(136, 'Paso 4.)Tiempo de ejecución', 'En un tiempo no mayor a 5 min. Suben la instancia con este comando: start_all y a la(s) pregunta(s) dar y y enter', 174, 193),
(137, 'Paso 5.)Finalización', 'Cuando termina de subir y arroja promp: /infranet/pin4/7.3/sys/test, espear un(1) minuto y ejecutar el comando testnap para verificar la instacia; el cual debe arrojar como resultado algo como esto:\r\n===> database 0.0.0.1 from pin.conf \"userid\"\r\nnap(901)>\r\n', 174, 194),
(138, '¿Qué es?', 'Callgate es la aplicación que se encarga de decodificar la información de eventos almacenada en las centrales, decodificarla y procesarla para determinar el tipo de tráfico cursado y dependiendo de las reglas de negocio entregarla en formatos predefinidos a las diferentes áreas de la compañía: Facturación, Interconexión, RA, Fraude y otros sistemas de almacenamiento de información como DWH y Consulta de CDRs.\r\nA continuación, se describen todos los servidores que actualmente están en producción.\r\n', 175, 195),
(139, 'Gestión de los Servidores', 'Cada servidor gestiona un tipo de servicio, a continuación, se describen los servicios que son gestionados por cada servidor.\r\nSi se presenta atraso en el estado de los servicios, ejecutar el siguiente paso a paso para identificar y solucionar la posible causa raíz.', 175, 196),
(140, '1)BAJAR SERVICIOS', 'Con el servicio y el servidor identificado, ingresar con usuario administrador y ubicarse en la aplicación que requiere detener (Collection, Processing, Distribution). \r\n\r\nIniciar siempre la revisión por los servicios de colección, luego procesamiento y finalizar con los servicios de distribución.\r\n\r\nPara el caso del ejercicio, vamos a detener la aplicación CG_Huawei_SGSN_FE1\r\n', 175, 197),
(141, '1.1)Abrir aplicación', 'Para abrir la aplicación callgate, dar clic derecho sobre la aplicación y ejecutar como administrador', 175, 198),
(142, '1.2)Logueo', 'Cuando haya abierto la aplicación loguese de manera correcta y luego click sobre el botón \"OK\"', 175, 199),
(143, '1.3)Validación', 'Cuando se haya logueado, se debe de validar que este en estado running, antes de bajar cualquier servicio', 175, 200),
(144, '1.4)Detención', 'Se debe ubicar el botón rojo para detener, clic sobre el botón detener (recuadro rojo), la aplicación pasa inmediatamente a estado stopped.', 175, 201),
(145, '1.5)Opciones', 'Luego de que la aplicación esté en estado \"stopped\" debe ubicar en la pestaña admin en la parte inferior derecha del panel y se abrirá la siguiente imagen.', 175, 202),
(146, '1.6)Remover Archivos y Salir', 'Cuando se esté en la ventana mencionada, dar clic en el recuadro rojo ubicado en la parte superior de la ventana, aparecerá un mensaje de confirmación, acéptelo dando clic en \"Yes\" .cuando se haya procedido a eso, puede cerrar la aplicación, es necesario que de clic en el botón de cancelar ', 175, 203),
(147, '2) Subir Servicios ', 'Para poder subir los servicios, se debe ingresar de nuevo al aplicativo y loguarse', 175, 204),
(148, '2.1)Servicios', 'Cuando entremos, se debe identificar el botón dentro del cuadrado rojo de la imagen, se debe de tener en cuenta que estemos en la pestaña \"Service\", cuando lo ubiquemos, damos clic en el botón ', 175, 205),
(149, '2.2)Servicios', 'Cuando se haya iniciado el servicio, la app cambia de pestaña y queda en \"Main\", allí damos clic en el botón play,la aplicación inicia haciendo una prueba de conectividad hacia todos los elementos de red que tenga configurados.', 175, 206),
(150, '2.3)Finalización de la Conexión', 'Cuándo finaliza la prueba de conexión, la aplicación queda en estado running pero no inicia el proceso de colección, por tanto, es necesario nuevamente detenerla dando clic sobre el botón con recuadro rojo y esperar que quede en estado stopped', 175, 207),
(151, '2.4)Subir Aplicación', 'para subir la aplicación procedemos a dar clic sobre el botón play y validar que inicie la colección de los archivos (CDRs). ', 175, 208),
(152, 'Ingreso', 'para empezar con el procedimiento, se debe de acceder al sitio: http://netvm-psc01:8080/usm/wpf  y pararse en la ruta: Inicio>Servicios Operaciones>Servicios IDC>Administracion Servidores, luego dar clic en la palabra resaltada', 176, 209),
(153, 'Elección', 'Cuando se cargue, elegimos la opción que nos dice Administración de servidores', 176, 210),
(154, 'Los datos', 'Cuando se haya elegido la opción, en los campos de tipo de solicitud , selecciona permisos Unix, y luego diligencia los demás campos', 176, 211),
(155, 'El correo', 'El correo va a llegar todos los días después de la 6:00 p.m. Contiene ajustes que deben ser aplicados, se debe alicar y enviar la respuesta.\r\n', 177, 212),
(156, 'Correo', 'El correo tiene la siguiente estructura', 178, 213),
(157, 'Estructura del Correo', 'Con el correo de sebe de hacer lo siguiente', 179, 214),
(158, 'Verificación Inicial', 'Recibir un correo por parte de Cindy Yohana Bustamante Franco (Cindy.Bustamante@asesor.une.com.co), quien es la encargada de realizar las solicitudes para el envío de dichas facturas.', 180, 215),
(159, 'Archivos Adjuntos', 'Los archivos recibidos deben contener la siguiente estructura básica para la generación de los insumos: “ Consecutivo; Identificador; correo electrónico destino ', 180, 216),
(160, 'En excel...', 'Se habilita la edición para el archivo más grande,se copia la información del archivo con menor tamaño que viene en el correo, la información copiada se pega en el archivo previamente abierto  a partir de la última fila', 180, 217),
(161, 'Guardado', 'El archivo con toda la información debe ser guardado en formato delimitado por comas (.csv) en la carpeta \\Recursos\\Sample Historico con el nombre: \"Ciclo_xx_284_296_fechadeenvio.csv\",a continuación cerrar el archivo Excel e ingresar a la ruta de red: \\Recursos\\Sample Historico y abrir con el editor Notepad++ el archivo anteriormente guardado.\r\nActo seguido realizar la combinación de teclas CTRL + H (Reemplazar) y en los recuadros colocar los siguientes parámetros de búsqueda y reemplazo. P1: “;”	P2: “|”  Sin las comillas. Verificar que el modo de búsqueda sea “Normal” y clic en reemplazar todo', 180, 218),
(162, 'Guardado 2', 'Este archivo se debe guardar en \\Recursos\\ con el nombre de \"Sample_Cartera_UNE.txt\".\r\n', 180, 219),
(163, 'Validación de Red', 'En la misma conexión de red validar en las carpetas DIJ y HTML que no exista información alguna, y en caso de existir eliminarla.', 180, 220),
(164, 'Ingreso al Servidor', 'Ingresar al servidor intpitney01(10.69.42.44) con las credenciales conocidas y ubicarse en el disco E:\\Cartera_UNE\\ y ejecutar el bat Cartera_UNE.bat, para iniciar la generación del archivo JRN y los archivos HTML, para los envíos respectivos.\r\nAl finalizar verificar en las carpetas anteriormente mencionadas (DIJ y HTML), que se haya generado los archivos con la cantidad indicada en el archivo base del proceso (Sample_Cartera_UNE.txt)\r\n\r\n', 180, 221),
(165, 'Ingreso al Servidor de Pitney', 'Ya con los insumos generados, ingresamos al servidor MEDPITNEY05 (10.69.43.105), con las mismas credenciales que se ingresan a los servidores Pitney 116 y 118.\r\nMediante el FileZilla conectarse al servidor Facbilli y transferir desde la conexión remota de la 44 los archivos DIJ y HTML generados.\r\n', 180, 222),
(166, 'Transferencias en FileZilla', 'Luego transferir a la carpeta F:\\vendors\\Envio_UNE\\OutProfiles\\envioUNE_Cartera\\html\\ del servidor 05 los archivos correspondientes y dejar por fuera el archivo dij, para validar que se encuentren todos los HTML y no se inicie el envío con los archivos incompletos.', 180, 223),
(167, 'Transferencias en FileZila 2', 'Ya validado que los archivos estén completos mover el archivo dij a su carpeta correspondiente y mediante el Microsoft SQL Server editar los campos de fecha y mes para realizar la consulta de los envíos.', 180, 224),
(168, 'Generación de Reporte y Finalización', 'Generar un reporte de los envíos exitosos y erróneos, el cual debe adjuntarse al responder el correo de solicitud enviado por Cindy Yohana Bustamante Franco (Cindy.Bustamante@asesor.une.com.co)', 180, 225),
(169, 'Inicio', 'Se debe ingresar a la maquina 10.69.32.117.\r\nAbrir la herramienta Microsoft SQL Server Management Studio.\r\nConectarse a la Instancia MEDPITNEY02\\SQL2005.\r\nAbrir una Hoja de Query sobre la Base de Datos EMESSAGING.\r\n', 181, 226),
(170, 'Segundo paso', 'Actualmente se copia un archivo JRN de los envíos realizados por ciclo. Dicho archivo se copia en la maquina 10.69.32.117 en la ruta F:\\ENVIOS y tiene el siguiente nombre Gestion_Envios_Email_20160702_E1.JRN (Esta tarea se realiza en este momento por el operador Pitney que lanza el ciclo). Este mismo archivo debe ser copiado en la maquina anteriormente mencionada, pero en la siguiente ruta F:\\ENVIOS\\EntradaOperadores, una vez finalice el envió de facturas electrónicas.\r\nCuando el archivo se encuentre en la ruta ejecutamos las siguientes sentencias en la Base de Datos de EMESSAGING \r\n', 181, 227),
(171, 'Tercer Paso', 'Para exportar el resultado en EMESSAGING debemos realizar los siguientes pasos:\r\nClick derecho + Save Results As… + Darle el nombre al archivo (Gestion_Mail_AAAAMMDD) + Save.\r\n(Nota), este archivo es fundamental para iniciar con la búsqueda de las cuentas de correos que estén erradas.\r\n', 181, 228),
(172, 'Detalle Procedimiento Identificación y Corrección de Correo', 'La salida del archivo del proceso anterior, contiene el siguiente formato:', 181, 229),
(173, 'De seguido....', 'Una vez exportado el archivo de (Gestion_Mail_AAAAMMDD) guardarlo en el equipo local. Lo  abrimos con Excel y delimitamos por comas seleccionando la primera columna (A) (Identificacion), debemos tener en cuenta que en la estructura del correo algunas cuentas vienen con comas, por tal motivo al estar delimitado por comas lo que este después de esta lo colocará en una columna siguiente, \r\nEjemplo: En la siguiente imagen podemos observar que las cuentas de correo tiene una coma en esta.\r\n', 181, 230),
(174, 'Guardar', 'Cuando lo hayamos delimitado en Excel debemos filtrar la última columna (G) y validar las cuentas que presentan este inconveniente.', 181, 231),
(175, 'Búsqueda', 'Una vez identificadas estas cuentas buscamos nuevamente en el EMESSAGING (base de datos), Ejecutamos de la siguiente consulta, la cual nos arrojara la información que buscamos y las reemplazamos en el archivo de Excel Gestion_Mail_AAAAMMDD.\r\n\"select mail from dbo.Jrn_envio_cp WHERE cuenta in(\'8911902240\',\'8918988659\',\'8911632091\');\"\r\n', 181, 232),
(176, 'Cuarto Paso', 'Después de la columna (F) Mail en la columna (G) colocamos el siguiente parámetro =\"\'\"&A2&\"\',\" Luego arrastramos el parámetro hasta el final de la fila, para así tener el campo identificación dentro de los caracteres de comillas simples (‘‘) y terminados en coma (,)', 181, 233),
(177, 'Quinto Paso', 'También debemos tener en cuenta que en la columna (B) Cuenta, varias veces la cuenta viene de 9 Dígitos por lo que debemos hacer lo siguiente: Seleccionamos toda la Columna Cuenta (B), en el formato de celda seleccionamos Más formatos de números…, luego seleccionamos Personalizada  y en el campo Tipo colocamos diez ceros (0000000000) y damos Aceptar.', 181, 234),
(178, 'Importante', 'Una vez tengamos las identificaciones con las comillas simples (‘‘), terminados en coma (,) y las cuentas con los 10 dígitos,  el archivo generado debemos guardarlo como un Libro de Excel.', 181, 235),
(179, 'MYSQL QUERY BROWSERY', 'Abrir localmente el Programa MySql Query Browser. (Instalar programa)\r\nhttps://drive.google.com/file/d/0BzmMJOf8MpYLTnJIbHd6S2piUlk/view?usp=sharing\r\nConfigurar la conexión a BD con los siguientes datos:\r\nServer Host: 10.69.44.95 \r\nPort: 3306\r\nUsername: ope.billing \r\nPasword: Tigo2016$\r\nDefault Schema: facturacion\r\n', 181, 236),
(180, 'Sexto Paso', 'Una vez conectado Debemos lanzar la siguiente consulta: SELECT Numero_Id, Nombre_Completo,  Celular_1, Celular_2, Email_1, Email_2 FROM facturacion.reconocer_tigo WHERE numero_id IN (\'IDENTIFICACIONES\',) and Email_1 is not null and Email_2 is not null and Celular_1 is not null and Celular_2 is not null;\r\nDonde debemos incluir el valor del export de la columna Identificación (dentro del paréntesis y borramos la última coma. La consulta debe quedar de esta forma\r\n', 181, 237),
(181, 'Séptimo Paso', 'Una vez termine de cargar toda la información presionamos las teclas (Ctrl + Enter)   o damos  click en el botón   (Execute the entered query).', 181, 238),
(182, 'Octavo Paso', 'Una vez termine el proceso como vemos en la imagen debemos exportar el resultado de la siguiente forma:\r\nClick derecho + Export Resultset + Export As CSV File… + Guardar\r\n', 181, 239),
(183, 'Aclaración', 'Cabe aclarar que no todas las identificaciones se localizaran en esta BD. Y si es ubicado no necesariamente obtendremos una cuenta de correo.', 181, 240),
(184, 'Noveno Paso', 'Una vez exportado el archivo de BD_AAAAMMDD.cvs  lo abrimos con Excel y delimitamos por comas seleccionando la primera columna (Numero_Id) guardarlo como BD_AAAAMMDD.xls en el equipo local. \r\nCon el archivo generado en el punto 2 cruzamos la información con el archivo base de errores por el campo Identificacion, debemos realizar las siguientes tareas:\r\nCopiamos la información del archivo BD_AAAAMMDD.xslt y la pasamos a una Hoja en el archivo Gestion_Mail_AAAAMMDD.xls\r\n\r\n', 181, 241),
(185, 'Décimo paso', 'Vamos a la hoja de Gestion_Mail_AAAAMMDD y con la columna (A) identificación consultamos en la hoja  BD_ AAAAMMDD en un rango específico los correos que tengamos de esos clientes. En la última columna (H) de la hoja Gestion_Mail_AAAAMMDD colocamos el siguiente parámetro =CONSULTAV(A2|BD_AAAAMMDD!A$2:F$134|5|FALSO) siendo \r\nA2  La primera identificación de la columna (A) Identificación de la hoja Gestion_Mail_ AAAAMMDD\r\nBD_AAAAMMMDD  La hoja de errores exportada de la base de datos BD_ AAAAMMDD\r\nA$2:F$134 El rango tomado de la hoja de errores exportada de la base de datos BD_ AAAAMMDD (Primer dato de la columna (A) Numero_Id  y el ultimo de la columna (F) Email_2)\r\nEl campo de la hoja BD_ AAAAMMDD   columna (E) donde están los Email_1\r\nFALSO  Para que muestre el registro exacto que deseamos ver. \r\n', 181, 242),
(186, 'Décimo primer paso ', 'Luego volvemos a hacer la misma consulta, en la última columna (I) de la hoja Gestion_Mail_AAAAMMDD colocamos el siguiente parámetro =CONSULTAV(A2|BD_AAAAMMDD!A$2:F$134|6|FALSO). Lo único que cambiamos en la consulta es el campo sobre el cual vamos a comparar en este caso sería en el (campo 6) Email_2\r\nEl campo de la hoja BD_ AAAAMMDD   columna (F) donde están los Email_2\r\n', 181, 243),
(187, 'Aclaración número 2', 'Para office 2010 se utiliza la Función BUSCARV \r\n=BUSCARV(A2|BD_AAAAMMDD!A$2:F$134|6|FALSO)\r\nPara office 2013 se utiliza Función CONSULTAV\r\n=CONSULTAV(A2|BD_AAAAMMDD!A$2:F$134|6|FALSO)\r\n\r\nAhora en la hoja de Gestion_Mail_AAAAMMDD hacemos la comparación de la columna (F) Mail  con la columna (H) Email_1 en la última columna (J) colocamos el siguiente parámetro =MAYUSC(F2)=MAYUSC(H2) siendo \r\nF2  El primer correo electrónico de la columna (F) Mail de la hoja Gestion_Mail_ AAAAMMDD\r\nH2  El primer correo electrónico de la columna (H) EMail_1 de la hoja Gestion_Mail_ AAAAMMDD\r\n', 181, 244);
INSERT INTO `articulo` (`idarticulo`, `nombre_articulo`, `descripcion_articulo`, `proceso_idProceso`, `imagen_idimagen`) VALUES
(188, 'Décimo segundo paso', 'volvemos a hacer la misma consulta, en la hoja Gestion_Mail_AAAAMMDD hacemos la comparación de la columna (F) Mail  con la columna (I) Email_2 en la última columna (K) colocamos el siguiente parámetro =MAYUSC(F2)=MAYUSC(I2) siendo\r\nF2  El primer correo electrónico de la columna (F) Mail de la hoja Gestion_Mail_ AAAAMMDD\r\nI2  El primer correo electrónico de la columna (I) EMail_2 de la hoja Gestion_Mail_ AAAAMMDD\r\n', 181, 245),
(189, 'Décimo tercer paso', 'Si en el Comparativo_1  nos da como resultado VERDADERO es que el correo electrónico que exportamos en el archivo BD_AAAAMMDD es igual al correo electrónico que habíamos exportado en el archivo Gestion_Mail_AAAAMMDD,  por lo tanto  validamos si en el Comparativo_2, el correo electrónico nos da como resultado FALSO, si es así reemplazamos el correo electrónico que tenemos en el Comparativo_2. Si el resultado es FALSO, reemplazamos el correo electrónico por el que tenemos en el Comparativo_1.\r\nLos correos electrónicos que nos den como resultado VERDADERO en el Comparativo_1  y Comparativo_2 son correos electrónicos que son iguales a  los del Archivos Gestion_Mail_AAAAMMDD. \r\nLos del Archivos Gestion_Mail_AAAAMMDD  y BD_AAAAMMDD los guardamos en la siguiente ruta:  \r\n', 181, 246),
(190, 'Inicio', 'Para generar el reporte de tiempos se utiliza la herramienta Control-M Reporting Facility, la cual se encuentra anclada en la barra de tareas o el acceso directo que se ubica en el escritorio.\r\n\r\nSe utilizan las siguientes credenciales para el logeo:  \r\nUsuario: CT1017139487\r\nContraseña: Tigo2015\r\n\r\nPara generar el reporte se utiliza la plantilla: TIEMPOS_BILLING, la cual ya tiene los parámetros (JOB) definidos para generar el reporte. Los cambios que deben realizar son los siguientes:\r\nTítulo del Reporte  Cambiar la fecha del ciclo en el formato establecido CC/MM/AAAA.\r\n\r\nAsunto  Indica a que proceso consolidado corresponde el reporte e igualmente la fecha debe ir en el formato CC/MM/AAAA\r\n\r\nSiguiente  Para pasar a la siguiente página.\r\n', 182, 247),
(191, 'Segundo Paso', 'Fecha -> Corresponde al rango en el que se ejecutó los procesos correspondientes a Billing (Si finaliza el mismo día de ciclo, es el mismo rango de inicio y fin.)\r\nTiempo -> Corresponde a las horas que tomaron los procesos para ser ejecutados (Si terminó el mismo día de Ciclo, dejar como aparece en la imagen e referencia)\r\n\r\nCampos de filtro ->  Corresponde a los Job que globaliza el reporte Billing (Inicia con Pre-Billing y Finaliza con Ítem vs Bill)\r\n\r\n\r\nFinalizar -> Como lo indica finaliza la validación y genera el reporte.\r\n', 182, 248),
(192, 'El reporte', 'EL reporte generado aparece justo al lado de las plantillas generadas con toda la información de ejecución en segundos.', 182, 249),
(193, 'Guardar', 'Para guardarlo seguimos los siguientes pasos:\r\nDar clic en File\r\nEn el desplegable elegir la opción Export\r\n', 182, 250),
(194, 'Tercer Paso', 'En el cuadro de dialogo buscar la siguiente ruta donde deben almacenar los reportes Billing  D:\\TigoUne\\Reporte Semanal y Mensual\\2016\\Marzo\\BILLING\r\n\r\nYa ubicados allí, guardar el reporte en formato PDF con el nombre  Billing_Ciclo_ccmmaaaa.pdf.\r\n', 182, 251),
(195, 'Inicio', 'Para el reporte de invoice se siguen los mismos pasos que en el reporte de Billing, con las siguientes modificaciones:\r\nPlantilla TIEMPOS INVOICE\r\nTiempo -> en el inicial se coloca promedio de finalización del proceso Ítem vs Bill (Eje. El proceso Ítem vs Bill finaliza a las 11:10 a.m. Se recomienda colocar a las 11:00 para garantizar que la información se genere completa.)\r\ncampos de Filtro -> Van desde Cifras control antes de Invoice hasta los SMS transferencia XSLT total y SMS transferencia muestras.\r\nClic en finalizar para generar el reporte.\r\nRealizar el guardado como se realiza el Billing pero con el nombre Invoice_Ciclo_ccmmaaaa.pdf en la ubicación -> D:\\TigoUne\\Reporte Semanal y Mensual\\2016\\Marzo\\INVOICE\r\n', 183, 252),
(196, 'Seguido....', 'Seguido del inicio, se complementa con la siguiente imágen', 183, 253),
(197, 'Ejecución', 'Para el reporte de Pitney deben tener muy presente el Ciclo y el servidor donde se ejecuta el proceso, así como las fechas de proceso.\r\nEj. -> Dado que los Ciclos 18 y 19 se ejecutan en la misma maquina MEDPITNEY01 (10.69.34.116), se generarán dos reportes así:\r\nEL primero que va desde el día de Ciclo (Siempre y cuando el proceso incie ese día) hasta las 23:59\r\nEl segunda va desde las 00:00 día siguiente hasta la hora de ejecución del SMS fin Pitney.\r\n\r\nSe utilizan las siguientes plantillas:\r\nTIEMPOS PITNEY 116\r\nTIEMPOS PITNEY 118\r\n\r\nEl proceso de generación es exactamente el mismo que se utiliza para las plantillas Billing e Invoice, con la diferencia mencionada en el literal 1.\r\n\r\nPara el guardado de los reportes se realiza así:\r\nFile -> Export -> D:\\TigoUne\\Reporte Semanal y Mensual\\2016\\Marzo\\PITNEY\r\nSi son dos reportes -> Pitney_ccmmaaaa_1.pdf y Pitney_ccmmaaaa_1.pdf\r\n', 184, 254),
(198, 'Cuentas a tener presentes', 'CUENTAS PROCESADAS SEMANAL MARZO.xlsx\r\nConversor Tiempos.xlsx\r\nTIEMPO TOTAL X CICLO SEMANAL MARZO.xlsx\r\nCUENTAS PROCESADAS MENSUAL MARZO.xlsx\r\nTIEMPO TOTAL X CICLO MENSUAL MARZO.xlsx\r\n', 185, 255),
(199, 'LLenado', 'Cifras procesadas -> Se toma del monitoreo del portal Billing (Billing e Invoice), para las xslt transferidas ver el mail (Validación XSLT PITNEY vs BRM CICLO) y comparar con el export.\r\n\r\nTiempos -> Para los tiempos de Billing y Pre ajustes se toma de la plantilla Billing_Ciclo_ccmmaaaa.pdf, se toma del campo Run Time (sec), se abre la plantilla Conversor Tiempos.xlsx  pestaña Conversor -> Conversor a Horas -> debajo del “60” se ingresa los segundos del campo Run Time (sec) y se van llenando en el archivo CUENTAS PROCESADAS SEMANAL MARZO.xlsx al ciclo y proceso que corresponda\r\nPara los tiempos del Invoice y export se toma la plantilla Invoice_Ciclo_ccmmaaaa.pdf y se siguen los mismos pasos que se indican para Billing y Pre ajustes.\r\n\r\nPara los tiempos de Pitney se toma la plantilla Pitney_ccmmaaaa.pdf se toma el total del campo Sum of \'Average Runtime (Sec)\' field: se realiza lo siguiente:\r\na.	 se colocan los segundos debajo del 60 y se llena el cuadro tiempo total Pitney el campo Total Pitney con la hora convertida.\r\n\r\nEn el reporte Pitney se toman los segundos (Run Time (sec)) del proceso transferir_arc_carga_computec y se transforman en horas para colocarlos en la tabla Tiempo parcial fin-entrega campo Hora fin trans carga, luego buscar en el correo la entrega a Computec de carga o impresión y la que mayor tiempo tenga se debe llenar en el campo Hora envió a comp así:\r\nHH:MM:60 -> Como no se puede medir en el correo los segundos que se toma el correo para salir de la bandeja, se redondea el valor para hacer el tiempo más exacto.\r\nLuego de obtener el total se llena en el archivo CUENTAS PROCESADAS SEMANAL MARZO.xlsx  en el campo correspondiente a Pitney.\r\n', 185, 256),
(200, 'Redondear Horas', 'Luego de tener el archivo CUENTAS PROCESADAS SEMANAL MARZO.xlsx con las cifras, se procede a copiar la información a la plantilla Conversor Tiempos en la Hoja TIEMPOS REF…\r\nAutomáticamente en la Hoja REDONDEO HORAS aparecerá todo lleno y con las respectivas horas redondeadas.\r\nLuego el total redondeado que aparece en HORAS de la HOJA REDONDEO HORAS, se transcribe a la plantilla TIEMPO TOTAL X CICLO SEMANAL MARZO.xlsx en la hoja TIEMPO TOTAL x CICLO\r\n', 185, 257),
(201, 'Finalizando', 'para finalizar se anexan las siguientes imágenes', 185, 258),
(202, 'Inicio', 'Este se genera para 4 áreas a saber:\r\nBILLING\r\nDWH\r\nOPERACIONES T.I\r\nOPERACIONES\r\nEn el plan de producción ingresar al ítem REPORTES  GENERAR REPORTES ->REPORTE MENSUAL DIARIO.\r\nAllí colocar la fecha de los rangos de la semana que deben consultar, acto seguido al generar los reportes descargar los PDF de las áreas así:\r\nBILLING -> REPORTE MENSUAL DIARIOS (BILLING)\r\nDWH -> REPORTE MENSUAL DIARIOS (DWH)\r\nOPERACIONES T.I -> REPORTE MENSUAL DIARIOS (OPERACIONES_TI)\r\nOPERACIONES -> REPORTE MENSUAL DIARIOS (OPERACIONES)\r\nYa con los PDF por área descargada, abrir la plantilla R.M.D.CICLO V2.xlsx y llenarla con los totales de cada área en los cuadros e ítem asignados\r\nCuando se tenga todo lleno se le envía a Lorena en un correo las siguientes plantillas para ella completar las gráficas y enviarle a Diana Salamanca el Informe:\r\nCUENTAS PROCESADAS SEMANAL MARZO.xlsx\r\nTIEMPO TOTAL X CICLO SEMANAL MARZO.xlsx\r\nR.M.D.CICLO V2.xlsx\r\n', 186, 259),
(203, 'Además...', 'Para el mensual basta con consolidar la información ya recogida en los semanales y enviarle a Lorena las siguientes plantillas:\r\nCUENTAS PROCESADAS MENSUAL MARZO.xlsx\r\nTIEMPO TOTAL X CICLO MENSUAL MARZO.xlsx\r\nR.M.D.CICLO V2.xlsx  Se debe solicitar en las fechas el mes completo y llenarlo igual que el semanal.\r\n', 186, 260),
(204, 'Recursos para iniciar', 'Servidor 10.69.34.60 (INFRANET), Usuario pin	\r\nRutas a utilizar:\r\n/infranet/pin/invoice_dir/DISPUTA\r\n/infranet/pin/invoice_dir/DISPUTA/BKP\r\n', 187, 261),
(205, 'Pre requisitos', 'Luego de haber realizado el proceso de Regeneración de las facturas, se recibe un correo por parte del especialista de Billing indicando realizar el Export y luego generar el o los pdf.', 187, 262),
(206, '1)Generación export', 'Abrimos conexión Putty en el servidor 10.69.34.60 usuario pin\r\nNos ubicamos en la ruta:  /infranet/pin4/7.3/apps/pin_inv\r\n\r\nEjecutamos la siguiente la siguiente instrucción: pin_inv_export -verbose -start  08/08/2009 -end 08/08/2009\r\n\r\nLuego de finalizado el Export nos ubicamos en la siguiente ruta: /infranet/pin/invoice_dir \r\n\r\nEstando ubicados en la ruta indicada en el paso 3, procedemos a digitar el comando ll -rt | grep xslt dicho comando nos mostrara todas las xslt que fueron generadas por el export.\r\n\r\n', 187, 263),
(207, '2)INCLUIR DISPUTA A LAS XSLT', 'Las disputas estan ubicadas en el tag BILLINFO(0)  campo 12 (“0”), tal como se indica en la siguiente linea.\r\nBILLINFO(0)=\"170\"|\"B1-541992032\"|\"1339304400\"|\"1341896400\"|\"1343710800\"|\"20120713\"|\"20120610\"|\"20120710\"|\"20120731\"|\"2524\"|\"127216\"|\"0\"|\"129740\"|\"INMEDIATO\"\r\nLas xslt exportadas en el servidor 10.69.34.60, no tiene dicho campo, por tal motivo se debe proceder a colocarlo de manera manual, ya que este campo es indispensable a la hora de generar el pdf.\r\nEn el caso que el export solo genere una xslt, el procedimiento es muy fácil, pero en caso el export genere 5 o más xslt, esta tarea de colocar a cada xslt el campo disputa, se vuelve algo tedioso.\r\n\r\nPara ahorrar tiempo en colocar el campo de Disputa a “X” cantidad de xslt vamos a seguir los siguientes pasos:\r\n', 187, 264),
(208, 'Paso 1', 'Copiar las xslt exportadas al directorio: /infranet/pin/invoice_dir/DISPUTA', 187, 265),
(209, 'Paso 2', 'Ya con las xslt(en este caso 39), procedemos a colocar el campo DISPUTAS, para esto ejecutamos la Shell  sh  incluir_disputa.sh,  la cual está ubicada en la ruta  /infranet/pin/invoice_dir/DISPUTA .\r\nEl resultado debe ser el siguiente:\r\nSe debe tener presente que dependiendo de la cantidad de xslt, es la cantidad de carpetas generadas.\r\n', 187, 266),
(210, 'Paso 3', 'Para este caso existen xslt que tiene el mismo número de cuenta, el cual corresponden a diferentes meses, por tal motivo a la hora de generar el pdf, no se podría generar al mismo tiempo.\r\nPor tal motivo se crean lotes de carpeta para que estas xslt que tiene numero de cuentas que son repetidas, queden en directorios diferentes, tal como lo muestra en la siguiente imagen.\r\n', 187, 267),
(211, 'Paso 4', 'Si observamos la siguiente xslt, 17630760260_B1-380151829.xslt está en los 6 directorios, es decir que tiene el mismo número de cuenta, pero las xslt’s son de diferentes meses.\r\nYa después de haberse creado las carpetas con las respectivas xslt a procesar, procedemos a pasar lote por lote las carpetas al servidor de Pitney para generar los respectivos PDF.\r\n', 187, 268),
(212, 'Posibles errores', 'Si al ejecutar la Shell no existan xslt en la ruta /infranet/pin/invoice_dir/DISPUTA genera el siguiente mensaje :', 187, 269),
(213, 'O en otro caso...', 'En caso que una o varias xslt ya tengan el campo disputas, genera el siguiente mensaje:', 187, 270),
(214, 'Paso 1', 'Cuando en la carpeta work encontramos archivos con error, procedemos a buscar el JRN que se copia en Vault, identificamos el número con la cual está identificada esta carga.', 188, 271),
(215, 'Paso 2', 'Una vez identificado este número nos vamos para el disco E, entramos a la carpeta pagedata, realizamos la búsqueda con el número que identificamos en el paso 1. Hay nos salen los archivos drp y jrn relacionados con la carga que presento error', 188, 272),
(216, 'Paso 3', 'El archivo jrn se copia y se lleva a la carpeta process (disco D/PBBI_CCM/Vault/Server/Process), este archivo le cambiamos la extensión .jrn por .remove', 188, 273),
(217, 'Nota y Finalización', 'Este archivo que copiamos en la carpeta process se procesa automáticamente y se traslada para la carpeta work (disco D/PBBI_CCM/Vault/Server/Work) en donde se realiza el proceso para descargar el archivo.\r\nNOTA: Es de anotar que si la carga tiene particiones, el proceso se debe de realizar con cada una de las particiones.\r\n', 188, 274),
(218, 'Inicio', 'Cuando estamos realizando envíos masivos de algún ciclo y por cualquier motivo este deja de hacer envíos masivos, procedemos a realizar lo siguiente:\r\n\r\nComo ejemplo se va a tomar el ciclo 06 de ENERO de 2016.\r\nLa falla en él envió se generó en el profile 1 el cual en total para envió tenía 2091, de los cuales solo alcanzo a enviar 255 correos masivos, esto se debió posiblemente a una pequeña desconexión con el Emessanging.\r\n', 189, 275),
(219, 'Verificación ', 'Como se puede apreciar en la imagen, el ultimo envió fue realizado a las 09:36 del día 01-13-2016, si durante 15 minutos realizan la consulta y este arroja la misma cantidad antes consultada, debemos realizar las siguientes validaciones :\r\nSe debe verificar en la siguiente ruta: F:\\Envio_Cmovil\\OutProfiles\\envioEmailX\\dij (envioEmailX) = Se  verificar según el número de profile que haya fallado.\r\n\r\nSe debe validar en la carpeta (dij), la existencia del archivo TigoEmailX.jrn, en caso que el archivo NO se encuentre en la ruta, este no va a continuar enviando los correos de manera masiva, y por tal motivo él envió del profile no se va a enviar completo.\r\n\r\nNota: Se debe tener presente que en la ruta: F:\\Envio_Cmovil\\OutProfiles\\envioEmailX\\html debe también existir el archivo TigoEmailX.html\r\n\r\n', 189, 276),
(220, '¿Qué debemos hacer para enviar la información que falta del profile que nos ha fallado?', 'Como primera opción podemos realizar lo siguiente: \r\n\r\nIr a la ruta F:\\Tigo_Edelivery\\Doc1\\Salidas\\Email\\CDD_MMDDAAAA_E#   del servidor donde está la información (10.69.34.116 ó 10.69.34.118)\r\n\r\nYa ubicado en la ruta proceda a buscar el archivo TigoEmailX.jrn, (recuerde que la X corresponde al número del profile que falló).\r\n\r\n\r\nLuego de identificado el archivo TigoEmailX.jrn procedemos a copiarlo a la ruta:\r\n\r\nF: \\Envio_Cmovil\\OutProfiles\\envioEmailX\\dij, recuerde que se debe colocar el archivo en la ruta del servidor en el cual se está procesando (10.69.34.13 ó 10.69.43.121).\r\n\r\nAl copiar este archivo a la carpeta antes indicada (dij), este automáticamente inicia el proceso de envío masivo. (Para continuar con la explicación de esta parte voy a utilizar el ciclo 6.)\r\n\r\nPara el envío del ciclo 6 solo se enviaron 255 correos masivos y al verificar en la carpeta dij no estaba el archivo TigoEmailX.jrn , por lo cual se procedió a realizar el reproceso, es decir se buscó el archivo TigoEmailX.jrn y fue copiado en la carpeta dij , este automáticamente inicio el envío total es decir los 2091  facturas electrónicas, pregunta : ¿ qué paso con los 255 envíos que ya se habían realizado antes?, R:/ Los 255 envíos realizados anteriormente, el proceso los identifica por medio de un ID_INSTANCE, que no permite que estos correos sean enviados nuevamente, y en el log  escribe que esa instancia ya fue enviada anteriormente y en la base de datos lo marca como CONTENT_FAILURE(este estado indica que no fue enviado.)  \r\n\r\nPara entender mejor esta parte, vamos a explicarlo con las dos siguientes imágenes: \r\n\r\nComo se puede apreciar en la imagen 1 las primeras 8 líneas nos indica que los envíos desde la herramienta Emessaging salieron exitosos.\r\n', 189, 277),
(221, 'En tal caso ....', 'Si nos centramos en la línea 1 de la imagen 1 podemos observar que en el estado dice Delivery Successfully, es decir que el envío desde la herramienta Emessaging salió exitoso, mientras que esta misma línea 1 de la segunda imagen en el estado dice: CONTENT_FAILURE,\r\nY si apreciamos la siguiente imagen que hace relación al log, indica que la instancia ya fue procesada, por tal motivo no realiza el reenvió de esta información.\r\nTambién observemos en la imagen 2 que el total de la consulta realizada es de 2091, es decir que a pesar que realizamos el reproceso de esta manera, los 255 que se enviaron en la primera ejecución, no los envío, y la segunda parte que eran 1836 que debió haberla enviado, no los envío.\r\n', 189, 278),
(222, 'Ejecución', 'Como al realizar el reproceso de esta forma, los resultados no fueron los esperados, vamos a realizar el siguiente que es el más efectivo:\r\n\r\nSe realiza la consulta de los correos que salieron de la herramienta de Emessanging de manera exitosa.\r\nEn este caso fueron 255 correos que salieron exitosamente, de esta consulta, nos interesa saber, cual fue el último correo enviado, para este ejemplo el ultimo enviado fue: 8902953906 aranavaspaso@hotmail.com, tal como se ilustra en la siguiente (imagen 3).\r\n', 189, 279),
(223, 'Identificación', 'Ya con esta información procedemos a identificar a partir de donde vamos a iniciar los reenvíos de los correos faltantes, para esto procedemos a realizar lo siguiente:\r\n\r\nIr a la ruta F:\\Tigo_Edelivery\\Doc1\\Salidas\\Email\\CDD_MMDDAAAA_E#   del servidor donde está la información (10.69.34.116 ó 10.69.34.118)\r\n\r\nYa ubicado en la ruta, proceder a buscar el archivo TigoEmailX.jrn, (recuerde que la X corresponde al número del profile que falló).\r\n\r\nAbrimos el archivo TigoEmailX.jrn con notepad ++, ya abierto el archivo pulsamos al mismo tiempo la combinación de teclas Ctrl + F. se abre el siguiente cuadro de dialogo, en la opción buscar digitamos o pegamos el número de cuenta o cuenta de correo electrónico a buscar, en este caso seria 8902953906 ó aranavaspaso@hotmail.com\r\n', 189, 280),
(224, 'Resultado de Búsqueda', 'El resultado de la búsqueda es el siguiente, en la marca amarilla y verde podemos apreciar que tenemos también identificado el número de cuenta y la cuenta de correo electrónico, ahora nos vamos a centrar en el primer y último correo electrónico que están en las líneas 4067 y 4099 de la imagen acá presente.', 189, 281),
(225, 'Otro resultado', 'Si apreciamos las imágenes del JRN(arriba) y el resultado de la consulta(abajo) la cuenta de correo electrónico tiene el mismo orden consecutivo, es decir, se observa que la cuenta faacturacion@3losandes.com y margarita@loveproducciones.com  en ambas imágenes están entre la cuenta aranavaspaso@hotmail.com ,  es decir tiene el mismo orden, por tal motivo con esta información ya sabemos que de la cuenta de correo margarita@loveproducciones.com hacia abajo debemos proceder a realizar el reproceso de los envíos.\r\n\r\n', 189, 282),
(226, '¿Qué pasos debemos seguir para realizar el Reproceso?', 'Como ya sabemos que desde la cuenta de  margarita@loveproducciones.com  hacia abajo falta por enviar los que realizaremos en lo siguiente:\r\n\r\nVamos a realizar la consulta a partir de la fecha y hora en este caso nos vamos a guiar a partir de la cuenta que no fue enviada.   margarita@loveproducciones.com  \r\n\r\nConsulta a utilizar en este caso:\r\n', 189, 283),
(227, 'La consulta', 'select \"customer_id\",\"sentTo\",\"outbound_message_status\" from emessaging.dbo.outbound_message where\r\nmessage_id is not null and date_send  >=\'2016-01-13 10:05:10.397\' and date_send <=\'2016-01-13 10:15:10.397\' and outbound_profile_id =\'5150\' order by date_send asc\r\n\r\nel resultado de esta consulta es la siguiente: \r\ncomo se observa en la imagen el resultado de la consulta es de 1836 registros, es decir los que al principio identificamos que faltan por enviar.\r\n', 189, 284),
(228, 'Exportar', '\r\nA continuación, vamos a exportar los datos a un archivo Excel de la siguiente manera:\r\nDamos click derecho en Save Result As  \r\n', 189, 285),
(229, 'Guardamos', 'Guardamos la información en la ruta de su preferencia', 189, 286),
(230, 'Ahora en excel', 'Luego de guardado el archivo procedemos a abrir el archivo en Excel. \r\n\r\nEste archivo abri la información en una misma columna,\r\n', 189, 287),
(231, 'Organización', 'vamos a proceder a organizarlo de la siguiente manera:\r\n\r\nSeleccionamos la columna A \r\nNos ubicamos en la pestaña Datos y damos click en Texto en Columnas \r\n', 189, 288),
(232, 'Resultados', 'Despues de haber dado siguiente, nos muestra la siguiente informacion, selecccionamos Tabulacion  y Coma\r\nComo podemos observar en la vista previa la informacion ya esta organizada, procedemos a dar click en Finalizar :\r\n', 189, 289),
(233, 'Resultado', 'El resultado dentro de excel sería el siguiente', 189, 290),
(234, 'SACAR XSLT PARA GENERAR EL DOC1READY_E.TXT', 'Ya con las cuentas procederemos a identificar y obtener las xslt para generar el Doc1Ready_E.txt\r\nPara obtener esta información se va a realizar el procedimiento indicado en el siguiente link :\r\nhttp://10.69.34.67/wiki/index.php/PDF:_Configuraci%C3%B3n_y_ejecuci%C3%B3n_del_Job_EXTRAE_XSLT_ERRORES_CICLO_en_control-m_para_la_extracci%C3%B3n_de_los_XSLT\r\na este link solo se puede ingresar si está conectado a la vpn o si está conectado en la red local de Colombiamovil\r\n', 189, 291),
(235, 'Ejecución', 'El procedimiento para genera el archivo Info_inscrito.csv es el siguiente:\r\nVolvemos al archivo Excel y borramos el encabezado y la 3ra columna\r\n', 190, 292),
(236, 'En excel', 'Nos ubicamos en la columna C y vamos a digitar o copiar la siguiente formula\r\n=CONCATENAR(A1|\";\"|B1|\";\"|\"1\"|\";\"|\"1\") \r\nNota: En mi equipo el Excel está configurado por Pipe (|), en su Excel por defecto se utiliza punto y coma(;), es decir que los pipe(|)  los vamos a cambiar por punto y coma(;).\r\nEl resultado final es el siguiente:   =CONCATENAR(A1;\";\";B1;\";\";\"1\";\";\";\"1\") , esta formula la replicamos en las demás filas y el resultado final sería este :\r\n', 190, 293),
(237, 'Notepad ++', 'Ya con la información procedemos a copiar la información que esta en este caso, en la columna C, \r\nAbrimos el Note pad ++ y pegamos la información.\r\nA nuestro nuevo archivo le vamos a agregar el siguiente encabezado:\r\n', 190, 294),
(238, 'Finalización ', 'Ya creado el InfoInscritos con nuestra información, procedemos a guardar el archivo con el nombre : INFO_INSCRITOS.csv , recuerden guardar el archivo en la ruta  F:\\Tigo_Edelivery\\Insumos del servidor  a utilizar (116 o 118), no sin antes renombrar el Info_Inscritos original.\r\nYa con nuestro info_inscritos procedemos a generar  el archivo Doc1Ready_E.txt,\r\nSe recuerda que esta información debe ser generada con los archivos Rtbilling del ciclo correspondiente.\r\nAl generar el archivo Doc1Ready_E.txt este debe tener la cantidad de cuentas a procederes decir 1836, si falta algúna cuenta se debe revisar con detalle, y en tal caso volver a generar el archivo.\r\nRecuerden también que al finalizar debe de dejar el archivo INFO_INSCRITOS original. \r\n', 190, 295),
(239, 'Ejecución', 'En caso que no haya  suficiente espacio en los servidores 10.69.34.118 – 10.69.34.116 de la unidad F: y no encuentren más información que borrar,\r\n\r\nProceder a realizar lo siguiente :\r\n\r\nEn la carpeta CDD_MMDDAAAA_E# la cual está ubicada en la ruta  F:\\TIgo_Edelivery se debe eliminar  los archivos que están seleccionados, según como se muestra en la imagen.\r\n\r\nEs decir solo deberían quedar los archivos  \r\n\r\ncuentas_link_ciclo_CDD_MMDDAAAA_E#.txt y Doc1Ready_E.txt\r\n', 191, 296),
(240, 'Paso 1', 'Buscar una referencia de pago al azar', 192, 297),
(241, 'Paso 2', 'Una vez identificada vamos al icono de búsqueda, pegamos la cuenta en la barra de buscar y le indicamos que se realice en el archivo actual de trabajo.', 192, 298),
(242, 'Paso 3', 'Acto seguido en la parte inferior nos mostrara la coincidencia tantas veces se encuentre en el archivo y la línea de ubicación:', 192, 299),
(243, 'Paso 4', 'Si hay duplicadas, se debe verificar nuevamente que todos los insumos estén ubicados correctamente:\r\nValidar que no existan XSLT duplicadas (si existe informar y enviarla mediante mail al especialista de soporte o al que haya solicitada la entrega adicional)\r\nii.	Insumos RTB sesten ubicados en los directorios asignados.\r\niii.	BD_errores_ccmmaaa.csv Si es ppal. Incluir las del archivo “CUENTAS A INCLUIR EN ERRORES CICLOS.xlsx“, junto a las que viajan desde exclusiones, si es adicional solo incluir las del archivo “CUENTAS A INCLUIR EN ERRORES CICLOS.xlsx“\r\nBD_muestras_ccmmaaaa.csv si es ppal. este con información, si es adicional en cero (0)\r\nBD_excluir_factura_ccmmaaaa.txt si es ppal. Que contenga información. Si es adicional y es enviada por el especialista en la solicitud de entrega colocarlo, en caso contrario dejarlo en cero (0)\r\nvi.	Volver a ejecutar el proceso y validar.\r\n', 192, 300),
(244, 'Detalles de Consumo', 'Detalles de consumo: para certificar que las facturas que correspondan tengan sus hojas de detalle se realiza de la siguiente forma:\r\n\r\na.	En el JRN buscamos cualquier cuenta al azar que lleve la leyenda “DETA”\r\n', 192, 301),
(245, 'Detalles de consumo-paso1', 'Ya con el AFP abierto con el editor Afpviewer van al menú Edit submenú Find…\r\n \r\n', 192, 302),
(246, 'Detalles de consumo-paso2', 'En el cuadro de dialogo a continuación ubicamos la información en los campos resaltados', 192, 303),
(247, 'Además en el paso 2....', 'se anexa la siguiente imagen ', 192, 304),
(248, 'Detalles de consumo-paso3', 'Damos doble clic para que nos lleve a la factura y navegamos en las páginas y certificamos que los detalles de consumo estén allí, como lo detalla el archivo JRN', 192, 305),
(249, 'Finalización ', 'Luego de esto se procede a realizar la entrega correspondiente.', 192, 306),
(250, 'Ejecución', 'Ubicarse en la ruta : /rtb_cdrs/data , allí crear el archivo BD_solicitud_muestras_mmddaaaa_AD.csv e incluir las cuentas a las cuales deseamos saber si tienen asociado un archivo RTBilling \r\nLuego de creado el archivo ejecutamos la Shell trans_rtb_pitney_AD.sh la cual está ubicada en la ruta /rtb_cdrs/sh \r\nModo de ejecución de la Shell :  trans_rtb_pitney_AD.sh  aaaammdd(día anterior al ciclo) aaaammdd(fecha del ciclo actual)\r\nLa Shell se encarga de buscar la información indicada en el archivo BD_solicitud_muestras_mmddaaaa_AD.csv contra los Detalle de Llamadas del ciclo correspondiente.\r\nLuego de encontrar la información correspondiente guarda los archivos en la carpeta RealTimeBilling_mmddaaaa_AD, luego comprime esta carpeta y procede a transferirla a los servidores 10.69.34.116 y 10.69.34.118\r\nEl archivo  RealTimeBilling_mmddaaaa_AD.tar.gz  va a quedar en la siguiente ruta :  F:\\TIgo_Edelivery\\Archivos_mmddaaaa_AD de ambos servidores.\r\n', 193, 307),
(251, 'En caso de que no haya información', 'En caso de no encontrar información genera el siguiente mensaje : ', 193, 308),
(252, 'Si está vacío', 'Si el archivo  BD_solicitud_muestras_mmddaaaa_AD.csv  no está, o está vacío genera el siguiente error : \r\n\r\nNota : Es muy  importante validar muy bien que las cuentas que va a almacenar en el archivo  BD_solicitud_muestras_mmddaaaa_AD.csv   SI  correspondan al ciclo que va a generar la entrega Adicional', 193, 309),
(253, 'Ejecución', 'Si al realizar el monitoreo de los buzones y observamos  que lleva más de media hora sin depurar automáticamente los correos fallidos, procedemos a realizar los siguientes pasos :\r\n\r\nAbrir conexión remota al servidor 10.69.34.13 y 10.69.34.117\r\nAbrir Services\r\n', 194, 310),
(254, 'Paso 1', 'Antes de reiniciar los siguientes servicios, debemos garantizar que en el momento no se estén realizando envíos, ya que estos reinicios, no permitiría enviar los correos con la facturación de manera exitosa. \r\n\r\nProcedemos a reiniciar el Apache Tomcat, como se observa en la imagen damos clic derecho y luego clic en Restart(se realiza el reinicio en ambos servidores 10.69.34.13 y 10.69.34.117)\r\n', 194, 311),
(255, 'Paso 2', 'Luego que haya reiniciado el ApacheTomcat y ya este arriba, procedemos a reiniciar el siguiente servicio IISAdmin Service, tal como lo indica en la siguiente imagen:\r\n(Se realiza el reinicio en ambos servidores 10.69.34.13 y 10.69.34.117)\r\n', 194, 312),
(256, 'Paso 3', 'Luego que haya reiniciado el IISAdmin Service y ya este arriba, procedemos a reiniciar el siguiente servicio Microsoft POP3 Service, tal como lo indica en la siguiente imagen:\r\n(Se realiza el reinicio en ambos servidores 10.69.34.13 y 10.69.34.117)\r\n\r\n', 194, 313),
(257, 'Paso 4', 'Luego que haya reiniciado el Microsoft POP3 Service y ya este arriba, procedemos a reiniciar el siguiente servicio Simple mail Transfer Protocol(SMTP), tal como lo indica en la siguiente imagen:', 194, 314),
(258, 'Finalización ', 'Luego de realizado los reinicios de los servicios indicados anteriormente,  nos ubicamos en cada una de la bandeja de entrega de los buzones de ERRORES y damos refrescar, en caso que no se vea que disminuye damos un tiempo de 30 minutos, en caso en este tiempo no se vea ningún avance, repetimos el procedimiento y reiniciamos los servidores 10.69.34.117 y 10.69.34.13, es de recordar que no debe estar realizando envíos.', 194, 315),
(259, 'Inicio', 'Como primera instancia debemos entrar al servidor donde se encuentra el ciclo que vamos a cargar, verificamos que en la carpeta Cdd_mmddaa_E# si se encuentren los doc1', 195, 316),
(260, 'Paso 2', 'Una vez verificado estos archivos nos vamos para la carpeta correspondiente al ciclo que se encuentra en carga', 195, 317),
(261, 'Paso 3', 'Entramos a la carpeta del ciclo correspondiente, verificamos la fecha de suspensión y se borran todos los archivos que se encuentran en esta, menos el comprimido. ', 195, 318),
(262, 'Paso 4', 'Una vez realizado el borrado nos vamos de nuevo a la carpeta Tigo_Edelivery, buscamos el proceso TIGO_CARGA_MSG.bat, lo abrimos por el Notepad\r\n', 195, 319),
(263, 'Paso 5', 'Ponemos el nombre correspondiente al ciclo y la fecha suspensión, apartir del ciclo 15 de agosto se coloca la fecha de suspensión y otra fecha la cual es según el ciclo un mes después menos un dia.', 195, 320),
(264, 'Paso 6', 'Le damos doble clik a TIGO_CARGA_MSG.bat y esperamos que reprocese los archivos de carga, vamos nuevamente a la carpeta de ciclo y verificamos que si estén los AFP y los JRN', 195, 321),
(265, 'Paso 7', 'Verificamos que el AFP si haya quedado con el emblema de Computec', 195, 322),
(266, 'Paso 8', 'Una vez verificado todo nos vamos para el servidor 10.69.34.133 (app/Tigo.2014!), en donde encontraran unas carpetas ya abiertas para poder interactuar más fácil. La conexión  carga es donde encontraran las conexiones a todos los servidores, de esta carpeta pueden seleccionar el servidor al que desean entrar\r\n', 195, 323),
(267, 'Paso 9', 'Seleccionar los dos archivos que se van a cargar y copiarlos en la carpeta download, tener en cuenta que los dos archivos no deben de superar 19GB en tamaño.\r\nALMC_TIGO_FACTURA_C##_DDMMAA_E#_300.AFP ALMC_TIGO_FACTURA_C##_DDMMAA_E#_300.JRN\r\n', 195, 324),
(268, 'Paso 10', 'Una vez copiados los archivos en download, este proceso es automático, en donde los organiza por tamaño y los transfiere a la carpeta work, en donde se procesan los archivos cargados', 195, 325),
(269, 'Paso 11', 'Se transfieren para la carpeta Work automáticamente.', 195, 326),
(270, 'Paso 12', 'Para validar que si este cargando, se le da refrescar y se podrá observar el aumento en el tamaño del archivo. En esta carpeta work si hay archivos que hayan cargado con error, el los mostrara con extensión .err\r\n\r\nSi los dos archivos a cargar superan los 19GB en tamaño\r\n', 195, 327),
(271, 'Paso 13', 'Se deben de copiar a la carpeta PARTIR. Para realizar este proceso no debe de haber nada cargando en work.', 195, 328),
(272, 'Paso 14', 'Se  selecciona el archivo JRN, se le da Fn+F2 y se copia el nombre completo.', 195, 329),
(273, 'Paso 15', 'Después se abre la ventana Command prompt, se le da fn+f3,Se borra el escrito ALMC_TIGO_FACTURA_C02_08022016_E1_300.JRN.\r\nCon el mouse se da clic izquierdo donde se encuentra el cursor y se le da clic a paste.\r\nUna vez realizado la copia se le da enter y el realiza inmediatamente la partición automáticamente.', 195, 330),
(274, 'Paso 16', 'En la carpeta PARTIR se podrá observar en cuantas partes se realizara la partición de los archivos, el cual ira procesando una a una. ', 195, 331),
(275, 'Paso 17', 'Una vez terminado se pude cerrar la conexión que realiza la partición.', 195, 332),
(276, 'Paso 18', 'Nos vamos para la carpeta PARTIR y borramos los tres primeros archivos.\r\nALMC_TIGO_FACTURA_C10_08102016_E1_300.AFP                                                                                                                          ALMC_TIGO_FACTURA_C10_08102016_E1_300.bat                                                                                                                          ALMC_TIGO_FACTURA_C10_08102016_E1_300.JRN\r\n', 195, 333),
(277, 'Paso 19', 'Una vez borrado estos archivos, le damos cortar a los archivos que fueron particionados y los trasladamos a la carpeta download. Hay que tener en cuenta que el archivo partir_AFP.jar nunca se toca.', 195, 334),
(278, 'Roles y Responsabilidades', 'El Especialista de facturación de interconexión y Roaming es el encargado de este proceso. Los usuarios de este cubo son los Especialistas del área de Roaming.', 196, 335),
(279, 'Pre requisitos', 'Primero Asegurar que los correos llamados \"Transferencia de logs proceso Cubos2, Transferencia de logs proceso Cubos3, Transferencia de logs proceso Cubos4, Transferencia de logs proceso cubo roaming incollect\" estén con fecha actual en su bandeja de entrada del Outlook.', 196, 336),
(280, 'Ejecución-Paso 1', 'El proceso inicia cuando a nuestro OUTLOOK de entrada llegan los cuatro correos de confirmación para proceder a ejecutar el cubo roaming del día como lo muestra el pantallazo.', 196, 337),
(281, 'Paso 2', 'A continuación procedemos a revisar cada uno de los correos recibidos y deben aparecer como lo muestra la imagen, los cuatro deben aparecer de la misma manera como se muestra el pantallazo.', 196, 338),
(282, 'Paso 3', 'Luego nos dirigimos donde tenemos guardado el archivo del CUBO ROAMING para comenzar su ejecución.\r\n\r\nEn este caso el archivo de Excel donde está el cubo roaming es en la RUTA EQUIPO-NUEVO VOL (F:)—BACKUP --- CARPETA LINA ----CUBOS---- Hay ya encontramos el Excel a ejecutar.\r\n\r\n', 196, 339),
(283, 'Paso 4', 'A continuación abrimos la carpeta cubos y nos dirigimos a cubo roaming abrimos nos dirigimos DATOS- ACTUALIZAR TODO.', 196, 340),
(284, 'Paso 5', 'Esperamos a que ejecute damos control G y vamos a archivo guardar como guardar en carpeta público y serramos el archivo.', 196, 341),
(285, 'Paso 6', 'Por ultimo enviamos un email a las siguientes personas y diciendo lo siguiente como lo muestra también el pantallazo:\r\n\r\nPara: Buritica, Sebastian <Sebastian.Buritica@tigo.com.co>; Castro, Carlos Alberto CarlosA.Castro@tigo.com.co\r\n\r\nCopia a: Cely, Lina <Lina.Cely@tigo.com.co>; Meneses, Juan <Juan.Meneses@tigo.com.co>; Roys, Dilia <Dilia.Roys@tigo.com.co>; Sanchez, Jorge Enrique Jorge.Sanchez@tigo.com.co\r\n\r\nAsunto: CUBO ROAMING\r\n\r\nBuenos días, \r\nEl cubo de Roaming quedó guardado en la carpeta público,\r\n\r\nCordial Saludo....', 196, 342),
(286, 'Roles y Responsabilidades', 'El Especialista de facturación de interconexión y Roaming es el encargado de este proceso. Los usuarios de este cubo son los Especialistas del área de Interconexión.', 197, 343),
(287, 'Pre requisitos', 'Primero Asegurar que los correos llamados \"Transferencia de logs proceso Cubos2, Transferencia de logs proceso Cubos3, Transferencia de logs proceso Cubos4\" estén con fecha actual en su bandeja de entrada del Outlook.\r\n\r\na.El procedimiento se inicia con el mes actual al que se va a realizar y se cambia día a día, cada día de cada mes guarda la información del mismo\r\n\r\nb.Asegurarse que la información obtenida sea validada tanto en los correos como en el momento de hacer el cambio a las fechas.', 197, 344),
(288, 'Validaciones iniciales', 'Ingresar al correo bandeja de entrada → Ir a los correos y revisar que la información dada sea correcta.\r\n\r\n', 197, 345),
(289, 'Validaciones Iniciales-2', 'Si al ingresar al correo aparece error no se puede ejecutar el paso siguiente de lo contrario proseguir.\r\nCuando el proceso comienza cada mes se adecua a la fecha actual cambiando así el mismo.', 197, 346),
(290, 'Desarrollo', 'Ingresar al programa PL/SQL Developer.→ Ingresar Username, se escribe inte →ingresar Password→******→ingresar Database→BILLING→Connect as→Normal→ OK.', 197, 347),
(291, 'Procedimientos de Pruebas', 'Nos dirigimos hacia la parte izquierda de la ventana a la pestaña con nombre procedures', 197, 348),
(292, 'Procedimientos de Pruebas paso 2', 'Damos doble clic en procedures y aparece así y damos clic en mes actual', 197, 349),
(293, 'Procedimientos de Pruebas paso 3', 'Damos clic derecho y vamos Edit', 197, 350),
(294, 'Obtener Información', 'Nos arroja una nueva ventana en la cual nos vamos a buscar la fecha anterior a la actual.', 197, 351),
(295, 'Obtener Información paso 2', 'Se debe poner comentarios a las sentencias de las vistas aún inválidas. Esto se hace un día antes de la fecha actual:', 197, 352),
(296, 'Ejecución', 'Después de este proceso damos la tecla F8 y esperamos a que en la parte inferior de la ventana nos arroje una bandera con color de azul, blanco, y negro que nos indica que el procedimiento se ha terminado.', 197, 353),
(297, 'Ejecución paso 2', 'Nos dirigimos nuevamente a _ PBD_CUBOTX:_ABRIL y damos clic derecho y vamos a la opción Test clic y damos la tecla F8', 197, 354),
(298, 'Ejecución paso 3', 'Nos arroja una nueva ventana a la cual se le da F8 esperamos a que nuevamente la bandera este completa con sus colores', 197, 355),
(299, 'Ejecución paso 3', 'Cuando el proceso termine nos dirigimos nuevamente a→ program window- edit sourcepbd_cubotx_abril.', 197, 356),
(300, 'Ejecución paso 4', 'Al inicio de todo el texto y subrayamos la parte donde dice →cdr_detall_detalladoabr→ sobre lo subrayado → damos clic derecho.', 197, 357),
(301, 'Ejecución paso 5', 'Nos dirigimos a la parte donde dice Query data y damos clic nos lleva a otra pantalla.\r\n\r\n', 197, 358),
(302, 'Ejecución paso 6', 'En esta parte lo que hay que proceder hacer es en la parte superior de la ventana que aparece sale una frase en la cual hay un (*) ese lo borramos y en ese mismo espacio escribimos distinct process_date', 197, 359),
(303, 'Ejecución paso 7', 'En el lugar donde está el * lo borramos y en ese mismo espacio escribimos distinct process_date y para finalizar todo el proceso damos la tecla F8', 197, 360),
(304, 'Ejecución paso 7', 'Automáticamente nos aparece una ventana con la fecha actual a la que lo estamos realizando.', 197, 361),
(305, 'Ejecución paso 8', 'Cerramos el programa lo abrimos nuevamente para hacer el siguiente procedimiento.', 197, 362),
(306, 'Ejecución paso 9 ', 'Aparece la ventana del archivo abierto\r\nA la parte izquierda de la ventana nos dirigimos a la pestaña donde dice VIEWS y damos doble clic', 197, 363),
(307, 'Ejecución paso 10', 'Al abrirlo aparece una ventana donde buscamos la pestaña donde dice VWCUBOICTV3', 197, 364),
(308, 'Paso 1', 'Damos cerrar al programa que estamos utilizando nos dirigimos a la carpeta donde tenemos guardado el Excel a correr.', 198, 365),
(309, 'Paso 2', 'El archivo lleva como nombre CuboIctv6 lo abrimos NOTA:¨ MIENTRAS SE CORRE EL CUBO, NO DEBE HABER NINGUN OTRO EXCEL ABIERTO¨.', 198, 366),
(310, 'Paso 3', 'Nos vamos a la parte superior de la ventana y damos clic en DATOS.', 198, 367),
(311, 'Paso 4', 'Procedemos a la parte donde esta actualizar todo damos clic y esperamos hasta que el procedimiento termine.', 198, 368),
(312, 'UBICACIÓN Y ENTREGA DEL CUBO DE INTERCONEXION.', 'Terminado el procedimiento damos control G para que se guarde el archivo y luego nos dirigimos a archivo GUARDAR COMO nos dirigimos a carpeta publico damos guardar y reemplazar GUARDAR', 198, 369),
(313, 'Anexo', 'Cuando uno termina el proceso hay que enviar un correo de esta forma indicando que el cubo se corrió y se guardó en la carpeta público y va dirigido a esas personas.', 198, 370),
(314, 'Procedimiento para Cargar Cronograma', 'Con base al Cronograma aprobado por cartera, se deberá crear el archivo \"cronograma.csv\" teniendo en cuenta las siguientes reglas:\r\n\r\nArchivo delimitado por ;\r\nEl archivo debe contener títulos o encabezados\r\nlas fechas deben estar en formato YYYYMMDD', 199, 371),
(315, 'Paso 2', 'Una vez creado el archivo con las caracteristicas, se debe subir al servidor 10.69.34.91 (fpp021) en la ruta:\r\n\r\n/Repositorio/shell/pospago/CORE/CRONOGRAMA/PLANOS', 199, 372),
(316, 'Paso 2', 'En Control-M, Ejecutar el job ubicado en BILLING >> PROCESOS >> CRONOGRAMA >> run_cronograma', 199, 373),
(317, 'Nota', ' La información solo se podrá cargar una vez por motivos de seguridad, si tiene inconvenientes por favor comunicarse con el especialista de facturación en disponibilidad.', 199, 374),
(318, 'Procedimiento Verificación Cronograma', 'Una vez se ejecute el job run cronograma, llegara un correo con la información respectiva del cargue.', 199, 375),
(319, 'Procedimiento Verificación Cronograma paso 2', 'El primer cuadro de información nos indica el log del sqlldr para la ultima ejecución del proceso, acá se encontrara información como:', 199, 376),
(320, 'Procedimiento Verificación Cronograma paso 3', 'El segundo cuadro de información nos muestra en formato de excel los registros ingresados en la bd.', 199, 377),
(321, 'Procedimiento Verificación Cronograma paso 4', 'Del correo de cronograma realizado por los operadores y aprobado por cartera. Abrir el adjunto con nombre \"Cronograma_ciclos_fact_mes_año.xlsx\"\r\nDel correo de \"Cargue Cronograma\" enviado por el JOB ejecutado en este proceso. Copiar el segundo cuadro de información \"-Cronograma 201508 a Validar-\"\r\nPegar la información en el archivo \"Cronograma_ciclos_fact_mes_año.xlsx\" de tal forma que correspondan entre filas al ciclo a verificar:', 199, 378),
(322, 'Procedimiento Verificación Cronograma paso 5', 'realizar comparacion entre las flp y fechas de suspencion con formula.\r\nejemplo: =L3-G3', 199, 379),
(323, 'Nota', 'Todas las validaciones deben dar cero, si se encuentra alguna diferente a cero se debera cambiar manualmente en la fuente T_CONTROL_FAC', 199, 380),
(324, 'Reproceso', 'El reproceso esta diseñado para el especialista de billing, este deberá ejecutar la siguiente sentencia con el periodo a reprocesar:', 199, 381),
(325, 'Pre requisitos', 'Verificar que en el correo se encuentre el adjunto Respuestas formulario registro de clientes_aaaammdd.xlsx', 200, 382),
(326, 'Imcompatibilidades', 'Istancias de BRM abajo,\r\nBloqueo del usuario operador.', 200, 383),
(327, 'Paso 1', 'Realizar la apertura del archivo \".xlsx\", verificar que este contenga la siguiente estructura', 200, 384),
(328, 'Paso 2', 'Acto seguido se deben ejecutar las siguiente instruccion para concatenar los POID, en virtud de como se realiza la busqueda en BRM:\r\n=CONCATENAR(\"*\" \'delimitador predefinido (\"|\", \";\" ó \",\")\' celda donde se ubica el POID \'delimitador predefinido (\"|\", \";\" ó \",\")\' \"*\") + tecla Intro', 200, 385),
(329, 'Paso 3', 'Para crear el INFO_INSCRITOS se utiliza la instrucción anterior, con las siguiente variante:\r\n=CONCATENAR(Celda donde esta la cuenta \'delimitador predefinido (\"|\", \";\" ó \",\")\'\";\"celda donde se ubica el mail \'delimitador predefinido (\"|\", \";\" ó \",\")\'\";1;1\") + tecla Intro', 200, 386),
(330, 'Paso 4', 'Luego ingresar al servidor BRMP1 en la siguiente ubicación: /brm/data/invoice_dir/cuentas/EMMESANGING', 200, 387),
(331, 'Paso 5', 'Tras lo siguiente editar el o los archivos (Todo de acuerdo a la cantidad de ciclos que se solicitan), con el comando vi de la siguiente manera:', 200, 388),
(332, 'Paso 6', 'Ya con la interface de edición activa, ejecutar los siguientes comandos:', 200, 389),
(333, 'Paso 7', 'Luego de haber editado los archivos con los nuevos POID, se procede a ejecutar la shell Por cada ciclo solicitado, de la siguiente manera:', 200, 390),
(334, 'Paso 8', 'Verificar que se creen los directorios correspondientes en formato Archivos_MMCCAAAA, en la cual se encuentran los archivos necesarios para realizar el proceso.', 200, 391),
(335, 'Paso 9', 'Conforme se tiene la información, se procede a transferir a alguno de los servidores pitney para su generación. (44 - 116 - 118)\r\nCon la información transferida a los servidores pitney, se procede a editar los bat necesarios para la ejecución del proceso***:', 200, 392),
(336, 'Paso 10', 'En la carpeta Insumos del servidor pitney, se realiza lo siguiente:', 200, 393),
(337, 'Paso 11', 'Al finalizar la ejecución y generación de los insumos para el proceso de envio de facturas, dejar los archivos como se encontraban (Aplica para los servidores 116 y 118)', 200, 394),
(338, 'Paso 12', 'Para el envio de las facturas electronicas utilizar el servidor 13 y el profile de envio 06, teniendo en cuenta las recomendaciones:\r\nDado que se genera la misma cuenta para diferentes meses, estos deben ser enviados uno (1) a la vez.\r\nLlevar un buen control de los enviados, para asi evitar duplicar el envio de la factura.', 200, 395),
(339, 'SEGUIMIENTO AL PROCESO', 'Ya sea que se tenga instalado el entorno grafico de consulta \"Microsoft® SQL Server® Management Studio\" en la maquina local o en el servidor 13, ejecutar la instrucción para consultar los envios que se realizan', 200, 396),
(340, 'ENTREGA DE RESULTADOS', 'Al finalizar todo el envío de las facturas solicitadas, exportar o copiar la información resultante de la consulta en la BD, a un archivo excel y guardarlo como: \"Reporte_Envio_AAAAMMDD.xlsx o .csv\", en la ruta local de su preferencia.', 200, 397),
(341, 'ENTREGA DE RESULTADOS paso 2', 'Luego sobre el mismo mail donde se recibio la solicitud, responderlo indicando que se realiza el envio y adjuntando el archivo guardado anteriormente con el resultado del proceso.', 200, 398),
(342, 'Posibles errores', 'Al momento de ejecutar la shell \"crear_carpeta.sh\" genera error \"cp: cannot access /brm/data/invoice_dir/historico/2015/bill10052015_E2/*216480971740*: No such file or directory\", no se escala y/o informa. Esto se debe a que no existe xslt para el periodo solicitado o la cuenta es de financiación.', 200, 399),
(343, 'Pre requisitos', 'Haber ejecutado el proceso irel del suspense', 201, 400),
(344, 'Ejecución', 'Ingresar a Control-M a la malla facturación diario y buscar el Job PROCESO GESTION ANOMALIAS DIARIO y en las propiedades darle clic en la opción confirm', 201, 401),
(345, 'Seguimiento al proceso', 'Se debe revisar el sysout que no tenga errores durante la ejecución', 201, 402),
(346, 'ENTREGA DE RESULTADOS', 'El proceso cuando termina su ejecución envía un mail a los destinatarios interesados en la finalización del proceso, adjunto pantallazo del correo', 201, 403),
(347, 'Posibles errores', 'Si el proceso presenta error en la ejecución se debe escalar a Angela Fandiño luz.fandino@tigo.com.co', 201, 404),
(348, 'Información del procedimiento', 'Se adjunta la imagen', 202, 405),
(349, 'Lógica de la aplicación', 'El proceso inicia en horas de la madrugada después de las 01:00 aproximadamente, previa validación Proceso Ajustes_Pos con respuesta exitosa, distribuirá en 10 hilos de ejecución, recorriendo en el sistema BRM cuentas con jerarquía (padres e hijas) de acuerdo a:', 202, 406),
(350, 'Además.....', 'Segun crónograma y si al siguiente día indica que es día de suspensión del ciclo, se genera el subproceso de Inmunidad CBA el cual prepara las cuentas ATP que van a ser inmunizadas hasta 2 días antes del siguiente ciclo con esto se garantiza el servicio hasta 1 día antes del siguiente Billing dia en el que deben estar suspendidas parcialmente (si aun siguen en mora) para que no le entregue los recursos. Para el resto de cuentas (no ATP) se genera un Pre_Collection, este archivo es adjuntado como parte de la respuesta del proceso y a su vez son insertados en la tabla de envíos de SMS para que el proceso de las 10 am envié la alerta al usuario (en donde se le indica que esta en mora y que en cualquier momento será suspendido).\r\n\r\nAl final genera archivo *.log (new_collect_yyyymmdd-hhmmss.log) para ser enviado vía e-mail', 202, 407),
(351, 'Alcance técnico', 'Creación de un proceso que realice de forma automática, cada día, la extracción de cuentas en con los saldos y estados en SIBEL para el proceso de COLLECTION, con envío de archivo PRE_COLLECTION.\r\n\r\nConciliación entre las tablas ITEM_T, BILL_T, ACCOUNT_T, BILLINFO_T del sistema INFRANET.', 202, 408),
(352, 'Cronograma facturación de cartera', 'Se adjunta imagen ', 202, 409),
(353, 'Linea del tiempo para móviles ATP', 'Se adjunta la imagen', 202, 410),
(354, 'Diagrama de casos de uso', 'Se adjunta la imagen', 202, 411);
INSERT INTO `articulo` (`idarticulo`, `nombre_articulo`, `descripcion_articulo`, `proceso_idProceso`, `imagen_idimagen`) VALUES
(355, 'Diagrama de Secuencia', 'Se adjunta la imagen', 202, 412),
(356, 'Descripción técnica de la aplicaciión', 'Parámetros', 202, 413),
(357, 'MER', 'TMP_BO_NEW_COLLECT_EXC: Relación de cuentas Inmunizadas\r\n\r\nTMP_BO_NEW_COLLECT: información al día de los cambios de estado Pre_collection\r\n\r\nTMP_BO_NEW_COLLECT_BK: información al día anterior de los cambios de estado de saldo de collection\r\n\r\nTMP_BO_NEW_COLLECT_SUB: relación de cuentas hijas asociadas a la tabla TMP_BO_NEW_COLLECT\r\n\r\nTMP_BO_NEW_COLLECT_SUB_BK: relación de cuentas hijas asociadas a la tabla del día anterior a la fecha de proceso', 202, 414),
(358, 'Configuración', 'ACCOUNT_T: Cuentas activas y canceladas del sistema INFRANET.\r\n\r\nBILLINFO_T: Datos de la factura.\r\n\r\nBILL_T: Saldos e historial de facturación.\r\n\r\nITEM_T: Distribución de factores que afectan la factura\r\n\r\nAJUSTES_POS: Distribución de factores que afectan la factura', 202, 415),
(359, 'Validaciones Internas', 'Se validara que los parámetros que le llegan al proceso no sean vacíos ni nulos. En caso de lentitud se debe validar las sesiones en BD asi;\r\n\r\nEn el portal http://10.69.32.14:82/bill/index.php', 202, 416),
(360, 'Flujo de proceso', 'Se adjunta la imagen', 202, 417),
(361, 'Identificación de Riesgos', 'Caída de la Base de datos\r\nVolumen de recaudo muy bajo\r\nLentitud BD Siebel\r\nCaida de la maquina donde residen los programas', 202, 418),
(362, 'Observaciones / Notas / Aclaraciones', 'Se debe haber recibido el e-mail de cargue de pagos del día anterior en la tabla AJUSTES_POS. Este e-mail llega como resultado del proceso de cargue de pagos que se ejecuta después de las 12 AM y tiene como remitente “pin@medbrmp2.tigo.com.co” y asunto “Resultado Cargue Ajustes en Pospago para el dd/mm/yyyy” se debe revisar el Log adjunto y verificar el mensaje de terminación “EXITOSO”. En caso de falla, el archivo adjunto indicara los posibles errores si son de I/O o problemas de red, escalar a operaciones, pero si los problemas son de datos, ej.” NO EXITOSO - ERROR: Registros inferiores a lo esperado” se deben verificar caídas en plataforma antes de escalar al encargado del proceso (Soporte Billing). Para determinar si el proceso inicio pero aún no ha terminado, se deben realizar las siguientes consultas en la base de datos PINPROD;', 202, 419),
(363, 'Además ', 'En donde ”FECHA” representa el día actual del proceso, “PROCESO” se busca por “AJUSTES_POS”, “FECHA_INICIO” representa la fecha en la que inicio el proceso (por lo general las 00:15), “FECHA_FIN” está en NULO si el proceso aún no ha terminado, “STATUS” sus posibles valores son START= En proceso, OK = Proceso termino satisfactorio, ERROR=Proceso termino con error, en caso de que el STATUS sea este último se debe validar el tipo de escalamiento que se debe realizar de acuerdo al párrafo anterior, el ultimo campo de interés en “MENSAJE” el cual muestra la descripción del error si lo hubiera. Si la consulta no retorna un registro del proceso “AJUSTES_POS” con fecha actual, se debe validar la ejecución del proceso Valida_saldos.sh, si este por algún motivo no inicio, se debe re-ejecutar nuevamente. Nota: Si el día anterior se aplicaron ajustes de forma masiva, es normal que el proceso tarde un poco más de lo acostumbrado.\r\n\r\n•	También se debe validar que se haya ejecutado, correctamente, la primera parte del proceso Valida_saldo.sh, este actualiza la tabla BackUp con la información de saldos y estados del día anterior para generar reactivaciones y suspensiones\r\n\r\n\r\nPara validar si el proceso de Valida_saldos.sh está en ejecución, ejecutar el comando en sistema operativo (10.69.32.229);', 202, 420),
(364, 'Tratamiento de errores', 'os posibles errores serán mostrados en el archivo Log del proceso y serán enviados vía e - mail.\r\n\r\n2.	Si el correo ajustes_pos no llega, validar con el administrador del servidor SMTP si el servicio está funcionando bien, validar que el correo no se esté desviando a los SPAM, validar que el archivo ya se encuentre en el directorio /brm/pin/7.4/millicom_custom/scripts/cartera/log, si el log no indica finalización del proceso, se debe contactar al DBA de turno para que realice las validaciones necesarias.\r\n\r\n3.	En el LOG final del proceso valida saldos (correo) se debe validar que cada uno de los pasos no contengan ningún mensaje de error, además de que en cada uno de los hilos el número de cuenta sea superior a 150 mil, esto está determinado en el paso 1 (--> Número de Cuentas por Thread: 107106). En los casos en los que genera pre-collection, el correo llega vacío y se debe revisar directamente el archivo log (/brm/pin/7.4/millicom_custom/scripts/cartera/log/new_collect_20130520-052203.log), si el parámetro mencionado no está acorde a lo indicado, se debe contactar al analista de Billing que tenga a cargo el soporte.', 202, 421),
(365, 'Alerta Collection', 'En caso de que alguna de las siguientes condiciones no se cumplan, el proceso dispara un correo en el que se va a resaltar la condicion fallida, en este caso se debe re-ejecutar el proceso y esperar que la alarma no se dispare, si vuelve a suceder es necesario entrar a revisar el proceso de tal manera que durante el día quede solucionado y al siguiente día se pueda suspender.', 202, 422),
(366, 'Sistema de Archivos', 'Carpetas y paquetes que conforman el proyecto Valida Saldos', 202, 423),
(367, 'Alcance técnico', 'Creación de un proceso que realice de forma automática o por demanda, el cargue y la aplicación de pagos en la tabla CTA_MASSIVE_PAYMENTS para el detalle y CTA_MASSIVE_CONTROL para el encabezado, a su vez debe generar el archivo de suspensiones parciales de ciclo en la primera ejecución y con cada archivo extraer las acciones de reconexión resultantes de la aplicación de dichos pagos.\r\n\r\nConciliación de la tabla CTA_MASSIVE_PAYMENTS de la Base de datos BRM.', 203, 424),
(368, 'Diagrama de arquitectura', 'Se adjunta la imagen', 203, 425),
(369, 'Casos de Uso', 'Se adjunta la imágen', 203, 426),
(370, 'Ilustración 2', 'Se ajunta la imagen', 203, 427),
(371, 'Correo', 'el correo correspondiente al proceso', 203, 428),
(372, 'Lógica de aplicación', 'Validación por demanda de los archivos asobancaria y/o magos manuales\r\n\r\nAl final genera respuesta para ser enviado vía e-mail\r\n\r\nValidaciones Internas\r\nValida el valor del pago, la existencia de la cuenta y la unicidad de los datos.\r\n\r\nAplicaciones involucradas', 203, 429),
(373, 'Identificación de Riesgos', 'Los archivos conforme se validan pueden retornar un error en el Log de la respuesta, mas sin embargo, a través de la extensión dejada, se puede identificar el error así;\r\n\r\n.ERR_DETAIL  Indica que el número de errores en el detalle supero el máximo Permitido\r\n\r\n.ERROR  Error en archivo por duplicidad y mal formado\r\n\r\n.EXISTS  El archivo ya fue aplicado\r\n\r\n.ERR_LOT  El número de lote del registro 05 no corresponde con el del 08', 203, 430),
(374, 'Tratamiento de errores', 'Si el error no corresponde a ninguno de los casos indicados anteriormente, se debe examinar el archivo log adjunto en el correo o en la carpeta del Logs', 203, 431),
(375, 'Arquitectura', 'Carpetas y paquetes que conforman el proyecto VALIDA_BANCOS en la maquina 10.69.32.229\r\n\r\no	/brm/pin/7.4/millicom_custom/scripts/cartera/install/LoadPayments.jar\r\n\r\no	/brm/pin/7.4/millicom_custom/scripts/cartera/sh/LoadPayments.sh\r\n\r\no	/brm/pin/7.4/millicom_custom/scripts/cartera/config/config_payment.properties\r\n\r\no	/brm/pin/7.4/millicom_custom/scripts/cartera/log\r\n\r\no	/brm/pin/7.4/millicom_custom/scripts/cartera/tmp\r\n\r\no	/data/iel/pagos/in', 203, 432),
(376, 'Recursos', 'Acceso con usuario de ejecución/verificación al aplicativo Control-M\r\nAcceso grafico o putty al servidor MEDBRMRE, usuario reporope', 204, 433),
(377, 'Pre requisitos', 'Fin exitoso del proceso BCKREP: Copia BRM\r\nHora programada para ejecución', 204, 434),
(378, 'Incompatibilidades', 'Ejecución retrasada o solicitada del proceso Foto Reporte\r\nEjecución de la Copia BRM', 204, 435),
(379, 'Ejecución', 'Verificar el inicio del proceso a las 02:00, en la carpeta varios automaticos.', 204, 436),
(380, 'Paso 2', 'Al inicial verificar en el output que no se presente algun inconveniente de conexión.', 204, 437),
(381, 'ENTREGA DE RESULTADOS', 'Al finalizar se recibe un correo con el asunto Proceso Cartera Foto diaria con Fecha AAAA/MM/DD OK, Informe consolidado en el cual deben verificar el archivo log adjunto Import_Collection_info_fechaproceso.log, este contine la información del proceso y por lo general debe finalizar con la linea \"INFO co.com.millicom.rtbill.ExeFotoDiaria.run(145)===> [run] Proceso termina EXITOSO\"', 204, 438),
(382, 'Posibles errores', 'Cualquier error \"ORA-XXX\" generado en el proceso, debe ser informado al especialista de cartera billing en soporte.\r\nSi hay errores diferentes a los presentados en el log ERROR propiedad NULA, deben ser informados al especialista de cartera billing en soporte.', 204, 439),
(383, 'Recursos a usar', 'Control – M, Carpeta VARIOS AUTOMATICO\r\nArchive Base de Datos, maquina MEDBRMRE ruta /fs_arch_01/utlfile/\r\nServidor Billing ruta /Repositorio/dump/desconciliados_cartera/\r\nServidor de Reportes en la ruta /pospagocm/Reportes/Elizabeth/', 205, 440),
(384, 'Incompatibilidades', 'Ventana Servidor MEDBRMRE\r\nVentana BD PINPROD', 205, 441),
(385, 'Ejecución', 'Verificar el inicio del proceso Saldo_Item_RepCartera en la hora indicada', 205, 442),
(386, 'Paso 2', 'Se verifica el output para garantizar que el proceso se ejecute sin inconvenientes\r\n', 205, 443),
(387, 'ENTREGA DE RESULTADOS', 'Al finalizar el proceso se recibe el mail con asunto Proceso Cartera diario con Fecha AAAA/MM/DD OK, Informe ,consolidado., y verificar el log adjunto CarteraDiario_AAAAMMDD.log', 205, 444),
(388, 'ENTREGA DE RESULTADOS PASO 2', 'Luego se ingresa al servidor MEDBRMRE en la ruta /fs_arch_01/utlfile/, ubicar los archivos mencionados en el mail y verificar la existencia del archivo compreso N_REPCARTERA_AAAAMMDD.csv.tar.gz.', 205, 445),
(389, 'ENTREGA DE RESULTADOS PASO 3', 'A continuación transferir a la maquina local los archivos generador luego comprimirlos con el nombre N_REPCARTERA_AAAAMMDD.zip y finalmente transferirlo al servidor de reportes en modo binario a la ruta /pospagocm/Reportes/Elizabeth/.', 205, 446),
(390, 'ENTREGA DE RESULTADOS PASO 4', 'Se respónde el correo recibido con la lista de distribución de cartera, adjuntando los log y la imagen de la transferencia al servidor de reportes.', 205, 447),
(391, 'ENTREGA DE RESULTADOS PASO 5', 'A continuación se transfieren los archivos Hijas_con_saldo_AAAAMMDD_SNA.csv y Desconciliados_AAAAMMDD.csv al servidor de billing en la ruta /Repositorio/dump/desconciliados_cartera/', 205, 448),
(392, 'ENTREGA DE RESULTADOS PASO 6', 'Se responde el mismo correo recibido del fin de cartera, donde se adjunta la imagen de los archivos ubicados en el servidor de billing', 205, 449),
(393, 'Manejo de errores', 'Si en algunos de los procesos se presenta error ORA-XXXX, escalar inmediatamente al soporte disponible DBA y al encargado del proceso.\r\nSi después de las 07:00 a.m. el proceso no ha finalizado se informa al DBA de soporte disponible para que verifique el proceso y encuentre el motivo de lentitud.\r\nSi se genera el siguiente error en la ejecución del proceso:\r\n\r\nSe debe esperar hasta que finalice el proceso FOTO REPORTE (como lo muestra el mismo log del error, en el mail adjunto), para ejecutar nuevamente el proceso de Saldo_Item_Repcartera y garantizar el exito del proceso.', 205, 450),
(394, 'Manejo de errores paso 2', 'Si se presenta el siguiente error al finalizar el proceso: 99;Error - ORA-20102: [PROC_WRT_CARTERA] Fallo al Inicio Error: ORA-02046: distributed transaction already begun Ver log del mail adjunto', 205, 451),
(395, 'Manejo de errores paso 3', 'Y no se generan los archivos Hijas_con_saldo_AAAAMMDD_SNA.csv - Desconciliados_AAAAMMDD.csv en la ruta /fs_arch_01/utlfile del servidor de reportes MEDBRMRE (10.69.32.23.\r\n\r\nProceder inmediatamente a Re-lanzar el proceso, para que realice la extracción/escritura de manera correcta.', 205, 452),
(396, 'Ejecución', 'Para explicar este proceso,se aplicará un ejemplo:\r\nEn el siguiente ejemplo se observa como el cliente realiza un pago y la afectacion sobre los items de cobro (alocacion);\r\n\r\nEl cliente hace un pago por 25724 por un canal electrónico;', 206, 453),
(397, 'Paso 2', 'La distribución de ese pago sobre cada uno de los items de cobro es así;\r\nDependiendo del tipo de Item afectado, el origen de la afectación (ajustes o pago) y la fecha efectiva, de afectación o de alocacion que para este caso es el campo FEC_EFECTIVA, se puede extraer de forma periodica lo recaudado o ajustado en cada unos de estos tipos de item, por ejemplo para extraer todo lo recaudado por impuestos se busca todo lo que cuyo POID_ITEM_TRANSFER inicie por \'/item/tax/\'.\r\n', 206, 454),
(398, 'Proceso', 'Se adjunta imagen', 206, 455),
(399, 'Correo', 'Se adjunta el correo con la siguiente información', 206, 456),
(400, 'Revisión del proceso', 'Se ejecuta el siguiente query en la base de datos del BRM.\r\n\r\nValidacion por medio del archivo Log en la maquina donde corre el proceso;\r\n\r\n/brm/pin/7.4/millicom_custom/scripts/cartera/log/log_super_dist_pya_YYYYMMDD-030000.log', 206, 457),
(401, 'Reproceso', 'n caso de falla porque el STATUS en la tabla LOG_CONTROLES es diferente a OK, usualmente se presenta por errores de conexión con la Base de datos PINPROD 7.3 la cual no tiene mantenimiento, Si la falla es del mismo dia, se debe llamar al operador e indicarle que re-ejecute el proceso. Si la falla se presento en dias anteriores se debe ingresar a la SH y re-ejecutar el pedazo faltante o re-jecutar completo, si el tiempo lo permite, cambiando la fecha de la variable lFecha_Ini.\r\n\r\nEn caso de que la falla persista, se debe escalar al DBA de turno por medio de los operadores quienes adjuntaran el correo del proceso como evidencia.', 206, 458),
(402, 'Recursos', 'Recaudos Bancarios WebServices descargados.\r\nControl-M\r\nCarpeta Diario\r\nJob CONCILIACION_ASO', 207, 459),
(403, 'Incompatibilidades', 'Ventana de mantenimiento sobre el servidor MEDBRMPI\r\nConciliación ASO en ejecución.', 207, 460),
(404, 'Ejecución', 'Para empezar de deben de tener en cuentas las reglas de conciliación', 207, 461),
(405, 'Paso 2', 'los siguientes recaudos deben ser descargados y transferidos al servidor mencionado anteriomente en la siguiente ruta: /brm/pin/7.4/millicom_custom/scripts/cartera/ws_asobank/', 207, 462),
(406, 'Paso 3 ', 'Luego de tener los recaudos en la ruta indicada, se procede a ejecutar el Job CONCILIACION_ASO, para iniciar el proceso de conciliación de recaudos WS contra CRM.', 207, 463),
(407, 'Seguimiento al proceso', 'Verificar en el output que se valla tomando cada recaudo, la cantidad de registros y en la ruta /brm/pin/7.4/millicom_custom/scripts/cartera/ws_asobank cada recaudo que se valla aplicando debe contener la finalización \".OK\"', 207, 464),
(408, 'ENTREGA DE RESULTADOS', 'Al finalizar el proceso de conciliación se recibe un correo con el asunto Se han encontrado archivos bancarios AAAA/MM/DD del remitente concilia_aso@tigo.com.co, el cual contiene los recaudos WS conciliados exitosamente.', 207, 465),
(409, 'Paso 4', 'A continuación buscar el correo con asunto APLICACION CONCILIACION PAGOS ASOBANCARIAS DD/MM/AAAA y enviarlo con la siguiente información:', 207, 466),
(410, 'Posibles errores', 'Si el proceso de conciliación marca el recaudo WS como \".OK\" pero el correo de confirmación llega sin información, se debe validar con el especialista de Cartera Billing en soporte, para que verifique la causa y esperar sus indicaciones.\r\nSi renombran un recaudo erradamente recaudo_ddmmaaaa* y al dia siguiente se debe aplicar el mismo recaudo con la fecha recaudo_dd-ammaaaa*, deben agregarle \"_1*\" para evitar que se genere error de almacenamiento en la tabla de recaudos WS ASO.\r\nCuando se presente el siguiente error:', 207, 467),
(411, 'Posibles errores 2', 'Si se presenta error en la validación del recaudo Redeban_Davivienda_ddmmaaaa.txt', 207, 468),
(412, 'Posibles errores 3', 'Verificar el archivo en su primer encabezado el cual debe contener 162 o más caracteres y estar tabulado, si esta por dejado de esta cifra informar a la entidad para que remitan el archivo completo y a los especialistas de soporte movilidad y cartera recaudos', 207, 469),
(413, 'Ejecución', 'Dado que el proceso es automático, se deben realizar las siguientes validaciones:\r\n\r\nEn la tabla VARIOS_AUTOMATICO, buscar y verificar el JOB Proceso RTBilling Diario', 208, 470),
(414, 'Paso 2', 'Ubicarse en la pestaña Execution, validar el servidor y la hora de inicio del proceso.', 208, 471),
(415, 'Seguimiento al proceso', 'Se adjunta la imagen', 208, 472),
(416, 'Paso 3', 'Adicional a este si es día de ciclo deben verificar que el Rollover tampoco genere errores diferentes a los de envío de correo.\r\n', 208, 473),
(417, 'ENTREGA DE RESULTADOS', 'Al finalizar la ejecución del JOB se debe recibir un correo con el log anexo de la ejecución.', 208, 474),
(418, 'Además...', 'Se debe verificar el Log y validar que no hallan errores.', 208, 475),
(419, 'Manejo de errores', 'Si en las líneas finales del proceso aparece un error, se debe informar inmediatamente al soporte Disponible de BRM para que verifique y ejecute el proceso nuevamente.\r\nSi en las líneas finales del proceso aparece un error, se debe informar inmediatamente al soporte Disponible de BRM para que verifique y ejecute el proceso nuevamente.\r\n', 208, 476),
(420, 'Paso 1', 'El primer paso es conectarse a la ip 10.69.32.227', 209, 477),
(421, 'Paso 2', 'on el usuario: operador y el pass XXXXXXXXX (se encuentra almacenada en el plan de producción)', 209, 478),
(422, 'Paso 3', 'Luego de estar conectados a la 227, debemos ingresar a la siguiente ruta: /brm/pin/7.4/millicom_custom/scripts/billing, Donde seleccionaremos la carpeta de periodos_perdidos.\r\n\r\nLuego, ejecutaremos la siguiente Shell\r\n\r\nSh periodos_perdidos. sh\r\n\r\n', 209, 479),
(423, 'Paso 4', 'La Shell de periodos perdidos se demora aproximadamente unos 5 minutos en ser procesada, luego nos llegara un correo con la información de las cuentas encontradas que presentan este tipo de casuística con el fin de corregirlas y poder garantizar su correcta facturación.', 209, 480),
(424, 'Paso 4-continuación', 'imagen anexa al paso 4', 209, 481),
(425, 'Ejecución', 'Se debe ingresar a Control M en la Malla Ciclo Buscar el JOB BITAS002 y verificarlo', 210, 482),
(426, 'Paso 2', 'Se debe ingresar al menú tools - order/force ', 210, 483),
(427, 'Paso 3', 'En la ventana desplegada validar que la tabla sea Ciclo, ubicar el JOB BITAS002 y en la opción Odate cambiar la fecha por la del Ciclo y damos clic en force ', 210, 484),
(428, 'Paso 4', 'En la ventana de confirmación damos Clic en “YES”, en la siguiente “HIDE” y cerramos ', 210, 485),
(429, 'Paso 5', 'Verificamos el JOB que forzamos de que tenga la fecha del Ciclo a ejecutar en la opción Order date', 210, 486),
(430, 'Paso 6', 'Luego damos Clic derecho en el JOB “Confirm” y en al ventana de confirmación damos “YES”', 210, 487),
(431, 'Seguimiento al proceso', 'Verificar el Sysout del JOB que se este generando la información de los archivos', 210, 488),
(432, 'ENTREGA DE RESULTADOS', 'Verificar en la ruta Log_Reportes_Pospago en \"Medbilling01\\Git\\GestionIT\\registros\" carpeta CifrasControl, que se halla generado una carpeta con el nombre mmddaaaa_ant_bill la cual contiene los archivos:\r\n\r\nCuentasAfacturarCICLO-mmddaaaa.txt\r\nCuentasYaFacturadasCICLO-mmddaaaa.txt\r\nDe los cuales el primer archivo debe contener la información de las cuentas que serán procesadas por el Billing de Ciclo, y se deben anexar en el mail que se envía al ajustar el ambiente de Billing ', 210, 489),
(433, 'Posibles Errores', 'Al finalizar el job y si los archivos no fueron transferidos de manera exitosa, se debe validar que el usuario “operador” no este bloqueado.', 210, 490),
(434, 'Ejecución', 'Se debe garantizar que los procesos BILLING CICLO, CIFRAS CONTROL DESPUES BILLING y TRAZABILIDAD ACTUALIZA BILLING , finalicen sin inconvenientes.', 211, 491),
(435, 'Paso 2', 'Acto seguido, verificar que el procesos FLAG INVOICE inicie su ejecución sin retrasos, validar que tome la fecha del ciclo y flp que se esta procesando, ya que inicia automatico.', 211, 492),
(436, 'Seguimiento al proceso', 'Al finalizar la ejecución del Job, verificar el resultado en el sysout, dando clic derecho Output y garantizar que no se generen errores en el procedimiento y que se indique la cantidad de cuentas, a las cuales se realizo el update.', 211, 493),
(437, 'ENTREGA DE RESULTADOS', 'En el buzon de correo buscar la plantilla: FLAG INVOICE DE CICLO cc/mm/aaaa * , donde se incluye la siguiente información:\r\n* Hace referencia a la fecha del Ciclo que se procesa en el momento\r\n\r\nLISTA DE DISTRIBUCIÓN: Leal, Juan <Juan.Leal@tigo.com.co>; Roys, Dilia <Dilia.Roys@tigo.com.co>; Salamanca, Diana <Diana.Salamanca@TigoUne.com>; Fandino, Luz <luz.fandino@TigoUne.com>; Pernett, Heydi <Heydi.Pernett@tigo.com.co>; levy.sanchez@mail.tigo.com.co; Navarrete, Miguel <Miguel.Navarrete@TigoUne.com>; Jacome Duque, Kevin Arturo <Kevin.Jacome@TigoUne.com>, lorena.ahumada@mail.tigo.com.co; sergio.ortiz@mail.tigo.com.co; \'Gustavo Orozco\' (gustavo.orozco@menestys-services.com); \'James Alvarez\' (james.alvarez@mail.tigo.com.co); \'Carlos Arroyave\' (carlos.arroyave@mail.tigo.com.co); cristian.tobon@mail.tigo.com.co\r\nIMAGENES DEL PROCESO: Se adjuntan las imagenes validando la fecha Ciclo, FLP correspondiente y la imagen donde se valide la cantidad de cuentas que se actualizaron (update).', 211, 494),
(438, 'Posibles errores', 'En caso de generarse error ORA-### , por desconexion o inaccesibilidad informar al disponible de billing con su debido mail y esperar las indicaciones.\r\nCualquier otro error que genere el proceso, informarlo para su verificación, siempre con su mail informando las novedades.', 211, 495),
(439, 'Prodedimiento', 'Ingresamos a Control M malla Ciclo, allí buscamos el JOB \'PREAJUSTES_Y_AJUSTES_BILLING_DE_CICLO y comprobar que sea el que vamos a ejecutar', 212, 496),
(440, 'Paso 2', 'Damos Clic derecho en el JOB \'“Confirm” y en al ventana de confirmación clic en “YES”, este debe cambiar de rosado a amarillo', 212, 497),
(441, 'Paso 3', 'Se debe ingresar al \'“Sysout” con clic derecho al JOB y verificar que las fechas sean:\r\n\r\nFecha inicial: Ciclo anterior en formato AAAA/MM/DD\r\nFecha Final: Ciclo actual en formato AAAA/MM/DD\r\nFecha Reporte Tasación: Debe estar al día en que se van a ejecutar los preajustes formato AAAA/MM/DD', 212, 498),
(442, 'Monitoreo del proceso', 'Se ingresa al portal Billing en la dirección http://10.69.32.14:82/bo se inicia sesión con el usuario y contraseña correspondiente, nos ubicamos en el menú monitoreo y en el submenú buscamos pre-ajustes y accedemos.', 212, 499),
(443, 'paso 2 monitoreo', 'Al seleccionar la fecha del ciclo y dar clic en consultar nos ira mostrando el progreso de cada paso que valla ejecutando hasta su finalización.', 212, 500),
(444, 'Manejo de errores', 'Si durante la ejecución se presenta algún error ORA - ### o el proceso está muy demorado, verificar y escalar con el DBA disponible, disponible de BRM para su verificación.\r\nSe debe tener presente que si el proceso presenta lentitud en el Paso 00 Completar_Reporte_Tasacion esto no es debido a bloqueos en la BD, se debe a que el proceso toma todos los eventos que fueron procesados por el REL desde la media noche hasta el momento previo al inicio del Ciclo.\r\nSi el proceso finaliza inesperadamente mostrando en el monitoreo el paso donde estaba ejecutando en ROJO , se debe informar al especialista de Billing para su verificación.', 212, 501),
(445, 'ENTREGA DE RESULTADOS', 'Después de validar que se hallan generado los archivos en la ruta D:\\Ajustes_billing\\tmp\\\r\n\r\nSe debe enviar un correo con las siguientes características:\r\n\r\nLista de distribución: FIN PREAJUSTES DE CICLO\r\nPlantilla de Correo: FIN PREAJUSTES DE CICLO DD/MM/YYYY.msg\r\nArchivos a Adjuntar: Se debe adjuntar el comprimido con los archivos generados en la macro\r\nEn la lista de distribución están incluidas las siguientes personas: Edison Alvarez, Luis Suarez, Diego Montoya, Dilia Roys, Miguel Navarrete, Ronald Beltran, Luz Fandino, Juan Leal, james alvarez, Ramiro Ayarza, July Zuluaga, info@menestys-services.com\r\n\r\nEnviar un SMS indicando finalización de los preajustes (Ver manual 1016 - Enviar SMS Fin Proceso Preajustes y Ajustes de Ciclo)', 212, 502),
(446, 'Ejecución', 'Se debe ingresar a control M en la Maya Ciclo y buscar el JOB BITAS018 ', 213, 503),
(447, 'Paso 2', 'Luego ir a menú tools – order/force', 213, 504),
(448, 'Paso 3', 'Allí verificamos que la tabla sea CICLO, buscamos el JOB y nos ubicamos en Odate para modificarle la fecha del Ciclo en ejecución', 213, 505),
(449, 'Paso 4', 'Damos clic en Force y nos sale una ventana a la cual damos Clic en “YES”, en la ventana siguiente damos clic en “Hide” y cerramos para completar el forzado del JOB .\r\nLuego buscamos el JOB modificado y lo verificamos\r\n\r\nLe damos Clic derecho “Confirm” y en la ventana de confirmación Clic en “YES” para que inicie el proceso', 213, 506),
(450, 'Seguimiento al proceso', 'Ingresar al sysout para verificar que no hallan errores y que se estén generando los archivos con la información de las facturas procesadas en el Billing de ciclo.\r\n\r\n', 213, 507),
(451, 'ENTREGA DE RESULTADOS', 'Con al información generada llenar el archivo Cifras de Control necesario para hacer la entrega a B.O (Ver manual IS-802 - Entrega a B.O )\r\nIngresamos a la ruta Log_Reportes_Pospago en \"Medbilling01\\Git\\GestionIT\\registros\" carpeta CifrasControl, verificamos que se halla generado una carpeta con el nombre mmddaaaa_ant_inv la cual contiene los archivos:\r\nBillGenerados-CICLO-mmddaaaa.txt\r\nBillGeneranInvoice-CICLO-mmddaaaa.txt\r\nDiffAccountBill-CICLO-mmddaaaa.txt\r\nDiffBillAccount-CICLO-mmddaaaa.txt', 213, 508),
(452, 'Posibles Errores', 'Falla en la transferencia de los archivos, se debe validar si la contraseña de operador no esta bloqueada, en tal caso, usar la herramienta asignada para desbloquear el usuario.\r\nSe ejecuta el JOB y no procesa, validar en el sysout la causa por la que no se ejecuta el proceso, si el error persiste escalar al administrador de infranet o de la plataforma para su revisión.', 213, 509),
(453, 'Ejecución', 'Los Jobs a utilizar son los siguientes:', 214, 510),
(454, 'Paso 2', 'Se debe ingresar a la Malla Billing y buscar el JOB AJUSTE_AMBIENTE_INVOICE_CICLO y verificar que sea para ajustar ambiente invoice y que la maquina sea MEDBRMPI.\r\n\r\n', 214, 511),
(455, 'Paso 3', 'Al dar clic en Properties muestra la siguiente información, en la cual se debe revisar que sea el Job a ejecutar', 214, 512),
(456, 'Paso 4', 'Dar clic derecho al Job AJUSTE_AMBIENTE_INVOICE_CICLO, este despliega un submenú del cual debemos seleccionar y dar clic a la opción Confirm.', 214, 513),
(457, 'Paso 5', 'Luego de confirmar la ejecución aparece el siguiente cuadro de dialogo al cual se debe dar clic en “Yes” para que el Job inicie el proceso.', 214, 514),
(458, 'Paso 6', 'Cuando el job AJUSTE_AMBIENTE_INVOICE_CICLO finaliza de manera exitosa automáticamente inician los Jobs AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP1 AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP2,los cuales se muestran en color amarillo.', 214, 515),
(459, 'Paso 7', 'Luego de finalizado los Jobs “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP1” “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP2” , los cuales se muestran en color verde, como lo indica la imagen.\r\n\r\n', 214, 516),
(460, 'Verificación SYOUT', 'Después de finalizado de manera correcta cada uno de los Jobs de Ajustes Ambiente, se procede a revisar los SysOut de cada Jobs.\r\n\r\nSYSOUT JOB “AJUSTE_AMBIENTE_INVOICE_CICLO”\r\n\r\nPara abrir el SysOut se debe dar clic derecho en el Job indicado y seleccionar y dar clic en la opción Sysout, como lo indica la imagen.', 214, 517),
(461, 'Verificación SYOUT paso 2', 'Este inmediatamente abren un cuadro de dialogo donde se indica el nombre del Job la hora de inicio y fin de la ejecucion del proceso.\r\n\r\n', 214, 518),
(462, 'Verificación SYOUT paso 3', 'Este automaticamente debe abrir el Sysout como se indica en la siguiente imagen, El cual debemos revisar la siguiente informacion : Verificar las fechas según cronograma y que el Sysout no contenga errores.', 214, 519),
(463, 'Verificación SYOUT paso 4', 'En caso de error debe ser escalado al administrador de soporte. En caso que no muestre las fechas o las fechas sean de un ciclo que no estamos procesando, se sebe revisar la tabla de los últimos ciclos procesados en el portal: http://10.69.32.14:82/bo/index.php\r\n\r\nSYSOUT JOB “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP1”\r\n\r\nPara abrir el SysOut se debe dar clic derecho en el Job indicado y seleccionar y dar clic en la opción Sysout, como lo indica la imagen.\r\n\r\n', 214, 520),
(464, 'Verificación SYOUT paso 5', 'Este inmediatamente abren un cuadro de dialogo donde se indica el nombre del Job la hora de inicio y fin de la ejecución del proceso.\r\n\r\n', 214, 521),
(465, 'Verificación SYOUT paso 6', 'Este automaticamente debe abrir el Sysout como se indica en la siguiente imagen,\r\n\r\nEl cual debemos revisar la siguiente informacion :\r\n\r\nVerificar las fechas según cronograma y que el Sysout no contenga errores.\r\n\r\n', 214, 522),
(466, 'SYSOUT JOB “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP2”', 'Para abrir el SysOut se debe dar clic derecho en el Job indicado y seleccionar y dar clic en la opción SysOut, como lo indica la imagen.', 214, 523),
(467, 'SYSOUT JOB “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP2” paso 2', 'Este inmediatamente abren un cuadro de dialogo donde se indica el nombre del Job la hora de inicio y fin de la ejecución del proceso.', 214, 524),
(468, 'SYSOUT JOB “AJUSTE_AMBIENTE_INVOICE_CICLO_MEDBRMP2” paso 3', 'Este automaticamente debe abrir el Sysout como se indica en la siguiente imagen,\r\n\r\nEl cual debemos revisar la siguiente informacion :\r\n\r\nVerificar las fechas según cronograma y que el Sysout no contenga errores.', 214, 525),
(469, 'ENTREGA DE RESULTADOS', 'Se debe enviar mail teniendo en cuenta lo siguiente:\r\nLista de distribución: AJUSTE AMBIENTE INVOICE Plantilla de Correo: AJUSTE DEL AMBIENTE PARA EL INVOICE DE CICLO /MM/YYYY.msg Archivos a Adjuntar: Se debe adjuntar el detalle deL ajuste del ambiente, pantallazo de Cifras control # 3 y la ejecución del proceso. En la lista de distribución están incluidas las siguientes personas: Beltran Alvarez, Ronald Fernando <RonaldFernando.BeltranAlvarez@tigo.com.co>; Navarrete, Miguel <Miguel.Navarrete@tigo.com.co>; Roys, Dilia <Dilia.Roys@tigo.com.co>; Fandino, Luz <Luz.Fandino@tigo.com.co>; Basabe, Leonardo <Leonardo.Basabe@tigo.com.co>; Alvarez, Edison <Edison.Alvarez@tigo.com.co>; Salamanca, Diana <Diana.Salamanca@tigo.com.co>; Montoya, Diego <Diego.Montoya@tigo.com.co>; Fandino, Luz <Luz.Fandino@tigo.com.co>; Suarez, Luis <Luis.Suarez@tigo.com.co>; Meneses, Juan <Juan.Meneses@tigo.com.co>; Leal, Juan <Juan.Leal@tigo.com.co>; Llanos, Yamil <Yamil.Llanos@tigo.com.co>; Prada, Alexander <Alexander.Prada@tigo.com.co>; Llanos, Yamil Yamil.Llanos@tigo.com.co Con copia a: Contacto James Alvarez <james.alvarez@mail.tigo.com.co>; Contacto Ramiro Ayarza <ramiro.ayarza@mail.tigo.com.co>; Contacto July Zuluaga <july.zuluaga@mail.tigo.com.co>; \'info@menestys-services.com\'; Cristian Tobon <cristian.tobon@mail.tigo.com.co>', 214, 526),
(470, 'Posibles Errores', 'En el momento de estar ajustando el ambiente el JOB finalice en rojo, se debe verificar el Sysout, en caso de encontrar problemas de conexión se debe escalar al administrador de infranet para que verifique si es debido a comunicación con la instancia u otro error para solucionarlo y continuar con el proceso.', 214, 527),
(471, 'Ejecución', 'Se debe ingresar a control M en la Maya Ciclo y buscar el JOB BITAS025', 215, 528),
(472, 'Paso 2', 'Luego ir a menú tools – order/force', 215, 529),
(473, 'Paso 3', 'Allí verificamos que la tabla sea CICLO, buscamos el JOB y nos ubicamos en Odate para modificarle la fecha del Ciclo en ejecución ', 215, 530),
(474, 'Paso 4', 'Damos clic en Force y nos sale una ventana a la cual damos Clic en “YES”, en la ventana siguiente damos clic en “Hide” y cerramos para completar el forzado del JOB ', 215, 531),
(475, 'Seguimiento al proceso', 'ngresar al sysout para verificar que no hallan errores y que se estén generando los archivos con la información de las facturas procesadas en el Billing de ciclo.\r\n\r\n', 215, 532),
(476, 'ENTREGA DE RESULTADOS', 'Ingresamos a la ruta Log_Reportes_Pospago en \"Medbilling01\\Git\\GestionIT\\registros\" carpeta CifrasControl, verificamos que se halla generado una carpeta con el nombre mmddaaaa_des_inv la cual contiene los archivos:\r\n\r\nDiffBillInvoice1-CICLO-mmddaaaa.txt\r\nDiffInvoiceBill2-CICLO-mmddaaaa.txt\r\nDiffInvoiceBill-CICLO-mmddaaaa.txt\r\nInvoiceDuplicados-CICLO-mddaaaa.txt\r\nInvoiceGenerados-CICLO-mmddaaaa.txt\r\nInvoiceInvoiceGen10007-CICLO-mmddaaaa.txt', 215, 533),
(477, 'Posibles Errores', 'Falla en la transferencia de los archivos, se debe validar si la contraseña de operador no esta bloqueada, en tal caso, usar la herramienta asignada para desbloquear el usuario.\r\nSe ejecuta el JOB y no procesa, validar en el sysout la causa por la que no se ejecuta el proceso, si el error persiste escalar al administrador de infranet o de la plataforma para su revisión.', 215, 534),
(478, 'Ejecución', 'En la malla Ciclo buscamos el JOB VALIDAR INVOICE DUPLICADO y lo verificamos', 216, 535),
(479, 'Paso 2', 'Forzamos el JOB con la fecha del ciclo para el cual queremos realizar la consulta:\r\n\r\nVamos al menú tools – order/force', 216, 536),
(480, 'Paso 3', 'Buscamos el JOB a forzar en la lista VALIDAR INVOICE DUPLICADO, verificamos que la opción Duplicate este activa, en el campo Odate cambiamos la fecha por la del Ciclo a consultar.', 216, 537),
(481, 'Paso 4', 'Buscamos el JOB a forzar en la lista VALIDAR INVOICE DUPLICADO, verificamos que la opción Duplicate este activa, en el campo Odate cambiamos la fecha por la del Ciclo a consultar.', 216, 538),
(482, 'Paso 5', 'Luego damos Clic en force, nos mostrara una ventana de confirmación donde daremos clic en “Yes” y en la siguiente Clic en Hide y Close', 216, 539),
(483, 'Paso 6', 'Validamos que el JOB se halla duplicado y que en la fila Order date de la pestaña Active tenga la fecha solicitada', 216, 540),
(484, 'Paso 7', 'Damos Clic derecho “Confirm” y en la ventana de confirmación clic en “Yes” para que inicie\r\n\r\n', 216, 541),
(485, 'seguimiento al proceso', 'Validamos el Sysout para verificar que el proceso inicie sin inconvenientes\r\n\r\n', 216, 542),
(486, 'Posibles Errores', '	No es posible realizar la consulta, se debe verificar si no hay bloqueos en la BD, si los hay escalar al disponible DBA y al soporte de infranet para que verifiquen.\r\n\r\nSi se encuentran Invoice duplicados se debe escalar al Especialista de Billing para que verifique y autorice si es posible continuar con el Ciclo.', 216, 543),
(487, 'Ejecución del proceso', 'Ingresar a Control M y buscar el JOB VALIDAR NUMERO DE CUENTAS ANTES DE LA NUMERACION\r\n', 217, 544),
(488, 'Paso 2', 'Dar clic derecho en “Confirm” y en al ventana confirmatoria clic en “YES”', 217, 545),
(489, 'Paso 3', 'Si el JOB queda en Gris se debe dar Clic derecho “Why”, en la ventana siguiente resaltar la condición y clic en “add condition” y dar “YES” en la ventana de confirmación', 217, 546),
(490, 'Seguimiento al proceso', 'Se debe revisar el sysout del proceso en busca de errores en la ejecución\r\n', 217, 547),
(491, 'Posibles Errores', 'Al finalizar la validación aparezcan cuentas de otros Ciclo, se debe informar vía mail y telefónicamente a Diana Salamanca, para que verifique y autorice continuar.\r\n', 217, 548),
(492, 'Ejecución', 'Se ingresa a Control M y se busca el JOB EJECUTAR NUMERACION', 218, 549),
(493, 'Paso 2', 'Dar Clic derecho “Confirm” y en la ventana confirmatoria Clic en “YES”', 218, 550),
(494, 'Seguimiento al proceso', 'Clic derecho al sysout y validar que el proceso este en ejecución\r\n', 218, 551),
(495, 'ENTREGA DE RESULTADOS', 'Se debe validar que se no hallan facturas del ciclo para numerar, para esto se debe ejecutar nuevamente el proceso de validación antes de la numeración (Ver manual 1030 - Validar Numero de Cuentas Antes de La Numeración), tanto el mail como el sysout arrojan el siguiente resultado', 218, 552),
(496, 'Posibles Errores', 'Se encuentran facturas sin numerar, se debe informar vía mail y telefónicamente a Diana Salamanca para que verifique el error.', 218, 553),
(497, 'Ejecución', 'Al finalizar de manera correcta el proceso de numeración, en la carpeta Ciclo identificar y ejecutar el JOb CARGOS_Y_MONTOS , el cual se encarga de generar un reporte para la gerencia con los cargos \"(valores de planes)\" y cobros \"(recaudos por ese concepto)\" durante el ciclo en ejecución.', 219, 554),
(498, 'Seguimiento al proceso', 'Al finalizar la ejecución del proceso, validar en el sysout (Output), que finalice sin errores.', 219, 555),
(499, 'Posibles Errores', 'Cualquier error ORA-xxx que presente el proceso, deben informar inmediatamente al especialista de Billing en soporte y esperar sus indicaciones.\r\n', 219, 556),
(500, 'Ejecución del proceso', 'Ingresar a Control M, buscar el JOB BITAS042 y verificarlo', 220, 557),
(501, 'Segundo paso', 'Damos Clic derecho “Confirm” y en la ventana de confirmación clic en “YES”', 220, 558),
(502, 'Seguimiento del proceso', 'Se debe abrir una conexión putty en la BD (10.69.34.34) usuario reporope para monitorear los archivo REPIMP_FAC_mmddaaaa.txt y REPIMP_FAC_mmddaaaa.log, se debe garantizar que el archivo txt este escribiendo la información y el log no presente errores.Recuerde que se debe abrir un putty por monitoreo, ya que la BD no permite cancelar los monitoreos con control + c\r\n', 220, 559),
(503, 'ENTREGA DE RESULTADOS', 'Se debe recibir un mail como el siguiente:', 220, 560),
(504, 'ENTREGA DE RESULTADOS PASO 2', 'Se debe enviar un correo con las siguientes características:\r\n\r\nPLANTILLA MAIL: REPORTE IMPUESTO FACTURADO\r\nLISTA DE DISTRIBUCION: REPORTE IMPUESTO FACTURADO CICLO\r\nARCHIVO ADJUNTO: Se debe adjuntar el archivo log del proceso\r\nEl mail se debe enviar a las siguientes personas: Muñoz, Magnolia; Ramiro Ayarza Santos; Roys, Dilia; Ruiz, Danny; Yepes, Iliana; \'info@menestys-services.com\'; \'james.alvarez@mail.tigo.com.co\'; \'july.zuluaga@mail.tigo.com.co\'; \'ramiro.ayarza@mail.tigo.com.co\'', 220, 561),
(505, 'Posibles Errores', 'Si el Job termina en rojo se debe validar el sysout en busca del error, si es por transferencia del archivo se debe realizar el proceso de forma manual trasfiriéndolo desde la ruta /usr/home/Reportes/Impuestos/DepMun y desbloquear el usuario operador con la herramienta determinada.\r\nSi el error es de ejecución en el proceso se debe escalar al Soporte disponible de infranet para que verifique y autorice su ejecución nuevamente.\r\nSi el error es de base de datos, se debe escalar al Soporte disponible de Base de Datos DBA para que corrija y se ejecute nuevamente el proceso.', 220, 562),
(507, 'Ruta de Origen', '/brm/data/invoice_dir/billMMDDAAAA', 221, 564),
(508, 'Ruta de Destino', 'F:\\Tigo_Edelivery\\Archivos_MMDDAAAA\\MMDDAAAA_E1\\billMMDDAAAA\r\n\r\n', 221, 565),
(509, 'Servidor BRM', 'Se adjunta la imagen', 221, 566),
(510, 'Correo proceso Exclusiones de Ciclo', 'En la imagen de EXCLUSIONES DE CICLO se debe tener presente que en:” Cuentas en la entrega:” el valor sea igual, en caso contrario se debe revisa en la carpeta billMMDDAAA (servidor PITNEY) si existen xslt con tamaño cero, para este caso, se retirar a una carpeta aparte y enviamos correo con estas xslt con tamaño cero al especialista de Billing que esta de soporte', 221, 567),
(511, 'Servidor Pitney', 'Se adjunta la imagen', 221, 568),
(512, 'Ruta Origen (10.69.32.227)', '/brm/pin/7.4/millicom_custom/scripts/billing/exclusiones_ciclo/archivos_enviados\r\n\r\n', 222, 569),
(513, 'Ruta De Destino (10.69.34.116 o 10.69.34.118)', 'F:\\Tigo_Edelivery\\Archivos_MMDDAAAA\\MMDDAAAA_E1', 222, 570),
(514, 'Archivos', 'Los archivos se transfieren de manera automática a los servidores de Pitney y con el formato MMDDAAAA, en caso que estos no transfieran de manera automática ir a la ruta de origen, para realizar la transferencia de manera manual, también cambiar el formato de fecha del archivo de DDMMAAA a MMDDAAAA\r\n\r\n', 222, 571),
(515, 'Ejemplo', 'Al finalizar el proceso de Exclusiones de Ciclo,  este envía un correo el cual contiene los siguientes  3 archivos adjuntos:\r\n\r\nBD_errores_DDMMAAAA.csv: Este archivo contiene las cuentas de las facturas que tiene algún  error ya sea por problemas en los cobros y por tal motivo no deben distribuirse por ningún medio.\r\n', 222, 572),
(516, 'paso 2', 'BD_excluir_facturas_DDMMAAAA.txt: Este archivo contiene las cuentas de las facturas que tiene algún tipo de exclusión, la estructura del archivo es tal como se muestra en la imagen:', 222, 573),
(517, 'Tipos de Exclusiones', 'Para mostrar el tipo de exclusiones, se muestra el siguiente cuadro', 222, 574),
(518, 'El correo', 'BD_excluir_facturas_DDMMAAAA.txt_###: Este archivo contiene la cantidad del archivo original (BD_excluir_facturas_DDMMAAAA.txt) más la cantidad que  se inserta de manera automática.\r\n\r\nEn la siguiente imagen se explica más en detalle el proceso a realizar.\r\n', 222, 575),
(519, 'Archivo', 'BD_solicitud_muestras_DDMMAAAA.csv: Este archivo contiene las cuentas de las facturas a las cuales se les genera la factura en formato pdf y deben ser enviadas al especialista de Billing que este de soporte para su revisión.', 222, 576),
(520, 'ControlLlamadas_mmddaaaa.txt', 'Este archivo contiene las cuentas junto con el número de eventos efectuados por el usuario.', 223, 577),
(521, 'Datos de origen', 'Servidor: Rtbilling(10.65.145.9)', 223, 578),
(522, 'Ruta', 'Ruta: /rtb_cdrs/data, En esta ruta, el archivo esta con el formato de fecha  ControlLlamadas_aaaammdd.txt,  pero este se transfiere a la ruta de destino en formato ControlLlamadas_mmddaaaa.txt  (Solo es un archivo)\r\n', 223, 579),
(523, 'Datos de destino(10.69.34.116 – 10.69.34.118)', 'F:\\Tigo_Edelivery\\Archivos_01162017\\01162017_E1', 223, 580),
(524, 'Estructura del Archivo', 'A  continuación de muestra la estructura del archivo', 223, 581),
(525, 'Detalle de las llamadas', 'DetalleLlamadas_aaaammdd_###.txt: Como su nombre lo indica, estos archivos contienen el número de cuenta asociada a la factura y el detalle de llamada de correspondiente a cada llamada realizada por el usuario.', 223, 582),
(526, 'Datos de origen detalle llamada', 'Servidor: Rtbilling(10.65.145.9)', 223, 583),
(527, 'Ruta', ' En esta ruta, el archivo esta con el formato de fecha  DetallelLlamadas_aaaammdd_###.txt.\r\n\r\n### = Corresponde a la cantidad de archivo generados en el servidor de origen.\r\n', 223, 584),
(528, 'Datos de destino ', 'F:\\Tigo_Edelivery\\Archivos_mmddaaaa\\RealTimeBilling_mmddaaaa\r\n\r\nEn dicha ruta se debe verificar que la cantidad transferida, sea la misma cantidad que está en el servidor de origen.', 223, 585),
(529, 'Estructura del Archivo de salida', 'Se muestra la estructura del archivo', 223, 586),
(530, 'Descripción', 'Este archivo contiene las cuentas de las facturas asociadas a alguna reclamación realizada por el usuario, es decir que si  al cliente actualmente le está llegando la factura detallada, pero solo quiere que le llegue resumida, entonces en el archivo debe estar la cuenta separado por (punto y coma); y para este caso llevaría el código 9,  que indica que la factura debe llegarle al cliente resumida.', 224, 587),
(531, 'Uso', 'Este archivo se actualiza de manera automática, en los servidores de Pitney el responsable de este archivo es el señor Paolo Vega. La estructura del archivo es la siguiente:\r\n\r\n', 224, 588),
(532, 'Tipo de relamaciones', 'Se adjunta tabla con los tipos de reclamaciones', 224, 589),
(533, 'Ruta', 'Ruta ubicación del archivo: F:\\TIgo_Edelivery\\Insumos (10.69.34.116 – 10.69.34.118)\r\n', 225, 590),
(534, 'Nota', 'Nota: Las cartas se envían según sea requerido por el área  solicitante, por lo general Paolo Vega  es la persona intermediaria quien pide estas solicitudes.\r\n\r\nademás,Este archivo debe permanecer vacío, en caso que no haya solicitudes activas de envíos de cartas anexa a la factura.\r\n', 225, 591),
(535, 'Ruta', 'F:\\TIgo_Edelivery\\Insumos (10.69.34.116 – 10.69.34.118)', 226, 592),
(536, 'Nota', 'Las cartas se envían según sea requerido por el área  solicitante, por lo general Paolo Vega  es la persona intermediaria quien pide estas solicitudes.\r\nEste archivo debe permanecer vacío, en caso que no haya solicitudes activas de envíos de cartas anexa a la factura.', 226, 593),
(537, 'Ruta de Origen', '/brm/pin/7.4/millicom_custom/scripts/clausulas/\r\n\r\nEl archivo se genera y transfiere a Pitney automáticamente entre las 08:00 y 08:30 ', 227, 594),
(538, 'Ruta destino', 'F:\\TIgo_Edelivery\\Insumos (10.69.34.116 – 10.69.34.118)', 227, 595),
(539, 'Estructura del Archivo ', 'Se adjunta la estructura del archivo', 227, 596),
(540, 'Descripción', 'Este archivo contiene las cuentas de las facturas  junto con el correo electrónico, su función es determinar por qué medio (físico y/o electrónico) se va a entregar la factura al cliente.\r\n\r\nEl último registro del archivo, corresponde a la fecha actual, en caso que esta fecha no corresponda a la del dia actual, el proceso genera error.\r\n', 228, 597),
(541, 'Ruta de Origen', '/brm/pin/7.4/millicom_custom/scripts/billing/factura_mail\r\n\r\nEl archivo se genera y transfiere a Pitney automáticamente entre las 00:30 y 01:30 \r\n', 228, 598),
(542, 'Ruta destino', ' F:\\TIgo_Edelivery\\Insumos (10.69.34.116 – 10.69.34.118)', 228, 599),
(543, 'Estructura del Archivo ', 'Imagen de la estructura del archivo', 228, 600),
(544, 'Tipos de envío', 'se adjunta los tipos de envío', 228, 601);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--

CREATE TABLE `ciclo` (
  `idciclos` int(11) NOT NULL,
  `nombre_ciclo` varchar(100) DEFAULT NULL,
  `descripcion_ciclo` varchar(5000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escalamiento`
--

CREATE TABLE `escalamiento` (
  `cod_escalamiento` int(11) NOT NULL,
  `area_encargada` varchar(20) DEFAULT NULL,
  `primer_contacto` varchar(100) DEFAULT NULL,
  `email_primer_contacto` varchar(100) DEFAULT NULL,
  `telefono_primer_contacto` varchar(30) DEFAULT NULL,
  `segundo_contacto` varchar(100) DEFAULT NULL,
  `email_segundo_contacto` varchar(100) DEFAULT NULL,
  `telefono_segundo_contacto` varchar(30) DEFAULT NULL,
  `proceso_idProceso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `escalamiento`
--

INSERT INTO `escalamiento` (`cod_escalamiento`, `area_encargada`, `primer_contacto`, `email_primer_contacto`, `telefono_primer_contacto`, `segundo_contacto`, `email_segundo_contacto`, `telefono_segundo_contacto`, `proceso_idProceso`) VALUES
(4, 'DWH', 'David Bonilla', 'david.bonilla@tigoune.com', '3006119488', 'Margarita Valiente', 'margarita.valiente@tigoune.com', '3012408312', 146),
(5, 'BRM', 'Jaime Ibarra', 'jaime.ibarra@TigoUne.com', '3016150304', 'Edilson Florez', 'Edilson.Florez@TigoUne.com', '3007614392', 162),
(6, 'CARTERA', 'Luz Fandiño', 'luz.fandino@tigo.com.co', '3017965586', '', '', '', 161),
(7, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 165),
(8, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 114),
(9, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 163),
(10, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 164),
(11, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 104),
(12, 'NOC', 'Disponible', 'SoporteAPP@TigoUne.com', '0515156270', '', '', '', 102),
(13, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 105),
(14, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 106),
(15, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 107),
(16, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 110),
(17, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 116),
(18, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 118),
(19, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 113),
(20, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 121),
(21, 'PROYECTOS FACTURACIÓ', 'Edna Perilla', 'Edna.Perilla@TigoUne.com', '3102732487', '', '', '', 122),
(22, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 124),
(23, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 125),
(24, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 126),
(25, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 127),
(26, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 129),
(27, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 130),
(28, 'NOC crear incidente', 'Disponible', 'SoporteAPP@TigoUne.com', '0515156270', '', '', '', 131),
(29, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 135),
(30, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 136),
(31, 'CARTERA', 'Luz Fandiño', 'luz.fandino@tigo.com.co', '3017965586', '', '', '', 138),
(32, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 145),
(33, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 147),
(34, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 149),
(35, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 159),
(36, 'SOPORTE BRM', 'Soporte BRM', '', '', '', '', '', 152),
(37, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 153),
(38, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 154),
(39, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 155),
(40, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 157),
(41, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 156),
(42, 'CARTERA', 'Luz Fandiño', 'luz.fandino@tigo.com.co', '3017965586', '', '', '', 201),
(43, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 203),
(44, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 202),
(45, 'BRM', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 204),
(46, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 205),
(47, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 206),
(48, 'CARTERA', 'Alexander Prada', 'Alexander.Prada@TigoUne.com', '3002115918', 'Manuel Peñafiel', 'Manuel.Penafiel@TigoUne.com', '3004913871', 207),
(49, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 208),
(50, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 209),
(51, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 210),
(52, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 211),
(53, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 212),
(54, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 213),
(55, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 214),
(56, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 215),
(57, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 216),
(58, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 217),
(59, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 218),
(60, 'BILLING DISPONIBLE', 'Disponible Según cronograma enviado	', '', '', '', '', '', 219),
(61, 'BRM', 'Jaime Ibarra', 'Jaime.Ibarra@TigoUne.com', '3016150304', 'Edilson Florez ', 'Edilson.Florez@TigoUne.com', '3007614392', 220);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE `imagen` (
  `idimagen` int(11) NOT NULL,
  `nombre_imagen` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imagen`
--

INSERT INTO `imagen` (`idimagen`, `nombre_imagen`) VALUES
(81, 'ajustes portal CRM.png'),
(82, 'correo ajustes portal CRM.PNG'),
(83, 'monitoreo ajustes CRM.png'),
(84, ''),
(85, 'monitoreo pagos y reconexiones.png'),
(86, ''),
(87, 'pr-cargar.png'),
(88, ''),
(89, 'sox-archivos.PNG'),
(90, 'sox-correo.PNG'),
(91, ''),
(92, ''),
(93, 'correo-tasacion.PNG'),
(94, ''),
(95, ''),
(96, 'correo-tasacioniel.PNG'),
(97, ''),
(98, 'correo-allocate.PNG'),
(99, ''),
(100, ''),
(101, ''),
(102, ''),
(103, 'correo-cartera.PNG'),
(104, ''),
(105, 'correo-colection.PNG'),
(106, 'correo-pagosdiarios.PNG'),
(107, ''),
(108, 'ruta-error.png'),
(109, ''),
(110, 'validacion-cuentas-billing.PNG'),
(111, 'correo-validacion-saldos.png'),
(112, ''),
(113, ''),
(114, 'recarga-ciclos.png'),
(115, ''),
(116, 'recaudos-listos-para-aplicar.PNG'),
(117, ''),
(118, ''),
(119, 'PR-ASURZ-E-CONCL-GESTR.png'),
(120, 'job-transferencia-de-archivos.png'),
(121, 'pagos-cheques-CDV.png'),
(122, ''),
(123, 'ajustes-colombia-CDV.png'),
(124, ''),
(125, 'archivo-colombia-credivalores.png'),
(126, ''),
(127, 'monitor-iel.png'),
(128, 'pagos-iel.PNG'),
(129, 'reactivaciones-1.png'),
(130, 'reactivaciones-2.png'),
(131, 'Verificar-el-estado-de-la-carga-de-servicios-y-enviar-correo.png'),
(132, 'verficar-llegada-correo-control-SOX-P4SC8.PNG'),
(133, 'rt-billing-generados.PNG'),
(134, ''),
(135, 'rt-billing-generados.PNG'),
(136, ''),
(137, 'correo-lentitud-IVR.PNG'),
(138, 'correo-aperturas-CDV.PNG'),
(139, ''),
(140, 'baja-pipeline-concatenacion.png'),
(141, 'correo-control-P18SC1.PNG'),
(142, ''),
(143, ''),
(144, ''),
(145, 'correo-ambiente-solicitud.png'),
(146, 'archivos-de-facturacion.PNG'),
(147, ''),
(148, 'correo-pagos-manuales.png'),
(149, 'cuentas-cobro-pyme.PNG'),
(150, 'compra-promociones.png'),
(151, 'tax-barranquilla.PNG'),
(152, 'archivos-credivalores.PNG'),
(153, 'ruta-credivalores.png'),
(154, 'estado-servicios-antes-billing.PNG'),
(155, 'estandar-cod-dane.png'),
(156, 'ajuste-ambiente-billing-de-ciclo.png'),
(157, 'pre-billing.PNG'),
(158, 'billing-de-ciclo.PNG'),
(159, ''),
(160, 'real-time-billing.png'),
(161, 'ajustes-ciclo.PNG'),
(162, ''),
(163, 'correo-pos-billing.PNG'),
(164, 'correo-ITEM-VS-BILL-Y-VERIFICAR-MAIL-TEM-VS-BIL.PNG'),
(165, 'bill-in-progress.png'),
(166, 'borrado-archivos-billing.png'),
(167, 'fin-cifras-control.PNG'),
(168, 'lista-blanca-roaming.png'),
(169, 'correo-info-inscritos.PNG'),
(170, ''),
(171, 'ambiente-billing-opcion.png'),
(172, 'ejecucion-billing-opcion.png'),
(173, 'cancelacion-promociones.PNG'),
(174, 'control-de-cambios-voz.png'),
(175, 'concilia-fase-uno.PNG'),
(176, 'concilia-fase-dos.PNG'),
(177, 'concilia-fase-tres.PNG'),
(185, 'cc-servidor.PNG'),
(186, 'cc-credenciales.PNG'),
(187, 'cc-PLSQL.PNG'),
(188, 'cc-conexion.PNG'),
(189, 'cc-conexion.PNG'),
(190, ''),
(191, ''),
(192, ''),
(193, ''),
(194, ''),
(195, 'callgate-arquitectura.PNG'),
(196, 'callgate-gestionServidores.PNG'),
(197, ''),
(198, 'callgate-1.1.PNG'),
(199, 'callgate-1.2.PNG'),
(200, 'callgate-1.3.PNG'),
(201, 'callgate-1.4.PNG'),
(202, 'callgate-1.5.PNG'),
(203, 'callgate-1.6.PNG'),
(204, 'callgate-1.1.PNG'),
(205, 'callgate-2.1.PNG'),
(206, 'callgate-2.2.PNG'),
(207, 'callgate-2.3.PNG'),
(208, 'callgate-2.4.PNG'),
(209, 'cambios-contraseña-ingreso.PNG'),
(210, 'cambios-contraseña-servicios.PNG'),
(211, 'cambios-contraseña-diligenciamiento.PNG'),
(212, 'inpoconsumo-correo.PNG'),
(213, 'correo-incidencias.PNG'),
(214, 'correo-aplicar-ajustes-ITEM.PNG'),
(215, 'correo-recordatorio-pago.PNG'),
(216, 'recordatorio-pago-2.PNG'),
(217, 'recordatorio-pago-3.PNG'),
(218, 'recordatorio-pago-7.PNG'),
(219, 'recordatorio-pago-8.PNG'),
(220, 'recordatorio-pago-9.PNG'),
(221, 'recordatorio-pago-11.PNG'),
(222, 'recordatorio-pago-12.PNG'),
(223, 'recordatorio-pago-13.PNG'),
(224, 'recordatorio-pago-14.PNG'),
(225, 'recordatorio-pago-15.PNG'),
(226, 'vef-1.PNG'),
(227, 'vef-2.PNG'),
(228, 'vef-3.PNG'),
(229, 'vef-4.PNG'),
(230, 'vef-5.PNG'),
(231, 'vef-6.PNG'),
(232, 'vef-7.PNG'),
(233, 'vef-8.PNG'),
(234, 'vef-9.PNG'),
(235, 'vef-10.PNG'),
(236, 'vef-11.PNG'),
(237, 'vef-12.PNG'),
(238, 'vef-13.PNG'),
(239, 'vef-14.PNG'),
(240, 'vef-15.PNG'),
(241, 'vef-16.PNG'),
(242, 'vef-17.PNG'),
(243, ''),
(244, 'vef-19.PNG'),
(245, 'vef-20.PNG'),
(246, 'vef-21.PNG'),
(247, 'generacion-con-tiempos-1.PNG'),
(248, 'generacion-con-tiempos-2.PNG'),
(249, 'generacion-con-tiempos-3.PNG'),
(250, 'generacion-con-tiempos-4.PNG'),
(251, 'generacion-con-tiempos-5.PNG'),
(252, 'generacion-con-tiempos-6.PNG'),
(253, 'generacion-con-tiempos-7.PNG'),
(254, ''),
(255, ''),
(256, 'tiempos-ciclos-1.PNG'),
(257, 'tiempos-ciclos-2.PNG'),
(258, 'tiempos-ciclos-3.PNG'),
(259, 'reporte-plan-produccion-1.PNG'),
(260, ''),
(261, ''),
(262, 'disputa-xslt-1.PNG'),
(263, ''),
(264, ''),
(265, 'disputa-xslt-2.PNG'),
(266, 'disputa-xslt-3.PNG'),
(267, 'disputa-xslt-4.PNG'),
(268, ''),
(269, 'disputa-xslt-5.PNG'),
(270, 'disputa-xslt-6.PNG'),
(271, 'descarga-vault-1.PNG'),
(272, 'descarga-vault-2.PNG'),
(273, 'descarga-vault-3.PNG'),
(274, ''),
(275, 'reproceso-envio-email-1.PNG'),
(276, ''),
(277, 'reproceso-envio-email-2.PNG'),
(278, 'reproceso-envio-email-3.PNG'),
(279, 'reproceso-envio-email-4.PNG'),
(280, 'reproceso-envio-email-5.PNG'),
(281, 'reproceso-envio-email-6.PNG'),
(282, 'reproceso-envio-email-7.PNG'),
(283, 'reproceso-envio-email-8.PNG'),
(284, 'reproceso-envio-email-9.PNG'),
(285, 'reproceso-envio-email-10.PNG'),
(286, 'reproceso-envio-email-11.PNG'),
(287, 'reproceso-envio-email-12.PNG'),
(288, 'reproceso-envio-email-13.PNG'),
(289, 'reproceso-envio-email-14.PNG'),
(290, 'recordatorio-pago-15.PNG'),
(291, ''),
(292, 'infoinscritos-1.PNG'),
(293, 'infoinscritos-2.PNG'),
(294, 'infoinscritos-3.PNG'),
(295, ''),
(296, 'recuperacion-servidor-pitney.PNG'),
(297, 'jrn-1.PNG'),
(298, 'jrn-2.PNG'),
(299, 'jrn-3.PNG'),
(300, ''),
(301, 'jrn-4.PNG'),
(302, 'jrn-5.PNG'),
(303, 'jrn-6.PNG'),
(304, 'jrn-7.PNG'),
(305, 'jrn-8.PNG'),
(306, 'jrn-9.PNG'),
(307, 'entregas-adicionales-rtb.PNG'),
(308, 'entregas-adicionales-rtb-1.PNG'),
(309, 'entregas-adicionales-rtb-2.PNG'),
(310, 'reinicio-envios.email-1.PNG'),
(311, 'reinicio-envios.email-2.PNG'),
(312, 'reinicio-envios.email-3.PNG'),
(313, 'reinicio-envios.email-4.PNG'),
(314, 'reinicio-envios.email-5.PNG'),
(315, ''),
(316, 'vault-1.PNG'),
(317, 'vault-2.PNG'),
(318, 'vault-3.PNG'),
(319, 'vault-4.PNG'),
(320, 'vault-5.PNG'),
(321, 'vault-6.PNG'),
(322, 'vault-7.PNG'),
(323, 'vault-8.PNG'),
(324, 'vault-9.PNG'),
(325, 'vault-10.PNG'),
(326, 'vault-11.PNG'),
(327, 'vault-12.PNG'),
(328, 'vault-13.PNG'),
(329, 'vault-14.PNG'),
(330, 'vault-15.PNG'),
(331, 'vault-16.PNG'),
(332, 'vault-17.PNG'),
(333, 'vault-18.PNG'),
(334, 'vault-19.PNG'),
(335, ''),
(336, ''),
(337, 'cubos-romaing-1.PNG'),
(338, 'cubos-romaing-2.PNG'),
(339, 'cubos-romaing-3.PNG'),
(340, 'cubos-romaing-4.PNG'),
(341, 'cubos-romaing-5.PNG'),
(342, 'cubos-romaing-6.PNG'),
(343, ''),
(344, ''),
(345, 'cubo-ict-1.PNG'),
(346, 'cubo-ict-2.PNG'),
(347, 'cubo-ict-4.PNG'),
(348, 'cubo-ict-5.PNG'),
(349, 'cubo-ict-6.PNG'),
(350, 'cubo-ict-7.PNG'),
(351, 'cubo-ict-8.PNG'),
(352, 'cubo-ict-9.PNG'),
(353, 'cubo-ict-10.PNG'),
(354, 'cubo-ict-11.PNG'),
(355, 'cubo-ict-12.PNG'),
(356, 'cubo-ict-13.PNG'),
(357, 'cubo-ict-14.PNG'),
(358, 'cubo-ict-15.PNG'),
(359, 'cubo-ict-16.PNG'),
(360, 'cubo-ict-17.PNG'),
(361, 'cubo-ict-18.PNG'),
(362, 'cubo-ict-19.PNG'),
(363, 'cubo-ict-20.PNG'),
(364, 'cubo-ict-21.PNG'),
(365, 'cubos-Interconexion-1.PNG'),
(366, 'cubos-Interconexion-2.PNG'),
(367, 'cubos-Interconexion-3.PNG'),
(368, 'cubos-Interconexion-4.PNG'),
(369, 'cubos-Interconexion-5.PNG'),
(370, 'cubos-Interconexion-6.PNG'),
(371, 'cargue-control-fac.PNG'),
(372, ''),
(373, 'cargue-control-fac-2.PNG'),
(374, ''),
(375, 'cargue-control-fac-3.PNG'),
(376, 'cargue-control-fac-4.PNG'),
(377, 'cargue-control-fac-5.PNG'),
(378, 'cargue-control-fac-6.PNG'),
(379, 'cargue-control-fac-7.PNG'),
(380, ''),
(381, 'cargue-control-fac-8.PNG'),
(382, ''),
(383, ''),
(384, 'envio-email-web-1.PNG'),
(385, 'envio-email-web-2.PNG'),
(386, 'envio-email-web-3.PNG'),
(387, 'envio-email-web-4.PNG'),
(388, 'envio-email-web-5.PNG'),
(389, 'envio-email-web-6.PNG'),
(390, 'envio-email-web-7.PNG'),
(391, 'envio-email-web-8.PNG'),
(392, 'envio-email-web-9.PNG'),
(393, 'envio-email-web-10.PNG'),
(394, 'envio-email-web-11.PNG'),
(395, ''),
(396, 'envio-email-web-12.PNG'),
(397, 'envio-email-web-13.PNG'),
(398, 'envio-email-web-14.PNG'),
(399, ''),
(400, ''),
(401, 'anomalias-1.PNG'),
(402, 'anomalias-2.PNG'),
(403, 'anomalias-3.PNG'),
(404, ''),
(405, 'vs-1.PNG'),
(406, 'vs-2.PNG'),
(407, 'vs-3.PNG'),
(408, 'vs-4.PNG'),
(409, 'vs-5.PNG'),
(410, 'vs-6.PNG'),
(411, 'vs-7.PNG'),
(412, 'vs-8.PNG'),
(413, 'vs-9.PNG'),
(414, 'vs-10.PNG'),
(415, 'vs-11.PNG'),
(416, 'vs-12.PNG'),
(417, 'vs-13.PNG'),
(418, ''),
(419, 'vs-14.PNG'),
(420, 'vs-15.PNG'),
(421, ''),
(422, 'vs-16.PNG'),
(423, 'vs-17.PNG'),
(424, ''),
(425, 'vb-1.PNG'),
(426, 'vb-2.PNG'),
(427, 'vb-3.PNG'),
(428, 'vb-4.PNG'),
(429, ''),
(430, ''),
(431, 'vb-5.PNG'),
(432, ''),
(433, ''),
(434, ''),
(435, ''),
(436, 'fr-1.PNG'),
(437, 'fr-2.PNG'),
(438, 'fr-3.PNG'),
(439, ''),
(440, ''),
(441, ''),
(442, 'sic-1.PNG'),
(443, 'sic-2.PNG'),
(444, 'sic-3.PNG'),
(445, 'sic-4.PNG'),
(446, 'sic-5.PNG'),
(447, 'sic-6.PNG'),
(448, 'sic-7.PNG'),
(449, 'sic-8.PNG'),
(450, 'sic-9.PNG'),
(451, 'sic-10.PNG'),
(452, 'sic-11.PNG'),
(453, 'pagos-ajustes-1.PNG'),
(454, 'pagos-ajustes-2.PNG'),
(455, 'pagos-ajustes-3.PNG'),
(456, 'pagos-ajustes-4.PNG'),
(457, 'pagos-ajustes-5.PNG'),
(458, ''),
(459, ''),
(460, ''),
(461, 'concilacion-aso.PNG'),
(462, 'concilacion-aso-1.PNG'),
(463, 'concilacion-aso-2.PNG'),
(464, 'concilacion-aso-3.PNG'),
(465, 'concilacion-aso-4.PNG'),
(466, 'concilacion-aso-5.PNG'),
(467, 'concilacion-aso-6.PNG'),
(468, 'concilacion-aso-7.PNG'),
(469, 'concilacion-aso-.PNG'),
(470, 'rtbillling-1.PNG'),
(471, 'rtbillling-2.PNG'),
(472, 'rtbillling-3.PNG'),
(473, 'rtbillling-4.PNG'),
(474, 'rtbillling-5.PNG'),
(475, 'rtbillling-6.PNG'),
(476, 'rtbillling-7.PNG'),
(477, 'pp-1.PNG'),
(478, 'pp-2.PNG'),
(479, 'pp-3.PNG'),
(480, 'pp-4.PNG'),
(481, 'pp-5.PNG'),
(482, 'gcdc-1.PNG'),
(483, 'gcdc-2.PNG'),
(484, 'gcdc-3.PNG'),
(485, 'gcdc-4.PNG'),
(486, 'gcdc-5.PNG'),
(487, 'gcdc-6.PNG'),
(488, 'gcdc-7.PNG'),
(489, 'gcdc-8.PNG'),
(490, ''),
(491, 'fi-1.PNG'),
(492, 'fi-2.PNG'),
(493, 'fi-3.PNG'),
(494, 'fi-4.PNG'),
(495, ''),
(496, 'pabc-1.PNG'),
(497, 'pabc-2.PNG'),
(498, 'pabc-3.PNG'),
(499, 'pabc-4.PNG'),
(500, 'pabc-5.PNG'),
(501, ''),
(502, ''),
(503, 'gccai-1.PNG'),
(504, 'gccai-2.PNG'),
(505, 'gccai-3.PNG'),
(506, 'gccai-4.PNG'),
(507, ''),
(508, 'gccai-5.PNG'),
(509, ''),
(510, 'aaic-1.PNG'),
(511, 'aaic-2.PNG'),
(512, 'aaic-3.PNG'),
(513, 'aaic-4.PNG'),
(514, 'aaic-5.PNG'),
(515, 'aaic-6.PNG'),
(516, 'aaic-7.PNG'),
(517, 'aaic-8.PNG'),
(518, 'aaic-9.PNG'),
(519, 'aaic-10.PNG'),
(520, 'aaic-11.PNG'),
(521, 'aaic-12.PNG'),
(522, 'aaic-13.PNG'),
(523, 'aaic-14.PNG'),
(524, 'aaic-15.PNG'),
(525, 'aaic-16.PNG'),
(526, ''),
(527, ''),
(528, 'gccdi-1.PNG'),
(529, 'gccdi-2.PNG'),
(530, 'gccdi-3.PNG'),
(531, 'gccdi-4.PNG'),
(532, ''),
(533, 'gccdi-5.PNG'),
(534, ''),
(535, 'validar-invoice-duplicados-1.PNG'),
(536, 'validar-invoice-duplicados-2.PNG'),
(537, 'validar-invoice-duplicados-3.PNG'),
(538, 'validar-invoice-duplicados-4.PNG'),
(539, 'validar-invoice-duplicados-5.PNG'),
(540, 'validar-invoice-duplicados-6.PNG'),
(541, 'validar-invoice-duplicados-7.PNG'),
(542, 'validar-invoice-duplicados-8.PNG'),
(543, ''),
(544, 'numeracion-antes-1.PNG'),
(545, 'numeracion-antes-2.PNG'),
(546, 'numeracion-antes-3.PNG'),
(547, 'numeracion-antes-4.PNG'),
(548, ''),
(549, 'numeracion-1.PNG'),
(550, 'numeracion-2.PNG'),
(551, 'numeracion-3.PNG'),
(552, 'numeracion-4.PNG'),
(553, ''),
(554, 'cargos-montos-1.PNG'),
(555, 'cargos-montos-2.PNG'),
(556, ''),
(557, 'rp-impuesto-facurado-1.PNG'),
(558, 'rp-impuesto-facurado-2.PNG'),
(559, 'rp-impuesto-facurado-3.PNG'),
(560, 'rp-impuesto-facurado-4.PNG'),
(561, 'rp-impuesto-facurado-5.PNG'),
(562, ''),
(563, ''),
(564, ''),
(565, ''),
(566, 'xslt-1.PNG'),
(567, 'xslt-2.PNG'),
(568, 'xslt-3.PNG'),
(569, ''),
(570, ''),
(571, 'archivos-exclusiones-1.PNG'),
(572, 'archivos-exclusiones-2.PNG'),
(573, 'archivos-exclusiones-3.PNG'),
(574, 'archivos-exclusiones-4.PNG'),
(575, 'archivos-exclusiones-5.PNG'),
(576, 'archivos-exclusiones-6.PNG'),
(577, ''),
(578, ''),
(579, ''),
(580, ''),
(581, 'archivos-rtb-1.PNG'),
(582, ''),
(583, ''),
(584, ''),
(585, ''),
(586, 'archivos-rtb-2.PNG'),
(587, ''),
(588, 'base-auto-1.PNG'),
(589, 'base-auto-2.PNG'),
(590, ''),
(591, 'base-cartas-1.PNG'),
(592, ''),
(593, 'info-cartas-1.PNG'),
(594, ''),
(595, ''),
(596, 'detalle-clausulas-1.PNG'),
(597, ''),
(598, ''),
(599, ''),
(600, 'info-inscritos-1.PNG'),
(601, 'info-inscritos-2.PNG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `idProceso` int(11) NOT NULL,
  `nombre_proceso` varchar(100) DEFAULT NULL,
  `intro_proceso` varchar(5000) DEFAULT NULL,
  `turno_idturno` int(11) NOT NULL,
  `estado_proceso` varchar(20) NOT NULL DEFAULT 'Activo',
  `tipo_proceso_idtipo_proceso` int(11) NOT NULL,
  `usuario_idusuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`idProceso`, `nombre_proceso`, `intro_proceso`, `turno_idturno`, `estado_proceso`, `tipo_proceso_idtipo_proceso`, `usuario_idusuario`) VALUES
(102, 'Aplicar ajustes portal CRM', 'Se ejecuta 3 veces al día a las 6AM, 3PM y 9PM; son ajustes que se encuentran debidamente aprobados por parte de las tiendas de Tigo ya sea a favor o en contra del cliente.', 1, 'Activo', 1, NULL),
(103, 'Monitoreo de pagos y reconexiones', 'Es un proceso que se hace diario en el turno de la mañana, como su nombre lo dice, se debe de monitorear .', 3, 'Activo', 1, NULL),
(104, 'Proceso de iral retail todos los servicios', 'Es un proceso que se hace diario en la mañana', 3, 'Activo', 1, NULL),
(105, 'Reporte control SOX P17 IC8', 'Es un proceso que se ejecuta diario a las 6:AM,es un proceso automático que se ejecuta a las 6AM, se encuentra en la malla de Facturación Diario; este proceso genera un reporte diario de las tarifas manejadas por Tigo Une en cada uno de los planes postpago que están en la plataforma.', 1, 'Activo', 1, NULL),
(106, 'Reporte Tasación', 'Este proceso se ejecuta a diario,lo que hace este reporte es generar un reporte con los eventos tasados por el mediador y alojados en la tabla de la base de datos de BRM', 1, 'Activo', 1, NULL),
(107, 'Reporte tasación iel', 'Este proceso se ejecuta a diario,lo que hace este reporte es generar un reporte con los eventos tasados por el mediador y alojados en la tabla de la base de datos de BRM, cabe destacar que es proceso va de la mano con el proceso de: Reporte tasación', 1, 'Activo', 1, NULL),
(108, 'Verificar cronograma de generación de medios a ajecutar', 'Se valida en el correo \"Cronograma de medios\", el archivo adjunto es decir el cronograma en excel, tenga los medios concordantes a la fecha actual o para el día', 1, 'Activo', 1, NULL),
(109, 'Veriicar ejecución de todos los reportesde de tasación hasta la fecha', 'Es un proceso que se hace en la mañana con la finalidad de verificar la ejecución de los reportes que se hacen en la tasación hasta la fecha a', 1, 'Activo', 1, NULL),
(110, 'New allocate payments antes billyng', 'Se ejecuta todos los días a las 7AM y se puede correr en paralelo con el Billing de ciclo, es un proceso de alocación de pagos y ajustes el cual se encarga de aplicar los ajustes que se encuentran listos en las tablas correspondientes, se encuentra en la malla de Facturación Diario. ', 1, 'Activo', 1, NULL),
(111, 'Pipeline retail tasación (verificar si está arriba)', 'Proceso que se hace de manera diaria en el turno de la mañana ', 3, 'Activo', 1, NULL),
(112, 'Cartera', 'También se conoce como Cartera Diario, este proceso genera un reporte con saldos de mora de las cuentas con cartera activa, se ejecuta todos los días, se encuentra en la malla de Varios Automáticos', 1, 'Activo', 1, NULL),
(113, 'Collection Suspended', 'Es un proceso automático que se encuentra en la malla de Varios Automáticos, este se ejecuta a las 8AM; es un proceso de actualización para las cuentas con ciclos rezagados', 1, 'Activo', 1, NULL),
(114, 'RPT pagos diarios mañana', 'Es un proceso diario, se encuentra en la malla como (Reporte Pagos Diarios_1).\r\nGenera un reporte con los pagos diarios que realizan los usuarios a Colombia Móvil con los diferentes medios dispuestos', 1, 'Activo', 1, NULL),
(115, 'Monitoreo de archivos out en ruta de error', 'Es un proceso que se hace con el fin de verificar los archivos generados en la ruta de error', 3, 'Activo', 1, NULL),
(116, 'Ejecutar validación de cuentas de billin', 'Proceso diario del turno de la mañana,se encuentra en la malla de Facturación Diario el job  y es automático ejecutándose a las 07:15; este proceso genera un reporte y consulta las cuentas hijas sin padre que contiene cada ciclo.', 1, 'Activo', 1, NULL),
(117, 'Verificar fin proceso validar saldos', 'Se encuentra en la malla Varios Automáticos, es un proceso Diario y automático; verificamos que el proceso haya iniciado sin errores en el Output. Verifica que el proceso realice el conteo para reactivaciones y suspensiones de las líneas en mora.', 3, 'Activo', 1, NULL),
(118, 'Envío de archivos de recarga de ciclo', 'Este proceso no se encuentra en Control M, esto aplica del 1 al 28 que son los días de ciclo', 1, 'Activo', 1, NULL),
(119, 'Bajada pipeline retail tasación', 'Este no se baja ni los DOMINGOS ni los FESTIVOS, en semana se debe estar al día para bajarlo antes de las 8AM(solo de lunes a viernes)', 1, 'Activo', 1, NULL),
(120, 'Enviar mail con informe de recaudos listos para aplicar', 'Este correo se envía cuando ya se tienen los bancos, en la carpeta de enviados buscamos un correo con el siguiente asunto: “Asobancarías y Archivos para Recaudo del día DD/MM/YYYY”', 1, 'Activo', 1, NULL),
(121, 'Verificar inicio y fin de generación archivo de clausula', 'Se encuentra ubicado en la malla de ciclo, es automático, se ejecuta a las 8AM, dentro de los prerrequisitos que sea día de ciclo del 1 al 28, este genera un listado con los clientes que tienen algún tipo de clausula ya sea de equipo o del plan.', 1, 'Activo', 1, NULL),
(122, 'PR ASURZ E CONCL GESTR', 'Es un proceso automático, se ejecuta a diario a las 8AM, se encuentra en la malla Varios Automáticos', 1, 'Activo', 1, NULL),
(123, 'Límite consumo transferencia de archivo', 'Es un proceso automático diario y se ejecuta a las 08:50, que se encuentra en la malla de Facturación Diario', 1, 'Activo', 1, NULL),
(124, 'Pagos realizados cheques CDV', 'Es un proceso de Credi Valores, es automático que se ejecuta a diario a las 09:00AM, se encuentra en la malla CC&D Factory > Servicios_Finan', 2, 'Activo', 1, NULL),
(125, 'Archivos ajustes colombia CDV', 'Es un proceso de Credi Valores, es automático que se ejecuta a diario a las 09:00AM, se encuentra en la malla CC&D Factory > Servicios_Finan', 1, 'Activo', 1, NULL),
(126, 'Archivo recaudo Colombia CDV', 'Es un proceso de Credi Valores, es automático que se ejecuta a diario a las 09:00AM, se encuentra en la malla CC&D Factory > Servicios_Finan', 1, 'Activo', 1, NULL),
(127, 'Monitor iel', 'En Control M se encuentra ubicado en la malla Facturación Diario.\r\nEste ayuda al procesamiento de los recaudos', 3, 'Activo', 1, NULL),
(128, 'Verificación pagos iel', 'Es un proceso diario que se hace en la mañana, esta actividad se debe realizar después de la primera ejecución del monitor IEL; ', 1, 'Activo', 1, NULL),
(129, 'Reactivaciones lite (light)', 'El proceso se encuentra en la malla de Facturación Diario, se hace en la mañana de manera diaria', 1, 'Activo', 1, NULL),
(130, 'Verificar el estado de la carga de servicios y enviar correo', 'Es una actividad que se hace de manera que podamos verificar la carga de los servcios, se hace en la mañana de manera diaria', 1, 'Activo', 1, NULL),
(131, 'verficar llegada correo control SOX P4SC8', 'Es un reporte diario que contiene los pagos no aplicados en CRM, el job que genera el correo se llama “RUN P4SC8” se encuentra en la malla de Facturación Diario y es un proceso automático, llega un correo con el siguiente asunto: “Control Interno P4SC8 con recaudo del día YYYY/MM/DD” por lo general llega con fecha de 2 días antes.', 1, 'Activo', 1, NULL),
(132, 'Verificar correo de la ejecución del real time billing diario', 'Es un proceso que se hace en la mañana de manera diaria', 3, 'Activo', 1, NULL),
(133, 'Verificar correo de la ejecución del real time billing diario', 'Es un proceso que se hace en la mañana de manera diaria', 3, 'Activo', 1, NULL),
(134, 'VERIFICAR ARCHIVOS RTB YY ATP COBROS', 'Este proceso se ejecuta diariamente, es automático a las 05:00 se encuentra en la malla de Varios Automáticos, el job se llama “Proceso RTBilling Diario”; es el billing de prepago, el procesa las llamadas realizadas por los usuarios control y ATP en el servidor de prepago para los días de ciclo realizar el procesamiento, el servidor es el 10.65.145.9.', 1, 'Activo', 1, NULL),
(135, 'Consulta y solución de lentitud en el IVR', 'Este proceso se encuentra en la malla de Facturación Diario, es automático y diario, el job se llama “CONSULTA Y SOLUCIÓN DE LENTITUD EN EL IVR” es un proceso que verifica que no haya dificultades en las conexiones IVR utilizados por los usuarios de Colombia Móvil. ', 4, 'Activo', 1, NULL),
(136, 'Recuado aperturas CDV', 'Es un proceso de Credi Valores, es automático que se ejecuta a diario a las 11:00 AM, se encuentra en la malla CC&D Factory > Recaudo Aperturadas CDV', 1, 'Activo', 1, NULL),
(137, 'Verificar bajada, subida exitosa pipeline mediation concatenación', 'Es un proceso que se hace diario en la mañana, se encuentra en la malla Facturación Diario Pipeline se encuentran los Jobs', 1, 'Activo', 1, NULL),
(138, 'Gestión de servicios control P18SC1', 'El proceso llega en cualquier hora del día, llega con el siguiente asunto: “GESTION DE SERVICIOS CONTROL P18SC1_YYYY/MM/DD” este correo llega con el adjunto .tar,gz', 4, 'Activo', 1, NULL),
(139, 'Reporte cuenta efecty ', 'Es automático, se encuentra en la malla Varios Automáticos; este proceso crea un listado de nuevas cuentas de clientes de Colombia Móvil para Efecty, estas son cargadas en la base de datos de Efecty', 1, 'Activo', 1, NULL),
(140, 'Generación archivo limite de consumo (saldo cuentas)', 'Es un proceso diario que se hace en la mañana, para su ejecución hacemos lo siguiente', 1, 'Activo', 1, NULL),
(141, 'Ajustar ambiente de solicitud', 'Es un proceso que se hace de manera diaria, este proceso se hace en cualquier hora del día sólo si llega el correo', 4, 'Activo', 1, NULL),
(142, '	 Archivo de facturación (recaudos exitosos)', 'Proceso que se hace diario en ambos turnos,para este proceso,llega un correo con el siguiente asunto: “ARCHIVO DE FACTURACIÓN recaudos exitosos” .\r\nEste llega con un adjunto el cual guardamos en la carpeta donde se aplican los bancos', 1, 'Activo', 1, NULL),
(143, 'Pagos Manuales', 'Es un proceso que se hace en la tarde de forma diaria, de este llega un correo con el cual de debe hacer lo siguiente', 2, 'Activo', 1, NULL),
(144, 'Colocación cuentas de cobro pyme', 'Es un proceso que se hace en la tarde de manera diaria', 2, 'Activo', 1, NULL),
(145, 'Compra Promociones Ciclo', 'Se ejecuta a las 6AM, es un proceso automático que se encuentra en la malla Varios Automáticos; Son promociones que los clientes no activan o no usan y este proceso se encarga de simular la compra de estas promociones', 1, 'Activo', 1, NULL),
(146, 'VERIFICACIÓN ARCHIVO TAX BARRANQUILLA', 'Este archivo se valida todos los días de Ciclo', 1, 'Activo', 1, NULL),
(147, 'Verificación Archivo Credivalores', 'Proceso que se hace en horas de la mañana, de forma diaria', 1, 'Activo', 1, NULL),
(148, 'VERIFICAR ESTADO DE LOS SERVICIOS ANTES DE INICIAR EL BILLING', 'Proceso que se hace en la mañana de manera diaria', 1, 'Activo', 1, NULL),
(149, 'Estandar cod Dane', 'Es un proceso que realiza la corrección de código, ciudad y departamento de acuerdo a la información dada por el Dane, se hace en la mañana ', 1, 'Activo', 2, NULL),
(150, 'Ajuste ambiente billing de ciclo', 'Se encarga de ajustar los diferentes parámetros para que las maquinas 10.69.32.227 y 10.69.32.228 den recursos a la 10.69.32.229 donde se ejecuta el Billing y garantizar el procesamiento de todas las Bill; es adecuar las maquinas ya que el proceso es muy pesado en el procesamiento', 3, 'Activo', 1, NULL),
(151, 'Pre Billing', 'Realiza la marcación de las cuentas Bill (Cuentas Padres) las cuales no fueron tenidas en cuenta para ser procesadas por el Billing', 1, 'Activo', 1, NULL),
(152, 'Billing Ciclo', 'Este procesa las cuentas ITEM y BILL, las pagadoras que son las cuentas padres y las cuentas hijas; este proceso es incompatible con los REL, Pipeline Retail Tasación arriba, ya que este tiene un alto consumo de recursos en la máquina, con otros billing, el ajuste de Ambiente para billing, Ajuste de ambiente para Invoice porque estos procesos utilizan las mismas instancias', 1, 'Activo', 1, NULL),
(153, 'Ejecutar Real Time Billing de ciclo', 'Este proceso genera los archivos con los detalles del consumo para los usuarios ATP y Control', 1, 'Activo', 1, NULL),
(154, 'Generación Ajustes de ciclo', 'Genera los ajustes de acuerdo a lo encontrado por el proceso de Pre-ajustes; Cuando los pre-ajustes terminan, generan los ajustes de ciclo con el siguiente correo: “Ajustes Ciclo DD/MM/YYYY” llega con un adjunto o varios adjuntos', 1, 'Activo', 1, NULL),
(155, 'PosBilling', 'Realiza el reverso del Pre-billing es decir desmarca las cuentas Bill que se marcaron en el proceso de pre-billing', 1, 'Activo', 1, NULL),
(156, 'ITEM VS BILL Y VERIFICAR MAIL ITEM VS BIL', 'Este proceso realiza la verificación de las reglas ejecutadas en los pasos 1, 2 y 3 de confirmación por correo', 1, 'Activo', 1, NULL),
(157, 'Bill in progress', 'Es un proceso que se hace en la mañana de manera diaria', 1, 'Activo', 1, NULL),
(158, 'Borrado Archivos Inicio de Billing', 'Es un proceso que se hace en la mañana con el fin de verificar los archivos para el inicio del bllling', 1, 'Activo', 1, NULL),
(159, 'EJECUTAR CUENTAS A FACTURAR.SH #1 (CIFRAS DE CONTROL ANTES DE BILLING)', 'Se hace después del estandar cod Dane, Son la cantidad de cuentas a procesar por el billing clasificadas por estatus 0 y estatus 4 (Estatus 4 son las cuentas que tienen problemas o errores)', 1, 'Activo', 1, NULL),
(160, 'Lista blanca Roaming', 'Es un proceso que se hace en la noche diario', 3, 'Activo', 1, NULL),
(161, 'Info inscritos', 'Es un proceso automático, que se encuentra en la malla Facturación Diario, el job se llama INFO_INSCRITOS.\r\nEs un listado de todas las cuentas que incluye el correo electrónico del usuario y un Flag que indica si la factura va solo para impresión o solo Email', 3, 'Activo', 1, NULL),
(162, 'Billing Diario Opción 1', 'Se debe de ejecutar todos los días del 1 al 30, es la pre facturación.', 3, 'Activo', 1, NULL),
(163, 'Cancelación Promociones REQ 1007 infranet Siebel', 'Se realiza después de finalizar el Billing Diario; se encarga de generar un reporte con las líneas a las cuales se les deben cancelar las promociones asignadas.', 3, 'Activo', 2, NULL),
(164, 'Recargas Control Cambio de Ciclo después de Billing Diario día D ', 'Se debe ejecutar después Cancelación de Promociones Req1007 Infranet Siebel, se encuentra en la malla de facturación Diario, se ejecuta los días de ciclo con excepción de los días 17, 29, 30 y 31; Este proceso obtiene información de las líneas cuenta control a las cuales se les asignan recursos ya sea voz o datos.', 1, 'Activo', 2, NULL),
(165, 'Concilia Toil', 'Es un proceso diario el cual se debe hacer a la 01:00, este proceso genera unos archivos que se descargan de la plataforma PAY U; con estos archivos se realiza la conciliación entre lo procesado por el CRM y los archivos descargados.', 3, 'Activo', 1, NULL),
(173, 'Cambio de contraseña BD plan de Producción', 'Este es un proceso que se hace con el fin de cambiar la contraseña del plan de producción y mantenerlo actualizado, no requiere de un turno en específico, es controlado por el área de Pitney y se divide en varios pasos:', 5, 'Activo', 4, NULL),
(174, 'Comandos para reinicio de instancia y matado de hilos ', 'Es un  proceso que se hace por parte del área de pitney y que tiene el fin de mostrar los comando para el reinicio de las instancias', 5, 'Activo', 4, NULL),
(175, 'Reiniciar servicios Aplicaciones Callgate', 'Es un proceso que hace el área de pitney y que debe de llevar una serie de pasos para su ejecución, dentro del documento, se explican lo qué es y su arquitectura, seguido de los pasos\r\n', 5, 'Activo', 4, NULL),
(176, 'Cambios de Password', 'Por procedimiento establecido los cambios de password, los permisos especiales, así como la creación de usuarios nuevos se deben realizar por\r\ncatálogo de servicios', 5, 'Activo', 4, NULL),
(177, 'Ajustes IMPOCONSUMO Jerarquía', 'Es un correo que llega de manera diaria después de las 6 de la tarde, viene con un adjunto ', 2, 'Activo', 1, NULL),
(178, 'Solicitud de Procesos para Entregar al área de PQR Incidencia - 3813599', 'Es un correo que tiene el fin de informar las aclaraciones sobre el proceso', 5, 'Activo', 4, NULL),
(179, 'Aplicar ajustes al ITEM Empresas Estatales', 'El correo debe llegar todos los días a las 09:00 a.m., este contiene los ajustes a aplicar del día anterior, se debe asegurar que el BILLING del día anterior haya finalizado.', 1, 'Activo', 1, NULL),
(180, 'Generación email cartera UNE', 'Es un proceso que se hace para generar el email con la cartera de une', 5, 'Activo', 4, NULL),
(181, 'Validación envío de facturas electrónicas', 'El siguiente procedimiento para determinar los mail no exitosos del proceso de envió de facturas electrónicas. Este proceso determinara las posibles causas de error. De acuerdo a los errores identificados, haremos la respectiva gestión para la corrección y reenvió de estas facturas electrónicas.', 5, 'Activo', 4, NULL),
(182, 'GENERACIÓN DE REPORTE BILLYNG', 'Es un proceso que tiene la finalidad de hacer los reportes Billyng', 5, 'Activo', 3, NULL),
(183, 'GENERACIÓN DE REPORTE INVOICE', 'Es un proceso que tiene la finalidad de hacer los reportes Invoice\r\n\r\n', 5, 'Activo', 3, NULL),
(184, 'GENERACIÓN DE REPORTE PITNEY', 'Es un proceso que tiene la finalidad de hacer los reportes pitney', 5, 'Activo', 3, NULL),
(185, 'TIEMPOS CICLO PARA LLENAR EL REPORTE', 'Proceso el cual determina el tiempo para los reportes según el ciclo', 5, 'Activo', 4, NULL),
(186, 'REPORTE MENSUAL DIARIO PLAN PRODUCCIÓN', 'Es un proceso que tiene como finalidad dar a conocer el reporte mensual ', 5, 'Activo', 3, NULL),
(187, 'INCLUIR DISPUTA EN LA XSLT', 'Es un proceso manejado por el área de Pitney', 5, 'Activo', 4, NULL),
(188, 'COMO RETIRAR CARGAS DE VAULT CUANDO ESTAS PRESENTEN ERROR.', 'Es un proceso donde se retiran las cargas de vault cuando éstas presentan algún error', 5, 'Activo', 4, NULL),
(189, 'REPROCESOS ENVIO EMAIL', 'En este artículo se explica los reprocesos para el envío de email', 5, 'Activo', 4, NULL),
(190, 'GENERAR ARCHIVO INFO_INSCRITOS.csv', 'Es un proceso que se hace para generar los archivos info_inscritos', 5, 'Activo', 4, NULL),
(191, 'Depuración Servidores Pitney', 'Es un proceso que se hace en caso que se los servidores de pitney estén llenos', 5, 'Activo', 4, NULL),
(192, 'Verificacion JRN y AFP', '1.	Verificación duplicadas: Mediante el Notepad++ o el editor de su preferencia realizar la búsqueda por referencia de pago en el JRN de carga e impresión así:', 5, 'Activo', 4, NULL),
(193, 'ENTREGAS ADICIONALES O PDF CON RTB', 'Es un proceso que se hace para las entregas adicionales para el RTB', 5, 'Activo', 4, NULL),
(194, 'Reinicio Servicios envío email', 'QUE DEBO HACEN EN CASO  DE QUE LOS BUZONES DE LOS CORREOS ERROR.EFACTURA@TIGO.COM.CO , ERROR_EFACTURA2@TIGO.COM.CO, ERROR.EFACTURA3@TIGO.COM.CO, NO SE ESTEN DEPURANDO DE MANERA AUTOMATICA?', 5, 'Activo', 4, NULL),
(195, 'Carga Vault', 'Este proceso se hace con la finalidad de cargar el vault', 5, 'Activo', 4, NULL),
(196, 'Cubo Roaming', 'Herramienta de información dinámica del tráfico de Roaming utilizada para realizar las conciliaciones técnicas y financieras por el operador y seguimiento y validación de comportamiento de tráfico de Roaming.', 5, 'Activo', 4, NULL),
(197, 'Cubo ICT', 'Herramienta de información dinámica del tráfico de Interconexión utilizada para realizar las conciliaciones técnicas y financieras por operador y seguimiento y validación de comportamiento de tráfico de Interconexión.\r\n\r\n', 5, 'Activo', 4, NULL),
(198, 'PROCEDIMIENTO PARA LA EJECUCIÓN DE ACTUALIZACIÓN DEL CUBO INTERCONEXIÓN.', 'Herramienta de información dinámica del tráfico de Interconexión utilizada para realizar las conciliaciones técnicas y financieras por operador y seguimiento y validación de comportamiento de tráfico de Interconexión.', 5, 'Activo', 4, NULL),
(199, 'Cargue T CONTROL FAC', 'La función principal es conocer el proceso de carga para el cronograma de facturación que se carga mensual por el operador pospago.', 5, 'Activo', 3, NULL),
(200, 'EMAIL: Envio WEB', 'Extraer los xslt y generar las facturas para envio electronico a clientes declarantes del año 2015.', 5, 'Activo', 4, NULL),
(201, 'Gestión Anomalías (Diario)', 'Contempla la forma en que se debe ejecutar el proceso para la generación de información diaria de eventos en anomalías', 3, 'Activo', 1, NULL),
(202, 'Valida Saldos', 'Identificación de cuentas hijas y padres, con saldo y estados, para los procesos de Suspensión y reconexión, en gestión diaria de acuerdo al crónograma de facturación.\r\n\r\n', 1, 'Activo', 1, NULL),
(203, 'Valida Bancos', 'Proceso que toma los archivos con formato asobancaria o PM para ser validados (cuenta y valor), crear y aplicar archivo en formato IEL.', 1, 'Activo', 1, NULL),
(204, 'Foto Reporte', 'Garantizar su ejecución y que la foto con los pagos, moras de usuarios con pospago activos se toma exitosamente.', 5, 'Activo', 2, NULL),
(205, 'Saldo Item rep cartera', 'Generación de reporte con saldos y alturas de mora de las cuentas con cartera activa', 1, 'Activo', 2, NULL),
(206, 'Distribución de Pagos y ajustes', 'Proceso que prepara la distribución que hicieron los ajustes y pagos sobre los items de cobro en la tabla MIC.CTA_CA_DIST_PYA de la cual se apoyan los reportes para entregar lo recaudado y ajustado durante el mes.', 1, 'Activo', 2, NULL),
(207, 'Conciliación ASO', 'Realizar la conciliación de lo procesado por CRM y los recaudos WebService.\r\nRECURSOS', 3, 'Activo', 2, NULL),
(208, 'RTBILLING Diarío', 'Procesar las Llamadas realizadas por los usuarios Control y ATP en el aplicativo Real Time Billing ubicado en el servidor de PREPAGO y para los días de ciclo realizar el procesamiento del Rollover.\r\n\r\n', 1, 'Activo', 1, NULL),
(209, 'Periodos Perdidos', 'Es un proceso que se debe ejecutar en cada ciclo entre el Billing e invoice del proceso de facturación. Este proceso es con el fin de identificar las posibles cuentas que están apuntando a un ciclo determinado de facturación pero que por cualquier error, no facturaron y así están dejando un valor en Billing progress pendiente por facturar.', 3, 'Activo', 2, NULL),
(210, 'Generación de cifras de control después del Billing', 'Informa el número de cuentas que se van a procesar en el Billing de Ciclo.', 2, 'Activo', 2, NULL),
(211, 'Flag Invoice', 'Verificar que la actualización de las cuentas sumarizadas, se aplique de forma exitosa.\r\n', 2, 'Activo', 2, NULL),
(212, 'Preajustes y ajustes Billing de ciclo', 'Generar los archivos con la información necesaria para aplicar los ajustes que se aplicarán a las cuentas de los usuarios en su facturación\r\n\r\n', 1, 'Activo', 1, NULL),
(213, 'Generación de Cifras de control antes de Invoice', 'Generar la información de las cuentas que serán procesadas por el Invoice.', 2, 'Activo', 2, NULL),
(214, 'Ajustar ambiente para Invoice de ciclo', 'Preparar la maquina para la Ejecución del proceso de Invoice para el Ciclo en Ejecución.', 1, 'Activo', 1, NULL),
(215, 'Generación de cifras de control después de Invoice', 'Generar los archivos con la información de las facturas ya formateadas.', 1, 'Activo', 2, NULL),
(216, 'Validar Invoice duplicados y marcar Invoice duplicados', 'Verificar en las tablas de facturacion si hay invoice duplicados y marcarlos para evitar su procesamiento', 2, 'Activo', 2, NULL),
(217, 'Numero de cuentas antes de la numeración', 'Verificar la cantidad de cuentas que se van a numerar para el Ciclo.\r\n\r\n', 2, 'Activo', 2, NULL),
(218, 'Ejecutar Numeración', 'Generar el Consecutivo numérico a la facturación el cual la diferencia de las demás.\r\n\r\n', 2, 'Activo', 2, NULL),
(219, 'Cargos y Montos', 'Generar el reporte con el detalle de los Cargos y Montos de la facturación procesada.\r\n', 2, 'Activo', 2, NULL),
(220, 'Reporte impuesto facturado', 'Generar el reporte con los impuestos aplicados en la facturación del Ciclo.\r\n\r\n', 1, 'Activo', 3, NULL),
(221, 'Archivos XSLT', 'Las xslt corresponden a todas  las facturas  del ciclo en curso exportadas  del servidor BRM(10.69.32.227) al servidor de PITNEY(10.69.34.116 o 10.69.34.118), la misma cantidad de xslt que están en el servidor de BRM son las misma que deben haberse transferido al servidor de PITNEY.\r\n\r\n', 5, 'Activo', 4, NULL),
(222, 'Archivos Exclusiones', 'Documento o manual que pertenece al área de PITNEY', 5, 'Activo', 4, NULL),
(223, 'Archivos RTBILLING', 'Documento que pertenece al área de pitney ', 5, 'Activo', 4, NULL),
(224, 'BASE_AUTO2DETA_RECLAMACIONES.TXT', 'Documento respecto a éste archivo, este archivo contiene las cuentas de las facturas asociadas a alguna reclamación realizada por el usuario', 5, 'Activo', 4, NULL),
(225, 'BASE_CARTAS.TXT', 'Este archivo contiene las cuentas de las facturas que deben llevar asociada una carta informativa\r\n', 5, 'Activo', 4, NULL),
(226, 'INFO_CARTAS.TXT', 'Este archivo contiene las cuentas de las facturas que deben llevar asociada una carta informativa, adicional contiene los datos que debe llevar la carta, ejemplo; nombre, móvil, tipo de carta, plan. etc', 5, 'Activo', 4, NULL),
(227, 'DETALLECLAUSULAS_MMDDAAAA.TXT', 'Este archivo contiene la información de los valores a cobra a los clientes por compra de equipos a cuotas.', 1, 'Activo', 4, NULL),
(228, 'Info Inscritos', 'Este archivo contiene las cuentas de las facturas  junto con el correo electrónico, su función es determinar por qué medio (físico y/o electrónico) se va a entregar la factura al cliente.', 5, 'Activo', 4, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_has_ciclo`
--

CREATE TABLE `proceso_has_ciclo` (
  `id_detalle` int(11) NOT NULL,
  `proceso_idProceso` int(11) NOT NULL,
  `ciclo_idciclos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_proceso`
--

CREATE TABLE `tipo_proceso` (
  `idtipo_proceso` int(11) NOT NULL,
  `nombre_tipo_proceso` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_proceso`
--

INSERT INTO `tipo_proceso` (`idtipo_proceso`, `nombre_tipo_proceso`) VALUES
(1, 'Diario'),
(2, 'Semanal'),
(3, 'Mensual'),
(4, 'Pitney');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `idturno` int(11) NOT NULL,
  `nombre_turno` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`idturno`, `nombre_turno`) VALUES
(1, 'Mañana'),
(2, 'Tarde'),
(3, 'Noche'),
(4, 'Ambos Horarios'),
(5, 'No requiere Horario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre_usuario` varchar(45) DEFAULT NULL,
  `apellido_usuario` varchar(45) DEFAULT NULL,
  `correo_usuario` varchar(60) DEFAULT NULL,
  `pass_usuario` varchar(45) DEFAULT NULL,
  `cod_seguridad` varchar(10) DEFAULT NULL,
  `estado_usuario` varchar(45) DEFAULT 'Activo',
  `tutorial_usuario` varchar(45) DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre_usuario`, `apellido_usuario`, `correo_usuario`, `pass_usuario`, `cod_seguridad`, `estado_usuario`, `tutorial_usuario`) VALUES
(1, 'Esteban', 'Calle', 'estebancalle731@gmail.com', 'MTIzNDU=', '#KGK&VfvBu', 'Activo', 'Activo'),
(4, 'Gustavo', 'Orozco', 'gustavo.orozco@menestys-services.com', NULL, 'JZd4jVZYVt', 'Activo', 'Activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idarticulo`),
  ADD KEY `fk_articulo_proceso1_idx` (`proceso_idProceso`),
  ADD KEY `fk_articulo_imagen1_idx` (`imagen_idimagen`);

--
-- Indices de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  ADD PRIMARY KEY (`idciclos`);

--
-- Indices de la tabla `escalamiento`
--
ALTER TABLE `escalamiento`
  ADD PRIMARY KEY (`cod_escalamiento`),
  ADD KEY `fk_escalamiento_proceso1_idx` (`proceso_idProceso`);

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`idimagen`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`idProceso`),
  ADD KEY `fk_proceso_tipo_proceso_idx` (`tipo_proceso_idtipo_proceso`),
  ADD KEY `fk_proceso_turno1_idx` (`turno_idturno`),
  ADD KEY `fk_proceso_usuario1_idx` (`usuario_idusuario`);

--
-- Indices de la tabla `proceso_has_ciclo`
--
ALTER TABLE `proceso_has_ciclo`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `fk_proceso_has_ciclo_proceso1_idx` (`proceso_idProceso`),
  ADD KEY `fk_proceso_has_ciclo_ciclo1_idx` (`ciclo_idciclos`);

--
-- Indices de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD PRIMARY KEY (`idtipo_proceso`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`idturno`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idarticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=545;

--
-- AUTO_INCREMENT de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  MODIFY `idciclos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escalamiento`
--
ALTER TABLE `escalamiento`
  MODIFY `cod_escalamiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
  MODIFY `idimagen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=602;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `idProceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT de la tabla `proceso_has_ciclo`
--
ALTER TABLE `proceso_has_ciclo`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  MODIFY `idtipo_proceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `idturno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `fk_articulo_imagen1` FOREIGN KEY (`imagen_idimagen`) REFERENCES `imagen` (`idimagen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_articulo_proceso1` FOREIGN KEY (`proceso_idProceso`) REFERENCES `proceso` (`idProceso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `escalamiento`
--
ALTER TABLE `escalamiento`
  ADD CONSTRAINT `fk_escalamiento_proceso1` FOREIGN KEY (`proceso_idProceso`) REFERENCES `proceso` (`idProceso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `fk_proceso_tipo_proceso` FOREIGN KEY (`tipo_proceso_idtipo_proceso`) REFERENCES `tipo_proceso` (`idtipo_proceso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proceso_turno1` FOREIGN KEY (`turno_idturno`) REFERENCES `turno` (`idturno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proceso_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proceso_has_ciclo`
--
ALTER TABLE `proceso_has_ciclo`
  ADD CONSTRAINT `fk_proceso_has_ciclo_ciclo1` FOREIGN KEY (`ciclo_idciclos`) REFERENCES `ciclo` (`idciclos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proceso_has_ciclo_proceso1` FOREIGN KEY (`proceso_idProceso`) REFERENCES `proceso` (`idProceso`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
