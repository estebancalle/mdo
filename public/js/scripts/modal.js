function modal(){
        var dialogButton = document.querySelector('.dialog-button');
        var dialog = document.querySelector('#dialog');
        if (! dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }
        dialog.showModal();
        // $("body").css("opacity",0.2);
        dialog.querySelector('button:not([disabled])')
        .addEventListener('click', function() {
            dialog.close();
            // $("body").css("opacity",1);
        });
}

// AVANZA AL TUTORIAL 2
$("#btntuto1").click(function(){
    $("#tuto1").hide();
    $("#tuto2").show();
})

// REGRESA AL TUTORIAL 1
$("#btntuto2-prev").click(function(){
    $("#tuto2").hide();
    $("#tuto1").show();
})

// REGRESA AL TUTORIAL 2
$("#btntuto3-prev").click(function(){
    $("#tuto3").hide();
    $("#tuto2").show();
})

// AVANZA AL TUTORIAL 3
$("#btntuto2-rev").click(function(){
    $("#tuto2").hide();
    $("#tuto3").show();
})

// REGRESA AL TUTORIAL 3
$("#btntuto4-prev").click(function(){
    $("#tuto4").hide();
    $("#tuto3").show();
})

// AVANZA AL TUTORIAL 4
$("#btntuto3-rev").click(function(){
    $("#tuto3").hide();
    $("#tuto4").show();
})


function cerrarModal(){
    $("#dialog").hide();
}




