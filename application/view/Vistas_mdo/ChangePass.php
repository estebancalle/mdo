<?php if(!$_SESSION['id_usu']){header("location:".URL."Home");}?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MDO</title>
	<!-- Material css  -->
  	<link rel="short icon" href="<?=URL?>img/manual.png">
	<link rel="stylesheet" href="<?=URL?>css/material.min.css">
	<link rel="stylesheet" href="<?=URL?>css/Nativos.css">
	<link rel="stylesheet" href="<?=URL?>css/login.css">
	<link rel="stylesheet" href="<?=URL?>css/pace.css">
	<!-- Liberias -->
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
</head>
<body>
	<!-- Inicio formulario -->
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--4-col"></div>
		<div class="mdl-cell mdl-cell--4-col">
			<center>
				<div class="mdl-card mdl-shadow--16dp" id="form">
				<div class="mdl-card__supporting-text">
					<center><h3>Cambiar Contraseña</h3></center>
					<hr>
					<form action="<?=URL?>Home/cambiarPass" method="POST">
						<input type="hidden" name="cod_usuario" value="<?=$_SESSION["id_usu"]?>">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    					<input class="mdl-textfield__input" type="password" id="correo" name="txtpass">
    					<label class="mdl-textfield__label" for="correo">Nueva Contraseña</label>
    					<div class="mensaje1"></div>
  					</div>
  					<br>
  					<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="logearse">
  						Cambiar
					</button>
					<br><br>
					<a href="<?=URL?>Home/iniciar">Volver</a>
					</form>
				</div>
			</div>
			</center>
		</div>
		<div class="mdl-cell mdl-cell--4-col"></div>
	</div>
	<!-- Jquery -->
	<script src="<?=URL?>/js/jquery.js"></script>
	<script src="<?=URL?>/js/pace.js"></script>
	<!-- Material js -->
	<script src="<?=URL?>/js/material.js"></script>
	<!-- Librerias -->
  	<script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
<?php
    if(isset($_SESSION['mensaje'])){
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje']=null;
  }
?>
</body>
</html>