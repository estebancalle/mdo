<?php 
if (!isset($_SESSION["id_usu"])) {
  header("location: " . URL . "home");
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Servidores</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>css/modal.css">
  <link rel="stylesheet" href="<?=URL?>css/floating-button.css">
  <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body onload="actualizar()">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Servidores</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
      <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
        $encriptacion = base64_encode($_SESSION["id_usu"])
      ?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <br>
      <div class="mdl-grid">
      	<div class="mdl-cell mdl-cell--6-col">
      		<center><h3>Servidores</h3></center>
      		 <center><p>Hola, <?=$_SESSION["alias"]?>, ésta es la sección de servidores, cada 20 segundos la página se va actualizar para dar mayor seguridad y que todo esté al día</p></center>
      	</div>
      	<div class="mdl-cell mdl-cell--6-col">
      		<center><h3>Usuarios en Línea (<?=$UsuariosOnline?>)</h3></center>
      		<ul class="demo-list-icon mdl-list" id="ListaUsuarios">
  				
			</ul>
      	</div>
      </div>
      <hr>
      <div class="mdl-grid" id="grid">
          <?php foreach($servidores as $value): ?>
            <div class="mdl-cell mdl-cell--4-col">
              <center>
              <div class="mdl-card mdl-shadow--2dp">
                <input type="hidden" id="cod_servidor" value="<?=$value->cod_servidor?>">
                  <!-- Se valida el estado del servidor para mostrar la cabecera de la tarjeta de unc color determinado -->
                  <?php if($value->estado_servidor=="Ocupado"){ ?>
                    <div class="mdl-card__title" style="height:150px; background:url('<?=URL?>img/mini-server.png')bottom right 15% no-repeat #D73C2C" id="title-<?=$value->cod_servidor?>">
                  <?php }else if($value->estado_servidor=="Mantenimiento"){ ?>
                      <div class="mdl-card__title" style="height:150px; background:url('<?=URL?>img/mini-server.png')bottom right 15% no-repeat #F1892D" id="title-<?=$value->cod_servidor?>">        
                  <?php }else{?>
                    <div class="mdl-card__title" style="height:150px; background:url('<?=URL?>img/mini-server.png')bottom right 15% no-repeat #0EAC51" id="title-<?=$value->cod_servidor?>">  
                  <?php } ?>
                  <!-- Fin validación -->
                  <h2 class="mdl-card__title-text" style="color:white"><?=$value->nombre_servidor?></h2>
                </div>
                <div class="mdl-card__supporting-text">
                  <center><h6>Información actual sobre el servidor</h6></center>    
                  <div class="mdl-grid" id="grid_principal-<?=$value->cod_servidor?>">
                    <?php if($value->usuario_idusuario==1){ ?>
                      <div class="mdl-cell mdl-cell--6-col">
                        <center><P>No hay usuario</P></center>
                      </div>
                    <?php }else{ ?>
                      <div class="mdl-cell mdl-cell--6-col">
                        <center>
                        <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
                        <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
                          <span class="mdl-chip__text"><?=$value->nombre_usuario?></span>
                        </span>
                        </center>
                      </div>
                    <?php } ?>
                    <!-- Se vuelve a validar el estado para mostrar el mismo en un bread -->
                    <?php if($value->estado_servidor=="Activo"){ ?>
                      <div class="mdl-cell mdl-cell--6-col">
                        <span class="mdl-chip mdl-chip--contact">
                          <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">D</span>
                          <span class="mdl-chip__text">Disponible</span>
                      </span>
                      </div>
                    <?php }else if($value->estado_servidor=="Mantenimiento"){ ?>
                      <div class="mdl-cell mdl-cell--6-col">
                        <span class="mdl-chip mdl-chip--contact">
                          <span class="mdl-chip__contact mdl-color--orange mdl-color-text--white">M</span>
                          <span class="mdl-chip__text">Mantenimiento</span>
                        </span>
                      </div>
                    <?php }else{ ?>
                      <div class="mdl-cell mdl-cell--6-col">
                        <span class="mdl-chip mdl-chip--contact">
                          <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">O</span>
                          <span class="mdl-chip__text">Ocupado</span>
                        </span>
                      </div>
                    <?php } ?>
                    <!-- Fin validación -->
                  </div>
                  <hr>      
                  <center>
                    <!-- Se valida la existencia de una nota en el servidor -->
                    <?php if($value->nota_servidor==""){ ?>
                        <p>No hay notas respecto al servidor</p>
                    <?php } else {?>
                        <p><?=$value->nota_servidor?></p>
                    <?php } ?>
                    <!-- Fin validación -->
                  </center>
                  <hr>
                  <!-- Se valida que el usuario esté ocupando o no el servidor -->
                  <?php if($value->usuario_idusuario==$_SESSION["id_usu"]){ ?>
                      <center><p>USTED CREÓ O ESTÁ OCUPANDO ÉSTE SERVIDOR</p></center>
                  <?php }else{?>

                  <?php }?>
                  <!-- Fin validación -->
                </div>
                <div class="mdl-card__menu">
                  <?php if($value->estado_servidor=="Ocupado"){ ?>
                      <?php if($value->usuario_idusuario==$_SESSION["id_usu"]){ ?>
                        <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" id="bloqueado-<?=$value->cod_servidor?>" onclick="desocuparServidor(<?=$value->cod_servidor?>)">
                            <i class="material-icons">forward</i>
                        </button>
                        <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="bloqueado-<?=$value->cod_servidor?>">
                          Desocupar Servidor
                        </div>
                      <?php }else{ ?>
                        <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" id="bloqueado-<?=$value->cod_servidor?>">
                          <i class="material-icons">block</i>
                        </button>
                        <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="bloqueado-<?=$value->cod_servidor?>">
                          Servidor ocupado
                        </div>
                      <?php } ?>
                  <?php }else if($value->estado_servidor=="Mantenimiento"){ ?>
                    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" onclick="MantenimientoOf(<?=$value->cod_servidor?>)" id="EndMant-<?=$value->cod_servidor?>">
                      <i class="material-icons">cancel_presentation</i>
                    </button>
                    <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="EndMant-<?=$value->cod_servidor?>">
                        Desmarcar como en mantenimiento
                    </div>
                  <?php }else{ ?>
                     <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" onclick="ocuparServidor(<?=$value->cod_servidor?>)" id="Disponible-<?=$value->cod_servidor?>">
                      <i class="material-icons">forward</i>
                    </button>
                    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" onclick="MantenimientoOn(<?=$value->cod_servidor?>)" id="outService-<?=$value->cod_servidor?>">
                      <i class="material-icons">warning</i>
                    </button>
                    <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="Disponible-<?=$value->cod_servidor?>">
                        Ocupar Servidor
                    </div>
                    <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="outService-<?=$value->cod_servidor?>">
                        Marcar como en mantenimiento
                    </div> 
                  <?php } ?>
                </div>
              </div>
              </center>
            </div>
          <?php endforeach; ?>
      </div>
    </div>
  </main>
  <?php if($_SESSION["rol"]==1){ ?>
  <div class="floating-button">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--16dp" onclick="modalServer()" id="boton-flotante">
          <i class="material-icons">add</i>
        </button>
    <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="boton-flotante">
      Nuevo Servidor
    </div>
  </div>
  <?php }else {?>

  <?php  }?>
</div>
<dialog id="dialog" class="mdl-dialog">
  <h3 class="mdl-dialog__title">Nuevo Servidor</h3>
  <div class="mdl-dialog__content">
    <p>los campos con * son obligatorios</p>
      <form action="">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%">
          <input class="mdl-textfield__input" type="text" id="txtservidor">
          <label class="mdl-textfield__label" for="txtservidor">Nombre *</label>
        </div>
        <div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label" style="width:100%">
          <textarea class="mdl-textfield__input" type="text" rows= "1" id="txtnota" ></textarea>
          <label class="mdl-textfield__label" for="txtnota">Descripción</label>
        </div>
        <center><a href="#" onclick="registrarServidor()"><i class="material-icons">check</i></a></center>
      </form>
  </div>
  <div class="mdl-dialog__actions">
    <button type="button" class="mdl-button" id="cerrarModal()">Cancelar</button>
  </div>
</dialog>
  <!-- Jquery -->
 <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/scripts/modal.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <!-- Dialog -->
  <script src="<?=URL?>js/dialog/dialog-polyfill.js"></script>
  <!-- Toastr -->
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script>
    function modalServer(){
        var dialogButton = document.querySelector('.dialog-button');
        var dialog = document.querySelector('#dialog');
        if (! dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }
        dialog.showModal();
        dialog.querySelector('button:not([disabled])')
        .addEventListener('click', function() {
            dialog.close();
        });
}

  function actualizar() {
      setInterval(function(){window.location.reload(true); }, 20000);
      toastr.success("Todo está actualizado");
      consultarUsuariosOnline();
  }

  // function actualizarUsuariosOnline(){
  // 	setInterval(function(){
  //     consultarUsuariosOnline();
  // 	},5000)
  // }

    function registrarServidor(){
      var url="<?php echo URL ?>";
      let tservidor=$("#txtservidor").val();
      let tnota=$("#txtnota").val();
      if (confirm("Desea registrar el servidor?")) {
        $.ajax({
          method:"POST",
          url:url+"Home/registrarServidor",
          datatype:'json',
          data:{servidor : tservidor , nota : tnota}
        })
        .done(function(rpta){
          if (rpta!=null) {
            toastr.success("Servidor Registrado... Actualizando");
            setInterval(function(){window.location.reload(true); }, 2000);
          }else{
            toastr.error("Error en la operación");
          }
        })
        .fail(function(error){
          console.log(error);
        })
      }else{

      }
    }

    function ocuparServidor(servi){
      var cod_servidor=servi;
      var url="<?php echo URL ?>";
      if (confirm("¿Desea ocupar éste servidor?")) {
        $.ajax({
          method:"POST",
          url:url+"Home/ocuparServidor",
          datatype:'json',
          data:{servidor : cod_servidor}
        })
        .done(function(rpta){
          if (rpta!=null) {
            toastr.success("Se ha ocupado el servidor, actualizando...");
            setInterval(function(){window.location.reload(true); }, 2000);
            // consultarServidor();
          }else{
            toastr.succes("No se pudo completar la operación");
            // consultarServidor();
          }
        })
        .fail(function(error){
          console.log(error);
        })
      }else{

      }
    }

    function desocuparServidor(servidorrr){
      var tservidorr=servidorrr;
      var url="<?php echo URL ?>";
      if (confirm("¿Desea desocupar éste servidor?")) {
          $.ajax({
            method:"POST",
            url:url+"Home/desocuparServidor",
            datatype:'json',
            data:{servidor : tservidorr}
          })
          .done(function(rpta){
            if (rpta!=null) {
            toastr.success("Se ha desocupado el servidor, actualizando...");
            setInterval(function(){window.location.reload(true); }, 2000);
            // consultarServidor();
          }else{
            toastr.succes("No se pudo completar la operación");
            // consultarServidor();
          }
          })
          .fail(function(error){
            console.log(error);
          })
      }else{

      }
    }

    function MantenimientoOn(server){
      var s=server;
      var url="<?php echo URL ?>";
      if (confirm("¿Marcar éste servidor con un estado de mantenimiento?")) {
          $.ajax({
            method:"POST",
            url:url+"Home/MantenimientoOn",
            datatype:'json',
            data:{servidor : s}
          })
          .done(function(rpta){
            if (rpta!=null) {
            toastr.success("Se ha completado el proceso");
            setInterval(function(){window.location.reload(true); }, 2000);
            // consultarServidor();
          }else{
            toastr.succes("No se pudo completar la operación");
            // consultarServidor();
          }
          })
          .fail(function(error){
            console.log(error);
          })
      }else{

      }
    }

     function MantenimientoOf(ss){
      var srvr=ss;
      var url="<?php echo URL ?>";
      if (confirm("¿Marcar finalización del mantenimiento?")) {
          $.ajax({
            method:"POST",
            url:url+"Home/MantenimientoOf",
            datatype:'json',
            data:{servidor : srvr}
          })
          .done(function(rpta){
            if (rpta!=null) {
            toastr.success("Se ha coompletado el proceso");
            setInterval(function(){window.location.reload(true); }, 2000);
            // consultarServidor();
          }else{
            toastr.succes("No se pudo completar la operación");
            // consultarServidor();
          }
          })
          .fail(function(error){
            console.log(error);
          })
      }else{

      }
    }

    function consultarUsuariosOnline(){
      var url="<?php echo URL ?>";
      // var usuarios=[];
      $.ajax({
      	method:"POST",
      	url:url+"Home/consultarUsuariosOnline",
      	datatype:'json'
      })
      .done(function(resultado){
      	var datos=$.parseJSON(resultado);
      	for(var contador in datos){
      		var html='<li class="mdl-list__item">';
      		html+='<span class="mdl-list__item-primary-content">';
      		html+='<i class="material-icons mdl-list__item-icon">person</i>'+datos[contador].nombre_usuario+' '+datos[contador].apellido_usuario+'';
      		html+='<span></li>';
      		$("#ListaUsuarios").append(html);
      		// usuarios.push(datos[contador].idusuario);
      		// for (var i = usuarios.length - 1; i >= 0; i--) {
      		// 	console.log(usuarios[i]);
      		// 	// if (usuarios.includes(usuarios[i].idusuario)) {

      		// 	// }else{
      		// 	// 	$("#ListaUsuarios").append(html);
      		// 	// 	usuarios.push(datos[contador].idusuario);
      		// 	// }
      		// }
      	}
      })
    }
  </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>