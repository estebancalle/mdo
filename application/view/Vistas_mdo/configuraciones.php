<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Configuraciones</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body onload="generarCodigo()">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Configuración de la cuenta</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
      <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid">
          <div class="mdl-cell mdl-cell--6-col">
            <div class="mdl-card mdl-shadow--8dp" style="width:100%;">
              <div class="mdl-card__title"><h3>Editar Información</h3></div>
              <div class="mdl-card__supporting-text">
              <form action="<?=URL?>Home/actualizarInfoUsuario" method="POST">
                <input type="hidden" name="txtpersona" value="<?=$resultado->idusuario?>">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                  <input class="mdl-textfield__input" type="text" id="sample3" value="<?=$resultado->nombre_usuario?>" name="txtnombre">
                  <label class="mdl-textfield__label" for="sample3">Nombre</label>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                  <input class="mdl-textfield__input" type="text" id="sample3" value="<?=$resultado->apellido_usuario?>" name="txtapellido">
                  <label class="mdl-textfield__label" for="sample3">Apellido</label>
                </div>
                <br>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                  <input class="mdl-textfield__input" type="email" id="sample3" value="<?=$resultado->correo_usuario?>" name="txtcorreo">
                  <label class="mdl-textfield__label" for="sample3">Correo</label>
                </div>
                <br>
                <center><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">Guardar</button></center>
              </form>
            </div>
            </div>
          </div>

          <div class="mdl-cell mdl-cell--6-col">
            <div class="mdl-card mdl-shadow--8dp" style="width:100%;">
              <div class="mdl-card__title"><h3>Cambiar Contraseña</h3></div>
              <div class="mdl-card__supporting-text">
              <form action="<?=URL?>Home/cambiarPass" method="POST">
                <input type="hidden" name="cod_usuario" value="<?=$resultado->idusuario?>">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                  <input class="mdl-textfield__input" type="password" id="sample3" name="txtpass">
                  <label class="mdl-textfield__label" for="sample3">Contraseña</label>
                </div>
                <input type="hidden" name="txtcod" id="txtcod">
                <br>
                <center><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">Guardar</button></center>
              </form>
            </div>
            </div>
          </div>
      </div>
    </div>
  </main>
</div>
  <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
    <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script>
    function generarCodigo(){
      var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789!@$%/#?&";
          var contraseña = "";
          for (i=0; i<10; i++) contraseña +=caracteres.charAt(Math.floor(Math.random()*caracteres.length));
            $("#txtcod").val(contraseña);
      }
  </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>
