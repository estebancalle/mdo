<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LOGIN MDO V2</title>
	<!-- Material css  -->
  	<link rel="short icon" href="<?=URL?>img/LOGO-MDO.png">
	<link rel="stylesheet" href="<?=URL?>css/material.min.css">
	<link rel="stylesheet" href="<?=URL?>css/Nativos.css">
	<link rel="stylesheet" href="<?=URL?>css/login.css">
	<!-- Librerias -->
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<body>
	<!-- GRID PRINCIPAL	 -->
			<div class="mdl-grid">
				<div class="mdl-card mdl-shadow--16dp" id="cartaPrincipal">
					<div class="mdl-card__supporting-text">
						<div class="mdl-grid">
							<div class="mdl-cell mdl-cell--6-col" id="columnaIzquierda"></div>
							<div class="mdl-cell mdl-cell--6-col" id="columnaDerecha">
								<center><h4>Iniciar Sesión</h4></center>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- GRID PRINCIPAL -->
	<!-- Jquery -->
	<script src="<?=URL?>/js/jquery.js"></script>
	<!-- Material js -->
	<script src="<?=URL?>/js/material.js"></script>
	<!-- Librerias -->
  	<script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
</body>
</html>