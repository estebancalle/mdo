<?php
ob_start();
?>
<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Nuevo Proceso</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/floating-button.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body>
<!-- Uses a header that contracts as the page scrolls down. -->
<style>
.demo-layout-waterfall .mdl-layout__header-row .mdl-navigation__link:last-of-type  {
  padding-right: 0;
}
</style>
<form action="<?=URL?>Home/registrarProcess" method="POST" enctype="multipart/form-data">
<div class="demo-layout-waterfall mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--waterfall">
    <!-- Top row, always visible -->
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Nuevo Proceso</span>
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
     <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons" style="color:blue;">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
     <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid" id="rejilla">
        <div class="mdl-cell mdl-cell--6-col">
          <div class="mdl-card mdl-shadow--4dp" style="width:100%">
            <div class="mdl-card__title" style="background:url('<?=URL?>img/pattern.jpg')center/cover; height:100px;"></div>
            <div class="mdl-card__supporting-text">
              <center><h4 style="color:black">Nombre y Descripción Inicial del proceso</h4></center>
              <center>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input required class="mdl-textfield__input" type="text" id="txtnombreProceso" name="txtnombreProceso">
                      <label class="mdl-textfield__label" for="txtnombreProceso">Nombre</label>
                    </div>
                    <br>
                    <div class="mdl-textfield mdl-js-textfield" style="width:100%;">
                      <textarea class="mdl-textfield__input" type="text" rows= "1" id="txtdescripcionProceso" name="txtdescripcionProceso"></textarea>
                      <label class="mdl-textfield__label" for="sample5">Descripción</label>
                  </div>
                  <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col">
                      <label>Tipo de Proceso</label>
                      <select required name="tipoProceso" id="tipo">
                       <?php foreach ($tipo as $value): ?>
                          <option value="<?=$value->idtipo_proceso?>"><?=$value->nombre_tipo_proceso?></option>
                       <?php endforeach;?>
                      </select>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                      <label>Turno</label>
                      <select required name="turnoProceso" id="turno">
                        <?php foreach ($turnos as $value): ?>
                          <option value="<?=$value->idturno?>"><?=$value->nombre_turno?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                   <center><p>Para añadir un artículo de clic en el botón  de nuevo artículo ublicado en el menú de arriba con un ícono de más, cuando finalice el registro de clic en el botón de enviar</p></center>
              </center>
            </div>
          </div>
        </div>
        <div class="mdl-cell mdl-cell--6-col">
          <div class="mdl-card mdl-shadow--4dp" style="width:100%;">
            <div class="mdl-card__title" style="height:100px; background:url('<?=URL?>img/pattern.jpg')center/cover"></div>
            <div class="mdl-card__supporting-text" id="estructura">
                <center><h4 style="color:black">Estructura del proceso</h4></center>
                <center><p>El proceso se va guardar de forma que quede fácil de entender y fácil de mantener por parte de los usuarios</p></center>
                <!-- Aquí va la edp -->
                <h4 id="tituloProceso" style="color:black"></h4>
                <div class="mdl-grid">
                  <div class="mdl-cell mdl-cell--6-col"><p id="edptipo"></p></div>
                  <div class="mdl-cell mdl-cell--6-col"><p id="edpturno"></p></div>
                </div>
                <hr>
                <p id="descripcionProceso"></p>
                <hr>
               <!--  <div class="mdl-grid">
                  <div class="mdl-cell mdl-cell--6-col"><h5>Cantidad de Artículos</h5></div>
                  <div class="mdl-cell mdl-cell--6-col"><p id="edptotal"></p></div>
                </div> -->
            </div>
          </div>
        </div>
        <!-- Artóculos generados -->
      <!-- Fin artículos generados -->
      </div>
      <center>
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
          Guardar proceso
        </button>
        <br><br>
      </center>
    </div>
  </main>
  <div class="floating-button">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--16dp" onclick="addsection()" id="boton-flotante" type="button">
          <i class="material-icons">add</i>
        </button>
    <div class="mdl-tooltip mdl-tooltip--large mdl-tooltip--left" for="boton-flotante">
      Nuevo Artículo
    </div>
  </div>
</div>
  <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script>
    var contador=0;
    function addsection(){
      var contadorArticulos;
      var contadorImagenes;
      contadorImagenes+=1;
      contadorArticulos+=1;
      var contenido="<center>";
      contenido="<div class='mdl-card mdl-shadow--6dp' style='margin:10px;'>";
      contenido+=" <div class='mdl-card__title' style='height:100px; background:url("+'<?=URL?>'+"img/pattern.jpg')center/cover'></div>";
      contenido+="<div class='mdl-card__supporting-text' id='estructura'>";
      contenido+="<center><h4 style='color:black'>Nuevo Artículo</h4></center>";
      contenido+="<center>";
      contenido+="<label>Nombre</label><br><br>";
      contenido+="<input required type='text' name='txtnombreArticulo[]' id='txtnombreArticulo'><br><br>";
      contenido+="<label>Descripción</label><br><br>";
      contenido+=" <textarea required style='width:100%' type='text' rows= '4' name='txtdescripcionArticulo[]'></textarea><br><br>";
      contenido+="<label>Imágen</label><br><br>";
      contenido+="<input type='file' name='imagen[]'>";
      contenido+="</center>";
      contenido+="<input type='hidden' name='contadorImg' value='"+contadorImagenes+"'>";
      contenido+="<input type='hidden' name='contadorArt' value='"+contadorArticulos+"'>";
      contenido+="</div>";
      contenido+=" <div class='mdl-card__menu'>";
      contenido+="<button class='mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect' style='color:white' onclick='deletesection(this)'>";
      contenido+="<i class='material-icons'>delete</i>";
      contenido+="</button>";
      contenido+="</div>";
      contenido+="</div>";
      contenido+="</center>";
      $("#rejilla").append(contenido);
      toastr.success('Se añadió un nuevo formulario');
      contador+=1;
      $("#edptotal").html(contador);
    }

    function deletesection(seccion){
       var e=$(seccion).parent().parent();
       contador-1;
        $(e).remove();
        $("#edptotal").html(contador);
    }

      // Funciones de edp(estructura de proceso)
      $("#txtnombreProceso").keyup(function(){
        $("#tituloProceso").html($("#txtnombreProceso").val());
      });

      $("#txtdescripcionProceso").keyup(function(){
        $("#descripcionProceso").html($("#txtdescripcionProceso").val());
      });

      $("#tipo").change(function(){
        $("#edptipo").html($("#tipo option:selected").html());
      });

      $("#turno").change(function(){
        $("#edpturno").html($("#turno option:selected").html());
      });



    $(document).ready(function() {
    $('#tipo').select2();
    $('#turno').select2();
    });
  </script>
</body>
</html>
</form>
<?php
ob_end_flush();
?>