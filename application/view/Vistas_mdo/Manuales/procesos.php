<?php
ob_start();
?>
<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Lista de Procesos</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
    <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
    <link rel="stylesheet" href="<?=URL?>css/pace.css">
    <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <!-- Google fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?=URL?>libs/Datatables/datatables.css">
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body>
<!-- Simple header with fixed tabs. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header mdl-layout--fixed-tabs">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
       <!-- Title -->
      <span class="mdl-layout-title">Lista de Procesos</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
       <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
    <!-- Tabs -->
    <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
      <a href="#fixed-tab-1" class="mdl-layout__tab is-active">Facturación</a>
      <a href="#fixed-tab-2" class="mdl-layout__tab">Pitney</a>
      <a href="#fixed-tab-3" class="mdl-layout__tab">Semanales</a>
      <a href="#fixed-tab-4" class="mdl-layout__tab">Mensuales</a>
      <a href="#fixed-tab-5" class="mdl-layout__tab">Diarios</a>
    </div>
  </header>
  <div class="mdl-layout__drawer">
     <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
   <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <section class="mdl-layout__tab-panel is-active" id="fixed-tab-1">
      <div class="page-content">
        <div class="mdl-grid">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%">
            <div class="mdl-card__supporting-text">
              <center><h4>Procesos de Facturación</h4></center>
              <br>
              <table id="procesos">
              <thead>
                <tr>
                  <th>Número</th>
                  <th>Proceso</th>
                  <th>Turno</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Ver</th>
                  <th>Cambiar Estado</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($procesos as $value): ?>
                <tr>
                  <td><?=$value->idProceso?></td>
                  <td><?=$value->nombre_proceso?></td>
                  <td><?=$value->nombre_turno?></td>
                  <td><?=$value->nombre_tipo_proceso?></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
                <td>
                  <span class="mdl-chip mdl-chip--contact">
                    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                    <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                <?php } else {?>
                  <td>
                    <span class="mdl-chip mdl-chip--contact">
                      <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">I</span>
                      <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                  <?php }?>
                  <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Inactivo"?>/<?=$value->idProceso?>">Inactivar</a></td>
                  <?php } else {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Activo"?>/<?=$value->idProceso?>">Activar</a></td>
                  <?php }?>
                </tr>
              <?php endforeach?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="fixed-tab-2">
      <div class="page-content">
        <div class="mdl-grid">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%">
            <div class="mdl-card__supporting-text">
              <center><h4>Procesos de Pitney</h4></center>
              <br>
              <table id="procesos_pitney">
              <thead>
                <tr>
                  <th>Número</th>
                  <th>Proceso</th>
                  <th>Turno</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Ver</th>
                  <th>Cambiar Estado</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($procesos_pitney as $value): ?>
                <tr>
                  <td><?=$value->idProceso?></td>
                  <td><?=$value->nombre_proceso?></td>
                  <td><?=$value->nombre_turno?></td>
                  <td><?=$value->nombre_tipo_proceso?></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
                <td>
                  <span class="mdl-chip mdl-chip--contact">
                    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                    <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                <?php } else {?>
                  <td>
                    <span class="mdl-chip mdl-chip--contact">
                      <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">I</span>
                      <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                  <?php }?>
                  <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Inactivo"?>/<?=$value->idProceso?>">Inactivar</a></td>
                  <?php } else {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Activo"?>/<?=$value->idProceso?>">Activar</a></td>
                  <?php }?>
                </tr>
              <?php endforeach?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="fixed-tab-3">
      <div class="page-content">
        <div class="mdl-grid">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%">
            <div class="mdl-card__supporting-text">
              <center><h4>Procesos Semanales</h4></center>
              <br>
              <table id="semanales">
              <thead>
                <tr>
                  <th>Número</th>
                  <th>Proceso</th>
                  <th>Turno</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Ver</th>
                  <th>Cambiar Estado</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($semanales as $value): ?>
                <tr>
                  <td><?=$value->idProceso?></td>
                  <td><?=$value->nombre_proceso?></td>
                  <td><?=$value->nombre_turno?></td>
                  <td><?=$value->nombre_tipo_proceso?></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
                <td>
                  <span class="mdl-chip mdl-chip--contact">
                    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                    <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                <?php } else {?>
                  <td>
                    <span class="mdl-chip mdl-chip--contact">
                      <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">I</span>
                      <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                  <?php }?>
                  <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Inactivo"?>/<?=$value->idProceso?>">Inactivar</a></td>
                  <?php } else {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Activo"?>/<?=$value->idProceso?>">Activar</a></td>
                  <?php }?>
                </tr>
              <?php endforeach?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     <section class="mdl-layout__tab-panel" id="fixed-tab-4">
      <div class="page-content">
        <div class="mdl-grid">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%">
            <div class="mdl-card__supporting-text">
              <center><h4>Procesos de Mensuales</h4></center>
              <br>
              <table id="mensuales">
              <thead>
                <tr>
                  <th>Número</th>
                  <th>Proceso</th>
                  <th>Turno</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Ver</th>
                  <th>Cambiar Estado</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($mensuales as $value): ?>
                <tr>
                  <td><?=$value->idProceso?></td>
                  <td><?=$value->nombre_proceso?></td>
                  <td><?=$value->nombre_turno?></td>
                  <td><?=$value->nombre_tipo_proceso?></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
                <td>
                  <span class="mdl-chip mdl-chip--contact">
                    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                    <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                <?php } else {?>
                  <td>
                    <span class="mdl-chip mdl-chip--contact">
                      <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">I</span>
                      <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                  <?php }?>
                  <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Inactivo"?>/<?=$value->idProceso?>">Inactivar</a></td>
                  <?php } else {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Activo"?>/<?=$value->idProceso?>">Activar</a></td>
                  <?php }?>
                </tr>
              <?php endforeach?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     <section class="mdl-layout__tab-panel" id="fixed-tab-5">
      <div class="page-content">
        <div class="mdl-grid">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%">
            <div class="mdl-card__supporting-text">
              <center><h4>Procesos Diarios</h4></center>
              <br>
              <table id="diarios">
              <thead>
                <tr>
                  <th>Número</th>
                  <th>Proceso</th>
                  <th>Turno</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Ver</th>
                  <th>Cambiar Estado</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($diarios as $value): ?>
                <tr>
                  <td><?=$value->idProceso?></td>
                  <td><?=$value->nombre_proceso?></td>
                  <td><?=$value->nombre_turno?></td>
                  <td><?=$value->nombre_tipo_proceso?></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
                <td>
                  <span class="mdl-chip mdl-chip--contact">
                    <span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                    <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                <?php } else {?>
                  <td>
                    <span class="mdl-chip mdl-chip--contact">
                      <span class="mdl-chip__contact mdl-color--red mdl-color-text--white">I</span>
                      <span class="mdl-chip__text"><?=$value->estado_proceso?></span>
                  </span>
                </td>
                  <?php }?>
                  <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  <?php if ($value->estado_proceso == "Activo") {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Inactivo"?>/<?=$value->idProceso?>">Inactivar</a></td>
                  <?php } else {?>
          <td><a style="text-decoration:none;" href="<?=URL?>Home/descartarProceso/<?=$estado = "Activo"?>/<?=$value->idProceso?>">Activar</a></td>
                  <?php }?>
                </tr>
              <?php endforeach?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
</div>
 <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script src="<?=URL?>libs/Datatables/datatables.js"></script>
  <script>
    $(document).ready( function () {
      $('#procesos').DataTable();
      $('#procesos_pitney').DataTable();
      $('#semanales').DataTable();
      $('#diarios').DataTable();
      $('#mensuales').DataTable();
  } );
  </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>
<?php
ob_end_flush();
?>

