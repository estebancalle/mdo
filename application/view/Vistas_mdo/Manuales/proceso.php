<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Detalle del proceso</title>
  <!-- Material css y otros -->
    <link rel="short icon" href="<?=URL?>img/manual.png">
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
    <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
    <link rel="stylesheet" href="<?=URL?>css/pace.css">
    <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <!-- Google fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body>
  <!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title"><?=$proceso->nombre_proceso?></span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
     <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid">
        <div class="mdl-card mdl-shadow--16dp" style="width:100%">
          <div class="mdl-card__supporting-text">
            <h3 style="color:black"><?=$proceso->nombre_proceso?>
              <a href="<?=URL?>Home/editProceso/<?=$proceso->idProceso?>"><button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" onclick="activarFormularioEditar()" id="btn-editar-on"><i class="material-icons">edit</i></button></a>
            </h3>
            <hr>
            <h6 style="color:black">Contenido</h6>
            <ul>
              <?php foreach ($articulos as $value): ?>
                <li><a href="#<?=$value->nombre_articulo?>"><?=$value->nombre_articulo?></a></li>
              <?php endforeach?>
            </ul>
            <hr>
            <div class="introduccion" style="width:100%"><p style="text-align:justify;"><?=$proceso->intro_proceso?></p></div>
            <hr>
                <?php foreach ($detalleArticulo as $value): ?>
                    <?php if ($value->nombre_imagen == "") {?>
                      <div class="mdl-grid">
                          <div class="contenedor" style="width:100%">
                              <h4 style="color:black" id="<?=$value->nombre_articulo?>"><?=$value->nombre_articulo?></h4>
                              <hr>
                              <p style="text-align:justify;" class="articulo"><?=$value->descripcion_articulo?></p>
                          </div>
                        </div>
                    <?php } else {?>
                      <div class="mdl-grid">
                      <div class="mdl-cell mdl-cell--6-col">
                        <center><h4 style="color:black" id="<?=$value->nombre_articulo?>"><?=$value->nombre_articulo?></h4></center>
                          <hr>
                          <p style="text-align:justify;"><?=$value->descripcion_articulo?></p>
                      </div>
                      <div class="mdl-cell mdl-cell--6-col">
                        <div class="demo-card-image mdl-card mdl-shadow--2dp" style="width:100%;">
                        <div class="mdl-card__title mdl-card--expand">
                          <img src="<?=URL?>galeria/<?=$value->nombre_imagen?>" style="max-width:100%; max-height:400px;">
                        </div>
                      <!--   <div class="mdl-card__actions">
                          <span class="demo-card-image__filename"><?=$value->nombre_imagen?></span>
                        </div> -->
                        </div>
                      </div>
                  </div>
                    <?php }?>
                  <?php endforeach?>
          </div>
        </div>
      </div>
     <!--  <footer class="mdl-mini-footer" style="width:100%">
          <div class="mdl-mini-footer__left-section">
          <div class="mdl-logo">MDO &copy</div>
            <ul class="mdl-mini-footer__link-list">
              <li><a href="menestys-services.com">Menestys Services S.A.S</a></li>
              <li><a href="#">Acerca de</a></li>
            </ul>
          </div>
      </footer> -->
      <div class="mdl-grid">

      </div>
    </div>
  </main>
</div>
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script src="<?=URL?>libs/Datatables/datatables.js"></script>
</body>
</html>
