<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Editar proceso</title>
    <!-- Material css y otros -->
    <link rel="short icon" href="<?=URL?>img/manual.png">
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
    <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
    <link rel="stylesheet" href="<?=URL?>css/pace.css">
    <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <!-- Google fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Toastr -->
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
</head>
<style>
    body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
    }
</style>
<body>
    <!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
        <!-- Title -->
            <span class="mdl-layout-title">Editar <?=$proceso->nombre_proceso?></span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation. We hide it in small screens. -->
                <nav class="mdl-navigation mdl-layout--large-screen-only">
                    <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
                    <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
                        Inicio
                    </div>
                    <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
                    <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
                        Nuevo Usuario
                    </div>
                    <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
                    <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
                        Nuevo Proceso
                    </div>
                    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
                </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
        <center>
            <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
                <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
                <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
                <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
            </span>
        <hr>
        </center>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
            <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
            <?php
                $encriptacion = base64_encode($_SESSION["id_usu"])
            ?>
            <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">
            <div class="mdl-grid">
                <div class="mdl-card" style="width:100%">
                    <div class="mdl-card__supporting-text">
                        <h3 style="color:black">Editar <?=$proceso->nombre_proceso?>
                            <a id="volver" href="<?=URL?>Home/cargarProceso/<?=$proceso->idProceso?>"><button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" id="btn-editar-on"><i class="material-icons">cancel</i></button></a>
                            <div class="mdl-tooltip" data-mdl-for="volver">
                                Volver a a vista del proceso
                            </div>
                        </h3>
                        <p style="text-align:justify">Hola <?=$_SESSION["alias"]?>, usted está en la vista de edición para los procesos, desde acá usted podrá:</p>
                        <ul>
                            <li>Editar la introducción del proceso es decir el inicio.</li>
                            <li>Eliminar o cambiar las imágenes de los artículos.</li>
                            <li>Eliminar los artículos necesarios.</li>
                            <li>Añadir un nuevo artículo.</li>
                        </ul>
                        <hr>
                        <!-- Contenedor externo -->
                            <div class="mdl-tabs mdl-js-tabs">
                                <div class="mdl-tabs__tab-bar">
                                    <a href="#tab1" class="mdl-tabs__tab is-active">Introducción</a>
                                    <a href="#tab2" class="mdl-tabs__tab">Artículos</a>
                                    <a href="#tab3" class="mdl-tabs__tab">Nuevos Artículos</a>
                                </div>
                                <div class="mdl-tabs__panel is-active" id="tab1"><br>
                                    <!-- contenido de la pestaña de intro -->
                                        <div class="mdl-card mdl-shadow--2dp" style="width:100%">
                                            <div class="mdl-card__supporting-text">
                                                <h5 style="color:black;">Editar encabezado de Introducción</h5>
                                                <hr>
                                                <input type="hidden" name="cod_proceso" id="txtcod_proceso" value="<?=$proceso->idProceso?>">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" name="txtnombreprocesoajax" id="txtnombre_proceso" value="<?=$proceso->nombre_proceso?>">
                                                    <label class="mdl-textfield__label" for="sample3">Nombre del proceso</label>
                                                </div>
                                                <br>
                                                <div class="mdl-textfield mdl-js-textfield" style="width:100%">
                                                    <textarea class="mdl-textfield__input" type="text" rows= "3" id="txtdescripcion_proceso" name="txtdescripcionprocesoajax"><?=$proceso->intro_proceso?></textarea>
                                                    <label class="mdl-textfield__label" for="sample5">Text lines...</label>
                                                </div>
                                                <br>
                                                <center>
                                                <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" onclick="actualizarEncabezadoProceso()">
                                                    <i class="material-icons">check</i>
                                                </button>
                                                </center>
                                            </div>
                                        </div>
                                    <!-- contenido de la pestaña de intro -->
                                </div>
                                <div class="mdl-tabs__panel" id="tab2"><br>
                                    <!-- contenido de la pestaña de artículos -->
                                        <div class="mdl-card mdl-shadow--2dp" style="width:100%">
                                            <div class="mdl-card__supporting-text">
                                                <h5 style="color:black">Editar artículos</h5>
                                                <hr>
                                                <div class="mdl-grid">
                                                    <!-- SECCIÓN DEL SELECT DE ARTICULOS -->
                                                        <div class="md-cell mdl-cel--6-col">
                                                            <div class="mdl-card" style="width:100%">
                                                                <div class="mdl-card__supporting-text">
                                                                    <label>Seleccionar Artículo</label><br>
                                                                    <select name="" id="selectArticulos" onchange="listarArticuloSeleccionado()">
                                                                        <?php foreach($articulos as $value): ?>
                                                                            <option value="<?=$value->idarticulo?>"><?=$value->nombre_articulo?></option>
                                                                        <?php endforeach; ?>
                                                                    </select><br>
                                                                    <br>
                                                                    <input type="hidden" id="articuloparaeliminar">
                                                                    <center>
                                                                        <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored" onclick="eliminarArticulo()">
                                                                            <i class="material-icons">delete</i>
                                                                        </button>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <!-- SECCIÓN DEL SELECT DE ARTICULOS -->

                                                    <!-- SECCIÓN DEL ARTICULO SELECCIONADO -->
                                                        <div class="mdl-cell mdl-cell--6-col">
                                                            <div class="mdl-card" style="width:100%">
                                                                <h5 style="color:black">Editar Artículo</h5>
                                                                <hr>
                                                                <div class="mdl-card__supporting-text" id="cartaedit-1">
                                                                    <input type="hidden" id="txtcodarticulo">
                                                                    <label>Nombre del Artículo</label><br>
                                                                    <div class="mdl-textfield mdl-js-textfield" style="width:100%">
                                                                        <input class="mdl-textfield__input" type="text" id="txtnombrearticulo">
                                                                        <label class="mdl-textfield__label" for="sample3"></label>
                                                                    </div><br>
                                                                    <label>Descripción</label><br>
                                                                    <div class="mdl-textfield mdl-js-textfield" style="width:100%;">
                                                                        <textarea class="mdl-textfield__input" type="text" rows="5" cols="10" id="txtdecripcionart"></textarea>
                                                                        <label class="mdl-textfield__label" for="sample5"></label>
                                                                    </div>
                                                                </div>
                                                                <center>
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" onclick="actualizarArticulo()">
                                                                        <i class="material-icons">check</i>
                                                                    </button>
                                                                </center>
                                                            </div>
                                                            <!-- <input type="hidden" id="imagenllena"> -->
                                                            <h5 style="color:black">Editar o añadir Imagen</h5>
                                                            <hr>
                                                            <label>Imagen</label><br><br>
                                                            <div class="demo-card-image mdl-card mdl-shadow--2dp" style="width:100%;">
                                                                <div class="mdl-card__title mdl-card--expand" id="imagenajax">
                                                                    <img src="" style="max-width:100%; max-height:400px;" id="imagenarticulo">
                                                                </div>
                                                                <div class="mdl-card__actions mdl-card--border">
                                                                    <form action="<?=URL?>Home/actualizarImagenArticulo" enctype="multipart/form-data" method="POST">
                                                                        <input type="hidden" name="cod_p" value="<?=$proceso->idProceso?>">
                                                                        <input type="hidden" id="cod_imagen_generada" name="cod_img">
                                                                        <input type="file" id="imagenart" name="imagen">
                                                                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" type="submit">
                                                                            Guardar
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                <div class="mdl-card__menu">
                                                                    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" onclick="eliminarImagen()">
                                                                    <i class="material-icons">delete</i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    <!-- SECCIÓN DEL ARTICULO SELECCIONADO -->
                                                </div>
                                            </div>
                                        </div>
                                    <!-- contenido de la pestaña de artículos -->
                                </div>
                                <div class="mdl-tabs__panel" id="tab3"><br>
                                    <div class="mdl-card mdl-shadow--2dp" style="width:100%">
                                        <div class="mdl-card__supporting-text">
                                            <h5 style="color:black">Añadir nuevo artículo</h5>
                                            <hr>
                                            <form action="<?=URL?>Home/registrarArticulo" enctype="multipart/form-data" method="POST">
                                                <input type="hidden" name="txtcodproceso" value="<?=$proceso->idProceso?>">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3" name="txtnuevotituloproceso">
                                                    <label class="mdl-textfield__label" for="sample3">Título</label>
                                                </div>
                                                <br>
                                                <div class="mdl-textfield mdl-js-textfield" style="width:100%">
                                                    <textarea class="mdl-textfield__input" type="text" rows= "3" id="txtdescripcion_proceso" name="txtdesxcripcionarticulonuevo"></textarea>
                                                    <label class="mdl-textfield__label" for="sample5">Descripción</label>
                                                </div>
                                                <br>
                                                <label>Imagen</label><br>
                                                <input type="file" name="imagennueva" id=""><br><br>
                                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                                                    Registrar
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        <!-- Contenedor Externo -->
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
    <!-- MODAL -->
        <div class="modal">
            
        </div>
    <!-- MODAL -->
    <script src="<?=URL?>/js/jquery.js"></script>
    <script src="<?=URL?>/js/pace.js"></script>
    <script src="<?=URL?>/js/modal.js"></script>
    <script src="<?=URL?>/js/scripts/editarProceso.js"></script>
    <!-- Material js -->
    <script src="<?=URL?>/js/material.js"></script>
    <script src="<?=URL?>libs/Datatables/datatables.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- Toastr -->
    <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#selectArticulos").select2();
        })

        // =====================================================================
        // AJAX PARA LISTAR EL ARTÍCULO SELECCIONADO
        // =====================================================================
        function listarArticuloSeleccionado(){
            var url="<?php echo URL?>";
            let cod_p=$("#selectArticulos option:selected").val();
            
            let request=$.ajax({
                method:"POST",
                url: url + "Home/listarDetalleArticulo",
                datatype:'json',
                data:{op : cod_p}
            })
                .done(function(request){
                    var datos=$.parseJSON(request);
                    var url2="<?php echo URL?>";
                    $("#txtnombrearticulo").empty();
                    $("#txtdecripcionart").empty();
                    $("#imagenarticulo").empty();
                    $("#txtnombrearticulo").val(datos.nombre_articulo);
                    $("#txtdecripcionart").text(datos.descripcion_articulo);
                    $("#articuloparaeliminar").val(datos.idarticulo);
                    
                    if (datos.nombre_imagen=="") {
                        $("#imagenarticulo").hide();
                        toastr.error('Éste artículo no tiene imagen...');
                        $("#cod_imagen_generada").val(datos.idimagen);
                        // $("#txtcodarticulo").val(datos.idarticulo);

                    }else{
                        $("#imagenarticulo").show();
                        $("#imagenarticulo").attr("src", url2+"galeria/"+datos.nombre_imagen);
                        $("#cod_imagen_generada").val(datos.idimagen);
                        var files = document.getElementById('files').files;
                        // $("#txtcodarticulo").val(datos.idarticulo);
                    }
                    // $("#cartaedit-1").append(inputs);
                    
                })
                .fail(function(error){
                    console.log(error);
                })
        }

        // =====================================================================
        // AJAX PARA ACTUALIZAR EL ENCABEZADO DEL PROCESO
        // =====================================================================
        function actualizarEncabezadoProceso(){
            var url="<?php echo URL?>";
            let nombre_proceso=$("#txtnombre_proceso").val();
            let descripcion_proceso=$("#txtdescripcion_proceso").val();
            let cod_proceso=$("#txtcod_proceso").val();
            
            if (confirm("Desea continuar?")) {
                    let respuesta=$.ajax({
                    method:"POST",
                    url: url + "Home/actualizarEncabezadoProceso",
                    datatype:'json',
                    data:{nombreP : nombre_proceso, descripcionP : descripcion_proceso, codP : cod_proceso}
                })
                .done(function(rpta){
                    if (rpta!=null) {
                        toastr.success('Se actualizó el nombre y el intro del proceso')   
                        setInterval(function(){
                            location.reload();
                        },1000);                 
                    }else{
                        toastr.error('No se pudo completar ésta operación');
                    }
                })
                .fail(function(error){
                    console.log(error);
                })
            }
            else{

            }
        }


        function eliminarImagen(){
            var url="<?php echo URL?>";
            var cod_imagen=$("#cod_imagen_generada").val();
            if ($("#cod_imagen_generada").val()=="") {
                toastr.error("No se hay na imagen en éste momento");
            }else{
                if (confirm("Seguro que desea eliminar ésta imagen")) {
                    let request=$.ajax({
                        method:"POST",
                        url: url + "Home/eliminarImagenArticulo",
                        datatype:'json',
                        data:{codI : cod_imagen}
                    })
                    .done(function(respuesta){
                        if (respuesta!=null) {
                            toastr.success("Se ha eliminado la imagen");
                            setInterval(function(){
                                location.reload();
                            },1000);   
                        }else{
                            toastr.error("No se podido completar la operación");
                        }
                    })
                    .fail(function(error){
                        console.log(error);
                    })
                }
                else{

                }
            }
        }

        function actualizarArticulo(){
            var url="<?php echo URL?>";
            if (confirm("Desea guardar los cambios")) {
                var nombre_articulo=$("#txtnombrearticulo").val();
                var descripcion=$("#txtdecripcionart").val();
                var cod_articulo=$("#articuloparaeliminar").val();

                let request=$.ajax({
                    method:"POST",
                    url:url+"Home/actualizarArticulo",
                    datatype:'json',
                    data:{nombreA : nombre_articulo , descripcionA : descripcion , codA : cod_articulo }
                })
                .done(function(respuesta){
                    if (respuesta!=null) {
                        toastr.success("Se ha actualizado el artículo");
                        setInterval(function(){
                                location.reload();
                        },1000);   
                    }else{
                        toastr.error("Error durante la operación");
                    }
                })
                .fail(function(error){
                    toastr.error(error);
                })
            }else{

            }
        }
        
        function eliminarArticulo(){
            var url="<?php echo URL?>";
            var articulo=$("#articuloparaeliminar").val();
            if (confirm("Seguro que desea eliminar éste artículo, ésta acción no tiene reverso")) {
                $.ajax({
                    method:"POST",
                    url:url+"Home/eliminarArticulo",
                    datatype:'json',
                    data:{txtcodarticulo : articulo}
                })
                .done(function(respuesta){
                    if (respuesta!=null) {
                        toastr.success("Se ha eliminado el artículo");
                        setInterval(function(){
                                location.reload();
                        },1000);   
                    }else{
                        toastr.error("No se pudo completar la operación");
                    }
                })
                .fail(function(error){
                    console.log(error);
                })
            }else{

            }
        }
    </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>
