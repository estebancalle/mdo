<?php 
if (!$_SESSION['id_usu']) {
    header("location: ".URL."home");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Turnos</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
	<!-- Material css y otros -->
  	<link rel="stylesheet" href="<?=URL?>css/material.min.css">
	  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
    <link rel="stylesheet" href="<?=URL?>css/pace.css">
    <!-- Toastrjs -->
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
    <link rel="stylesheet" href="<?=URL?>libs/Datatables/datatables.css">
	<!-- Google fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
	<!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Turnos</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
      <!-- <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="">Ciclos</a>
        <a class="mdl-navigation__link" href="">Procesos</a>
        <a class="mdl-navigation__link" href="">Turnos</a>
      </nav> -->
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">MDO</span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>home/iniciar"><i class="material-icons">arrow_back</i> Volver</a>
      <a class="mdl-navigation__link" href="<?=URL?>login/cerrarSesion">Cerrar Sesión</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <br>
      <!-- Contenido procesos -->
      <center><h3>Procesos del turno</h3></center>
        <div class="mdl-grid">
          <div class="mdl-cell mdl-cell--4-col"></div>
          <div class="mdl-cell mdl-cell--4-col">
            <table class="TablaProcesos">
            <thead>
              <tr>
                <th>Proceso</th>
                <th>Tipo</th>
                <th>Turno</th>
                <th>Ver</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($turno as $value): ?>
                    <tr>
                      <td><?=$value->nombre_proceso?></td>
                      <td><?=$value->nombre_tipo_proceso?></td>
                      <td><?=$value->nombre_turno?></td>
                    <td><a href="<?=URL?>home/cargarProceso/<?=$value->idProceso?>"><i class="material-icons">remove_red_eye</i></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
          </div>
          <div class="mdl-cell mdl-cell--4-col"></div>
        </div>
      <!-- Fin contenido procesos -->
    </div>
  </main>
</div>
	<!-- Jquery -->
	<script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
	<!-- Material js -->
	<script src="<?=URL?>/js/material.js"></script>
  <!-- Toastrjs -->
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script src="<?=URL?>libs/Datatables/datatables.js"></script>
</body>
<script>
  $(document).ready( function () {
    $('.TablaProcesos').DataTable();
} );
</script>
<script>
  $(".descripcion_proceso").dblclick(function(){
    $(".descripcion_proceso").hide();
    $(".editar").show("fast");
  })

  $(".editar").dblclick(function(){
    $(".editar").hide("fast");
    $(".descripcion_proceso").show("fast");
  })
</script>
<?php
    if(isset($_SESSION['mensaje'])){
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje']=null;
  }
?>
</html>