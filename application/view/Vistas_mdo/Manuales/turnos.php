<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Turnos</title>
   <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>css/modal.css">
  <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="<?=URL?>libs/Datatables/datatables.css">
</head>
<body>
  <!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Lista de turnos</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
       <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
   <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--4-col">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%;">
            <div class="mdl-card__title" style="background:url(https://images.pexels.com/photos/100077/pexels-photo-100077.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940)center/cover;">
              <center><h4 style="color:white;">Turno de la Mañana</h4></center>
            </div>
            <div class="mdl-card__supporting-text">
              <table id="morning">
                <thead>
                  <tr>
                    <th>Número</th>
                    <th>Proceso</th>
                    <th>Tipo</th>
                    <th>Ver</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($morning as $value): ?>
                  <tr>
                    <td><?=$value->idProceso?></td>
                    <td><?=$value->nombre_proceso?></td>
                    <td><?=$value->nombre_tipo_proceso?></td>
                    <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  </tr>
                  <?php endforeach?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="mdl-cell mdl-cell--4-col">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%;">
            <div class="mdl-card__title" style="background:url(https://images.pexels.com/photos/69224/pexels-photo-69224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940)center/cover;">
              <center><h4 style="color:white;">Turno de la Tarde</h4></center>
            </div>
            <div class="mdl-card__supporting-text">
              <table id="after">
                <thead>
                  <tr>
                    <th>Número</th>
                    <th>Proceso</th>
                    <th>Tipo</th>
                    <th>Ver</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($after as $value): ?>
                  <tr>
                    <td><?=$value->idProceso?></td>
                    <td><?=$value->nombre_proceso?></td>
                    <td><?=$value->nombre_tipo_proceso?></td>
                    <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  </tr>
                  <?php endforeach?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="mdl-cell mdl-cell--4-col">
          <div class="mdl-card mdl-shadow--8dp" style="width:100%;">
            <div class="mdl-card__title" style="background:url(https://images.pexels.com/photos/355465/pexels-photo-355465.jpeg?auto=compress&cs=tinysrgb&h=350)center/cover;">
              <center><h4 style="color:white;">Turno de la Noche</h4></center>
            </div>
            <div class="mdl-card__supporting-text">
              <table id="night">
                <thead>
                  <tr>
                    <th>Número</th>
                    <th>Proceso</th>
                    <th>Tipo</th>
                    <th>Ver</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($night as $value): ?>
                  <tr>
                    <td><?=$value->idProceso?></td>
                    <td><?=$value->nombre_proceso?></td>
                    <td><?=$value->nombre_tipo_proceso?></td>
                    <td><a href="<?=URL?>Home/cargarProceso/<?=$value->idProceso?>">Ver en Detalle</a></td>
                  </tr>
                  <?php endforeach?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <footer class="mdl-mini-footer" style="width:100%">
          <div class="mdl-mini-footer__left-section">
          <div class="mdl-logo">MDO &copy</div>
            <ul class="mdl-mini-footer__link-list">
              <li><a href="menestys-services.com">Menestys Services S.A.S</a></li>
              <li><a href="#">Acerca de</a></li>
            </ul>
          </div>
      </footer>
      </div>
    </div>
  </main>
</div>

   <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script src="<?=URL?>libs/Datatables/datatables.js"></script>
  <script>
    $(document).ready( function () {
      $('#morning').DataTable();
      $('#after').DataTable();
      $('#night').DataTable();
  } );
  </script>
</body>
</html>
