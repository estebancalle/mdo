<!DOCTYPE html>
<html>
<head>
	<title>Página no Disponible</title>
	<link rel="short icon" href="<?=URL?>img/manual.png">
	<link rel="stylesheet" href="<?=URL?>css/material.min.css">
	<link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet"> 
</head>
<body style="background-color:#3498DB">
	<div class="mdl-grid">
		<!-- Wide card with share menu button -->
		<style>
		h1,h2,h3,h4{
			font-family: 'Questrial', sans-serif;
		}
		.mdl-card {
			width: 100%;
			border: 2px;
			padding: 10px;
			border-radius: 50px 20px;
			margin-top:100px;
			font-family: 'Questrial', sans-serif;
		}
		.mdl-card__title {
			color: #fff;
			height: 300px;
			background: url('http://material-design.storage.googleapis.com/publish/v_2/material_ext_publish/0Bx4BSt6jniD7Sy1kVWlTS1NnNGM/style_imagery_bestpractices_focus5.png') center / cover;
			border: 2px;
			padding: 10px;
			border-radius: 50px 20px;
		}
		.mdl-card__title>h2{
			font-size:90px;
		}
		.demo-card-wide > .mdl-card__menu {
			color: #fff;
		}

		.col2{
			align-items: :center;
			width:100%;
			margin-top:100px;
			color:white;
		}

		#imback{
			color:white;
			size:60px;
		}
	</style>

	<div class="mdl-cell mdl-cell--6-col">
		<div class="mdl-card mdl-shadow--8dp">
			<div class="mdl-card__title">
				<h2 class="mdl-card__title-text ">404</h2>
			</div>
			<div class="mdl-card__supporting-text">
				Ésta página no está disponible de momento o no está funcionando, por favor vuelva al inicio
			</div>
		</div>
	</div>
	<div class="mdl-cell mdl-cell--6-col">
		<div class="col2">
			<center>
				<h2 style="font-size:70px">Página</h2>
				<h5 style="font-size:40px">No</h5>
				<h1 style="font-size:90px;">Disponible</h1>
				<br>
				<a href="<?=URL?>Home/Iniciar">
					<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="imback">
  						Regresar al Inicio
					</button>
				</a>
			</center>
		</div>
	</div>
</div>
</body>
</html>