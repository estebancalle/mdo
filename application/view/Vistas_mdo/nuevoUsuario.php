<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
if ($_SESSION["rol"]==2) {
  header("location: " . URL . "home/iniciar");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Nuevo Usuario</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body onload="generarCodigo()">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Nuevo Usuario</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
       <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons" style="color:blue;">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
   <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid">
      <div class="mdl-cell mdl-cell--6-col">
        <!-- Wide card with share menu button -->
        <style>
          .demo-card-wide.mdl-card {
              width:100%;
          }
          .demo-card-wide > .mdl-card__title {
              color: #fff;
              height: 176px;
              background: url('<?=URL?>img/pattern.jpg') center / cover;
          }
          .demo-card-wide > .mdl-card__menu {
              color: #fff;
          }
        </style>
        <div class="demo-card-wide mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title">
              <h2 class="mdl-card__title-text">Nuevo Usuario</h2>
            </div>
            <div class="mdl-card__supporting-text">
              <center><p>Ingrese los campos</p></center>
                <form action="<?=URL?>Home/nuevosuario" method="POST">
                  <center>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%">
                <input class="mdl-textfield__input" type="text" id="sample3" name="txtnombre">
                <label class="mdl-textfield__label" for="sample3">Nombre *</label>
              </div>
              <br>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%">
                <input class="mdl-textfield__input" type="text" id="sample3" name="txtapellido">
                <label class="mdl-textfield__label" for="sample3">Apellido</label>
              </div>
              <br>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%">
                <input class="mdl-textfield__input" type="text" id="sample3" name="txtcorreo">
                <label class="mdl-textfield__label" for="sample3">Correo</label>
              </div>
              <br>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%">
                <select class="mdl-textfield__input" name="txtrol">
                  <?php foreach($roles as $value): ?>
                    <option value="<?=$value->cod_rol?>"><?=$value->nombre_rol?></option>
                  <?php endforeach; ?>
                </select>
                <label class="mdl-textfield__label" for="octane">Rol</label>
              </div>
              <br>
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="hidden" id="txtcod" readonly name="txtcod">
                <!-- <label class="mdl-textfield__label" for="sample3">Código de Seguridad</label> -->
              </div>
              <br>
              <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                Registrar
            </button>
                  </center>
                </form>
            </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--6-col">
        <center><h4>Usuarios</h4></center>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style="width:100%">
          <thead>
            <tr>
              <th class="mdl-data-table__cell--non-numeric">Nombre</th>
              <th class="mdl-data-table__cell--non-numeric">Apellido</th>
              <th class="mdl-data-table__cell--non-numeric">Tipo</th>
              <th class="mdl-data-table__cell--non-numeric">Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($usuarios as $value): ?>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric"><?=$value->nombre_usuario?></td>
                  <td class="mdl-data-table__cell--non-numeric"><?=$value->apellido_usuario?></td>
                  <td class="mdl-data-table__cell--non-numeric"><?=$value->nombre_rol?></td>
                  <?php if($value->estado_usuario=="Online"){ ?>
                    <td class="mdl-data-table__cell--non-numeric"><p style="color:#3498DB"><?=$value->estado_usuario?></p></td>
                    <td>NA</td>
                  <?php }else if($value->estado_usuario=="Activo"){ ?>
                    <td class="mdl-data-table__cell--non-numeric"><p style="color:#27AE60"><?=$value->estado_usuario?></p></td>
                    <td>
                      <button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" id="blockU<?=$value->idusuario?>" type="button" onclick="bloquearUsuario(<?=$value->idusuario?>)">
                        <i class="material-icons">block</i>
                      </button>
                      <div class="mdl-tooltip mdl-tooltip--large" for="blockU<?=$value->idusuario?>">
                        Bloquear
                      </div>
                    </td>
                  <?php }else{ ?>
                    <td class="mdl-data-table__cell--non-numeric"><p style="color:#C0392B"><?=$value->estado_usuario?></p></td>
                    <td>
                      <button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" id="ActU<?=$value->idusuario?>" type="button" onclick="ActivarUsuario(<?=$value->idusuario?>)">
                        <i class="material-icons">check</i>
                      </button>
                      <div class="mdl-tooltip mdl-tooltip--large" for="ActU<?=$value->idusuario?>">
                        Activar
                      </div>
                    </td>
                  <?php } ?>
                </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <footer class="mdl-mini-footer" style="width:100%">
          <div class="mdl-mini-footer__left-section">
          <div class="mdl-logo">MDO &copy</div>
            <ul class="mdl-mini-footer__link-list">
              <li><a href="menestys-services.com">Menestys Services S.A.S</a></li>
              <li><a href="#">Acerca de</a></li>
            </ul>
          </div>
      </footer>
      </div>
    </div>
  </main>
</div>
  <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
    <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script>
    function generarCodigo(){
      var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789!@$%/#?&";
          var contraseña = "";
          for (i=0; i<10; i++) contraseña +=caracteres.charAt(Math.floor(Math.random()*caracteres.length));
            $("#txtcod").val(contraseña);
      }


      function bloquearUsuario(u){
        var url="<?php echo URL ?>";
        var usu=u;
        if (confirm("¿Desea bloquear éste usuario y no permitirle el acceso?")) {
          $.ajax({
            method:"POST",
            url:url+"Home/bloquearUsuario",
            datatype:'json',
            data:{usuario: usu}
          })
          .done(function(rpta){
            if (rpta!=null) {
              toastr.success("Usuario Bloqueado");
            }else{
              toastr.error("ERROR DURANTE LA OPERACIÓN");
            }
          })
          .fail(function(error){
            console.log(error);
          })
        }else{

        }
      }

      function ActivarUsuario(us){
        var url="<?php echo URL ?>";
        var usu=us;
        if (confirm("¿Desea Activar éste usuario y permitirle el acceso?")) {
          $.ajax({
            method:"POST",
            url:url+"Home/ActivarUsuario",
            datatype:'json',
            data:{usuario: usu}
          })
          .done(function(rpta){
            if (rpta!=null) {
              toastr.success("Usuario Activado");
            }else{
              toastr.error("ERROR DURANTE LA OPERACIÓN");
            }
          })
          .fail(function(error){
            console.log(error);
          })
        }else{

        }
      }
  </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>
