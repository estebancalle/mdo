<!DOCTYPE html>
<html lang="en">
<head><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124730824-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124730824-1');
</script>

	<meta charset="UTF-8">
	<title>MDO</title>
	<!-- Material css  -->
  	<link rel="short icon" href="<?=URL?>img/LOGO-MDO.png">
	<link rel="stylesheet" href="<?=URL?>css/material.min.css">
	<link rel="stylesheet" href="<?=URL?>css/Nativos.css">
	<link rel="stylesheet" href="<?=URL?>css/login.css">
	<!-- Librerias -->
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
	body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
		font-family: 'Questrial', sans-serif;
	}
</style>
<body>
	<!-- Inicio formulario -->
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--4-col"></div>
		<div class="mdl-cell mdl-cell--4-col">
			<center>
				<div class="mdl-card mdl-shadow--16dp" id="form">
				<div class="mdl-card__supporting-text">
					<center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
					<center><h3>Iniciar Sesión</h3></center>
					<hr>
					<form action="<?=URL?>Login/validarL" method="POST">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    					<input class="mdl-textfield__input" type="email" id="correo" name="txtcorreo">
    					<label class="mdl-textfield__label" for="correo">Correo</label>
    					<div class="mensaje1"></div>
  					</div>
  					<br>
  					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    					<input class="mdl-textfield__input" type="password" id="pass" name="txtpass">
    					<label class="mdl-textfield__label" for="pass">Contraseña</label>
    					<div class="mensaje2"></div>
  					</div>
  					<br>
  					<!-- <div class="g-recaptcha" data-sitekey="6LfrdmMUAAAAABHPUgKqLO78Bb0_XHOJ-1p2fFD7"></div> -->
  					<br>
  					<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="logearse">
  						Entrar
					</button>
					<br><br>
					<center><a href="#" id="btnrecuperar">¿Olvidó la contraseña?</a></center>
					<center><p>MDO &copy <?=VERSION?></center>
					</form>
				</div>
			</div>
			<!-- Contenedor recuperar pass -->
			<div class="mdl-card mdl-shadow--16dp" id="recuperar" style="display:none;">
				<div class="mdl-card__supporting-text">
					<center><h3>Recuperar Contraseña</h3></center>
					<hr>
					<form action="<?=URL?>Login/recupass" method="POST">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    					<input class="mdl-textfield__input" type="email" id="correo" name="txtcorreo">
    					<label class="mdl-textfield__label" for="correo">Correo</label>
    					<div class="mensaje1"></div>
  					</div>
  					<br>
  					<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="logearse">
  						Enviar
					</button>
					<br><br>
					<center><a href="#" id="btnvolver">Volver</a></center>
					</form>
				</div>
			</div>
			</center>
		</div>
		<div class="mdl-cell mdl-cell--4-col"></div>
	</div>
	<!-- Jquery -->
	<script src="<?=URL?>/js/jquery.js"></script>
	<!-- Material js -->
	<script src="<?=URL?>/js/material.js"></script>
	<!-- Librerias -->
  	<script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
	<script src="<?=URL?>js/scripts/validaciones.js"></script>
	<script>
		$("#correo").keyup(function(){
				let Expresion = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		// VALIDAR CORREO
		if(!Expresion.test($(this).val())){
			$(".Mensaje1").html("<p style='color:red;'>El formato de correo es Incorrecto</p>");
			$("#logearse").prop("disabled",true);
		}
		// VALIDAR CORREO Y CADENA
		else if(Expresion.test($(this).val())==true && $(this).val().length > 1 && $(this).val().length <= 40 ){
			$(".Mensaje1").html("<p style='color:green;'>Correcto</p>");
			$("#logearse").prop("disabled",false);
		}
		// VALIDAR CANTIDAD DE CARACTERES
		else if($(this).val().length > 40){
			$(".Mensaje1").html("<p style='color:red;'>El máximo de caracteres es de 40</p>");
			$("#logearse").prop("disabled",true);
		}
		// VALIDACION SI EL CAMPO ESTA VACIO
		else if($(this).val().length == 0) {
			$(".Mensaje1").html("<p style='color:red;'>El campo no puede estar vacio</p>");
			$("#logearse").prop("disabled",true);
		}
		})


		$("#pass").keyup(function(){
			if($(this).val().length=0){
				$(".Mensaje2").html("<p style='color:red;'>El campo no puede estar vacio</p>");
				$("#logearse").prop("disabled",true);
			}
			else if($(this).val().length>50){
				$(".Mensaje2").html("<p style='color:red;'>El campo no puede tener más de 50 caracteres</p>");
				$("#logearse").prop("disabled",true);
			}
			else{
				$("#logearse").prop("disabled",false);
			}
		})

		$("#btnrecuperar").click(function(){
			$("#form").animate().hide();
			$("#recuperar").animate().show();
		})

		$("#btnvolver").click(function(){
			$("#recuperar").animate().hide();
			$("#form").animate().show();
		})
	</script>
</body>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</html>