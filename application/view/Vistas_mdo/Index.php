<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124730824-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124730824-1');
</script>

  <meta charset="UTF-8">
  <title>Manuales de Operación</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
  <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
  <link rel="stylesheet" href="<?=URL?>css/pace.css">
  <link rel="stylesheet" href="<?=URL?>css/modal.css">
  <!-- Google fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body onload="saberHora(),tutorial()">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer
            mdl-layout--fixed-header">
  <header class="mdl-layout__header mdl-shadow--2dp">
    <div class="mdl-layout__header-row">
      <!-- Título -->
       <span class="mdl-layout-title">Inicio</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
     <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons" style="color:blue;">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
    <div class="page-content">
      <div class="mdl-grid">
        <!-- DAY CARD -->
        <div class="dia" style="display:none; width:100%;" >
          <style>
            .demo-card-wide.mdl-card {
                width: 100%;
            }
            .demo-card-wide > #morning {
                color: #fff;
                height: 250px;
                background: url('<?=URL?>img/morning.jpg') center / cover;
            }
            .demo-card-wide > .mdl-card__menu {
                color: #fff;
            }
          </style>
          <div class="demo-card-wide mdl-card mdl-shadow--2dp">
              <div class="mdl-card__title" id="morning">
                <h2 class="mdl-card__title-text" style="font-size:40px;">Buenos Días <?=$_SESSION["alias"]?>, ¿qué desea hacer hoy?</h2>
              </div>
              <div class="mdl-card__supporting-text">
                              <div class="mdl-card-supporting-text">
                <div class="mdl-grid">
                  <style>
                        .demo-card-event.mdl-card {
                          width: 256px;
                          height: 200px;
                          background:#3498DB;
                        }
                        .demo-card-event > .mdl-card__actions {
                          border-color:#3498DB;
                        }
                        .demo-card-event > .mdl-card__title {
                          align-items: flex-start;
                        }
                        .demo-card-event > .mdl-card__title > h4 {
                          margin-top: 0;
                        }
                        .demo-card-event > .mdl-card__actions {
                          display: flex;
                          box-sizing:border-box;
                          align-items: center;
                        }
                        .demo-card-event > .mdl-card__actions > .material-icons {
                          padding-right: 10px;
                        }
                        .demo-card-event > .mdl-card__title,
                        .demo-card-event > .mdl-card__actions,
                        .demo-card-event > .mdl-card__actions > .mdl-button {
                          color: #fff;
                        }
                    </style>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                       <!-- carta escalamiento -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Escalamiento</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre y conozca los escalamientos respectivos a los procesos</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/escalamiento">
                            Conocer
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">trending_up</i>
                          </div>
                      </div>
                    <!-- carta escalamiento -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta usuario -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Usuario</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo usuario para darle acceso al sistema</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newUser">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">person_add</i>
                          </div>
                      </div>
                    <!-- carta usuario -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta añadir -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Proceso</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo proceso para la wiki</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newProcess">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">add_circle</i>
                          </div>
                      </div>
                    <!-- carta añadir -->
                    </center>
                  </div>
              </div>
          </div>
              </div>
        </div>
      </div>
        <!-- END DAY CARD -->
        <!-- AFTER CARD -->
        <div class="tarde" style="display:none; width:100%">
          <style>
            .demo-card-wide.mdl-card {
                width: 100%;
            }
            .demo-card-wide > #after {
                color: #fff;
                height: 250px;
                background: url('<?=URL?>img/tardes.jpg') center / cover;
            }
            .demo-card-wide > .mdl-card__menu {
                color: #fff;
            }
          </style>
          <div class="demo-card-wide mdl-card mdl-shadow--2dp">
              <div class="mdl-card__title" id="after">
                <h2 class="mdl-card__title-text" style="font-size:40px;">Buenas Tardes <?=$_SESSION["alias"]?>,tal vez le interese esto:</h2>
              </div>
              <div class="mdl-card__supporting-text">
                              <div class="mdl-card-supporting-text">
                <div class="mdl-grid">
                  <style>
                        .demo-card-event.mdl-card {
                          width: 256px;
                          height: 200px;
                          background:#3498DB;
                        }
                        .demo-card-event > .mdl-card__actions {
                          border-color:#3498DB;
                        }
                        .demo-card-event > .mdl-card__title {
                          align-items: flex-start;
                        }
                        .demo-card-event > .mdl-card__title > h4 {
                          margin-top: 0;
                        }
                        .demo-card-event > .mdl-card__actions {
                          display: flex;
                          box-sizing:border-box;
                          align-items: center;
                        }
                        .demo-card-event > .mdl-card__actions > .material-icons {
                          padding-right: 10px;
                        }
                        .demo-card-event > .mdl-card__title,
                        .demo-card-event > .mdl-card__actions,
                        .demo-card-event > .mdl-card__actions > .mdl-button {
                          color: #fff;
                        }
                    </style>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                       <!-- carta escalamiento -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Escalamiento</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre y conozca los escalamientos respectivos a los procesos</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/escalamiento">
                            Conocer
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">trending_up</i>
                          </div>
                      </div>
                    <!-- carta escalamiento -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta usuario -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Usuario</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo usuario para darle acceso al sistema</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newUser">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">person_add</i>
                          </div>
                      </div>
                    <!-- carta usuario -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta añadir -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Proceso</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo proceso para la wiki</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newProcess">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">add_circle</i>
                          </div>
                      </div>
                    <!-- carta añadir -->
                    </center>
                  </div>
              </div>
          </div>
              </div>
          </div>
        </div>
        <!-- END AFTER CARD -->
        <!-- night card -->
        <div class="noche" style="display:none; width:100%" >
          <style>
            .demo-card-wide.mdl-card {
                width: 100%;
            }
            .demo-card-wide > #night {
                color: #fff;
                height: 250px;
                background: url('<?=URL?>img/noches.jpg') center / cover;
            }
            .demo-card-wide > .mdl-card__menu {
                color: #fff;
            }
          </style>
          <div class="demo-card-wide mdl-card mdl-shadow--16dp">
              <div class="mdl-card__title" id="night">
                <h2 class="mdl-card__title-text " style="font-size:40px;">Buenas Noches <?=$_SESSION["alias"]?></h2>
              </div>
              <div class="mdl-card__supporting-text">
                              <div class="mdl-card-supporting-text">
                <div class="mdl-grid">
                  <style>
                        .demo-card-event.mdl-card {
                          width: 256px;
                          height: 200px;
                          background:#3498DB;
                        }
                        .demo-card-event > .mdl-card__actions {
                          border-color:#3498DB;
                        }
                        .demo-card-event > .mdl-card__title {
                          align-items: flex-start;
                        }
                        .demo-card-event > .mdl-card__title > h4 {
                          margin-top: 0;
                        }
                        .demo-card-event > .mdl-card__actions {
                          display: flex;
                          box-sizing:border-box;
                          align-items: center;
                        }
                        .demo-card-event > .mdl-card__actions > .material-icons {
                          padding-right: 10px;
                        }
                        .demo-card-event > .mdl-card__title,
                        .demo-card-event > .mdl-card__actions,
                        .demo-card-event > .mdl-card__actions > .mdl-button {
                          color: #fff;
                        }
                    </style>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                       <!-- carta escalamiento -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Escalamiento</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre y conozca los escalamientos respectivos a los procesos</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/escalamiento">
                            Conocer
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">trending_up</i>
                          </div>
                      </div>
                    <!-- carta escalamiento -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta usuario -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Usuario</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo usuario para darle acceso al sistema</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newUser">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">person_add</i>
                          </div>
                      </div>
                    <!-- carta usuario -->
                    </center>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col">
                    <center><i class="material-icons">keyboard_arrow_down</i></center>
                    <center>
                      <!-- carta añadir -->
                      <div class="demo-card-event mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                          <h4>Nuevo Proceso</h4><br>
                        </div>
                        <div class="mdl-card__supporting-text">
                          <p style="color:white">Registre un nuevo proceso para la wiki</p>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                          <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/newProcess">
                            Registrar
                          </a>
                          <div class="mdl-layout-spacer"></div>
                            <i class="material-icons">add_circle</i>
                          </div>
                      </div>
                    <!-- carta añadir -->
                    </center>
                  </div>
              </div>
          </div>
              </div>
          </div>
        </div>
        <!-- end night card -->
         <!-- Wide card with share menu button -->
      <style>
      #cabecera-mensaje {
        width:100%;
        margin-top:20px;
        margin-bottom:20px;
      }
      .demo-card-wide > #mensaje {
        color: #fff;
        height: 250px;
        background: url('<?=URL?>img/banner-new.png') center / cover;
      }
      .demo-card-wide > .mdl-card__menu {
        color: #fff;
      }
      </style>
      <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="cabecera-mensaje">
        <div class="mdl-card__title" id="mensaje">
          <h2 class="mdl-card__title-text">¿Qué es MDO?</h2>
        </div>
        <div class="mdl-card__supporting-text">
          MDO de sus siglas (Manuales de Operación) es un sistema enfocado a la gestión de la información de manera que sea accedida por las personas nuevas dentro del área
          de facturación, conozca los procesos más importantes ya sean del PITNEY O FACTURACIÓN, gestione los escalamientos, registre y consulte procesos, conozca los turnos y permita 
          que la información fluya a través de los usuarios
        </div>
      </div>
      <!-- SLIDE TELEFONÍA MÓVIL -->
       <style>
      #cabecera-mensaje {
        width:100%;
        margin-top:20px;
        margin-bottom:20px;
      }
      .demo-card-wide > #fact {
        color: #fff;
        height: 70px;
        background-color:rgb(48,48,48);
      }
      .demo-card-wide > .mdl-card__menu {
        color: #fff;
      }
      </style>
      <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="cabecera-mensaje">
        <div class="mdl-card__title" id="fact">
          <h2 class="mdl-card__title-text">Diagrama del proceso de Facturación</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <img src="<?=URL?>img/fact-illustration.png" style="max-width:100%;">
        </div>
      </div>
      <!-- FIN SLIDE TELEFONÓA MÓVIL -->
       <!-- SLIDE ARCHIVOS BAT -->
       <style>
      #cabecera-mensaje {
        width:100%;
        margin-top:20px;
        margin-bottom:20px;
      }
      .demo-card-wide > #fact {
        color: #fff;
        height: 70px;
        background-color:rgb(48,48,48);
      }
      .demo-card-wide > .mdl-card__menu {
        color: #fff;
      }
      </style>
      <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="cabecera-mensaje">
        <div class="mdl-card__title" id="fact">
          <h2 class="mdl-card__title-text">Mapa mental Archivos .bat</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <center><img src="<?=URL?>img/BAT.png" style="max-width:70%;"></center>
        </div>
      </div>
      <!-- FIN SLIDE ARCHIVOS BAT -->
      <!-- Slide 2 -->
      <br>
      <!-- <iframe src="https://sena4-my.sharepoint.com/personal/jcallec_senaedu_edu_co/_layouts/15/Doc.aspx?sourcedoc={2cf1bca2-bf3a-4ad7-b529-24a2d565ac92}&amp;action=embedview&amp;wdAr=1.7777777777777777" width="962px" height="565px" frameborder="0">Esto es un documento de <a target="_blank" href="https://office.com">Microsoft Office</a> incrustado con tecnología de <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe> -->
      <br>
      <style>
      #cabecera-mensaje {
        width:100%;
        margin-top:20px;
        margin-bottom:20px;
      }
      .demo-card-wide > #telmov {
        color: #fff;
        height: 70px;
        background-color:rgb(48,48,48);
      }
      .demo-card-wide > .mdl-card__menu {
        color: #fff;
      }
      </style>
      <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="cabecera-mensaje">
        <div class="mdl-card__title" id="telmov">
          <h2 class="mdl-card__title-text">Telefonía Móvil</h2>
        </div>
        <div class="mdl-card__supporting-text">
                 <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                   <div class="mdl-tabs__tab-bar">
                      <a href="#tab1" class="mdl-tabs__tab is-active">Recepción</a>
                      <a href="#tab2" class="mdl-tabs__tab">Monitoreo</a>
                      <a href="#tab3" class="mdl-tabs__tab">Invoice-Billing</a>
                      <a href="#tab4" class="mdl-tabs__tab">Exlusiones</a>
                  </div>
                <div class="mdl-tabs__panel is-active" id="tab1">
                  <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col">
                      <br>
                      <p>Con la recepción, se extrae todos los datos provenientes de las antenas situadas en las distintas zonas y con ellas a su vez se transmite la información al  usuario, un centro de manejo de datos, se encarga de gestionar toda la información recibida y monitorearla para continuar con el proceso de facturación</p>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                      <center><img src="<?=URL?>img/Slide/antena.png"></center>
                    </div>
                  </div>
              </div>
              <div class="mdl-tabs__panel" id="tab2">
                <div class="mdl-grid">
                  <div class="mdl-cell mdl-cell--6-col">
                      <center><img src="<?=URL?>img/Slide/monitoreo.png"></center>
                  </div>
                  <div class="mdl-cell mdl-cell--6-col">
                    <br>
                    <p>En el monitoreo, lo que se busca es organizar la información que llega a traves de procesos que complementan la operación, también se busca mitigar los errores, puesto que durante el monitoreo u operación la información es manipulada  para que tenga distintas funciones como por ejemplo(correos, número de cuentas,etc).</p>
                  </div>
                </div>
              </div>
              <div class="mdl-tabs__panel" id="tab3">
                <div class="mdl-grid">
                  <div class="mdl-cell mdl-cell--6-col">
                    <p>Este procesa las cuentas ITEM y BILL, las pagadoras que son las cuentas padres y las cuentas hijas; este proceso es incompatible con los REL, Pipeline Retail Tasación arriba, ya que este tiene un alto consumo de recursos en la máquina, con otros billing, el ajuste de Ambiente para billing, Ajuste de ambiente para Invoice porque estos procesos utilizan las mismas instancias</p>
                  </div>
                  <div class="mdl-cell mdl-cell--6-col">
                    <center><img src="<?=URL?>img/invoice.png"></center>
                  </div>
                </div>
              </div>
              <div class="mdl-tabs__panel" id="tab4">
               <div class="mdl-grid">
                 <div class="mdl-cell mdl-cell--6-col">
                   <center><img src="<?=URL?>img/exclusiones.png"></center>
                 </div>
                 <div class="mdl-cell mdl-cell--6-col">
                   <p>Las exclusiones dentro del proceso, van a ser aquellos archivos finales que dan como producto la factura del cliente.</p>
                 </div>
               </div>
              </div>
          </div>
        </div>
      </div>
      <!-- End slide2 -->
      <div class="mdl-grid">
               <div class="mdl-cell mdl-cell--4-col">
          <style>
          .demo-card-square.mdl-card {
              width: 100%;
              height: 320px;
          }
          .demo-card-square > #procesos {
              color: #fff;
              background:url('<?=URL?>img/processs.png') bottom right 15% no-repeat #46B6AC;
              background-color:#0057A0;
          }
        </style>
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand" id="procesos">
              <h2 class="mdl-card__title-text">Procesos</h2>
            </div>
            <div class="mdl-card__supporting-text">
              Conozca los procesos más importantes de las distintas áreas
            </div>
            <div class="mdl-card__actions mdl-card--border">
              <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/procesos">
                  Ir allá
              </a>
            </div>
        </div>
        </div>
        <div class="mdl-cell mdl-cell---4-col">
        <style>
          .demo-card-square.mdl-card {
              width: 100%;
              height: 320px;
          }
          .demo-card-square > #servidores {
              color: #fff;
              background:url('<?=URL?>img/server.png') bottom right 15% no-repeat #46B6AC;
              background-color:#0057A0;
          }
        </style>
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand" id="servidores">
              <h2 class="mdl-card__title-text">Servidores PITNEY</h2>
            </div>
            <div class="mdl-card__supporting-text">
              Gestione los tiempos de ocupación en los servidores en tiempo real y los usuarios que estan en ellos
            </div>
            <div class="mdl-card__actions mdl-card--border">
              <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/servidores">
                  Ir allá
              </a>
            </div>
        </div>
        </div>
        <div class="mdl-cell mdl-cell--4-col">
          <style>
          .demo-card-square.mdl-card {
              width: 100%;
              height: 320px;
          }
          .demo-card-square > #turnos{
              color: #fff;
              background:url('<?=URL?>img/clock.png') bottom right 15% no-repeat #46B6AC;
              background-color:#0057A0;
          }

        </style>
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title mdl-card--expand" id="turnos">
              <h2 class="mdl-card__title-text">Turnos</h2>
            </div>
            <div class="mdl-card__supporting-text">
              Distinga los turnos y cada uno de los procesos relevantes de estos
            </div>
            <div class="mdl-card__actions mdl-card--border">
              <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="<?=URL?>Home/turnos">
                  Ir allá
              </a>
            </div>
        </div>
        </div>
      </div>
       <footer class="mdl-mini-footer" style="width:100%">
          <div class="mdl-mini-footer__left-section">
          <div class="mdl-logo">MDO &copy</div>
            <ul class="mdl-mini-footer__link-list">
              <li><a href="menestys-services.com">Menestys Services S.A.S</a></li>
              <li><a href="#">Acerca de</a></li>
            </ul>
          </div>
      </footer>
      </div>
      </div>
      </div>
    </div>
  </main>
</div>
<!-- MODAL -->
<dialog id="dialog" class="mdl-dialog mdl-shadow--12dp">
  <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:60px;"></center>
  <center><h3 class="mdl-dialog__title">Bienvenido a MDO</h3></center>
  <div class="mdl-dialog__content">
        <center>
          <p id="text-intro">
          Los manuales de operación son importantes durante la operación, así que simplificamos la información para que sea accedida de manera más ágil
          </p>
        </center>
    <br>
    <!-- step-1 -->
      <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="tuto1">
        <div class="mdl-card__title" style="background:url('<?=URL?>img/slide1.png')center/cover; height:175px">
          <h2 class="mdl-card__title-text" style="color:white">Manuales y procesos</h2>
        </div>
        <div class="mdl-card__supporting-text mdl-text-center">
          <p style="text-align:Center">Registre, edite, busque manuales respectivos a los procesos de las distintas áreas, ya sean Pitney o Facturación y también sus turnos</p>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto1"><i class="material-icons">keyboard_arrow_right</i></a></center>
        </div>
      </div>
    <!-- step-1 -->
    <!-- step-2 -->
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="tuto2" style="display:none">
        <div class="mdl-card__title" style="background:url('<?=URL?>img/slide2.png')center/cover; height:175px">
          <h2 class="mdl-card__title-text" style="color:white">Escalamiento</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <p style="text-align:center">Consulte en la matriz de escalamiento los procesos los cuales tienen algún soporte</p>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <div class="mdl-grid">
              <div class="mdl-cell mdl-cell--6-col">
                <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto2-prev"><i class="material-icons">keyboard_arrow_left</i></a></center>
              </div>
              <div class="mdl-cell mdl-cell--6-col">
                <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto2-rev"><i class="material-icons">keyboard_arrow_right</i></a></center>             
              </div>
          </div>
        </div>
      </div>
    <!-- step-2 -->
    <!-- step-3 -->
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="tuto3" style="display:none">
        <div class="mdl-card__title" style="background:url('<?=URL?>img/slide3.jpg')center/cover; height:175px">
          <h2 class="mdl-card__title-text" style="color:white">Usuarios</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <p style="text-align:center">Permita el acceso al sistema por medio de el registro de usuarios, nosotros nos encargamos del resto...</p>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <div class="mdl-grid">
              <div class="mdl-cell mdl-cell--6-col">
                <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto3-prev"><i class="material-icons">keyboard_arrow_left</i></a></center>
              </div>
              <div class="mdl-cell mdl-cell--6-col">
                <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto3-rev"><i class="material-icons">keyboard_arrow_right</i></a></center>             
              </div>
          </div>
        </div>
      </div>
    <!-- step-3 -->
     <!-- step-3 -->
     <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="tuto4" style="display:none">
        <div class="mdl-card__title" style="background:url('<?=URL?>img/slide4.png')center/cover; height:175px">
          <h2 class="mdl-card__title-text" style="color:white">Seguridad y comodidad</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <p style="text-align:center">Disfrute de una interfaz cómoda sobre material Design Lite de Google, a su vez de encriptación nativa del lenguaje para sus contraseñas </p>
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <div class="mdl-grid">
              <div class="mdl-cell mdl-cell--6-col">
                <center><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btntuto4-prev"><i class="material-icons">keyboard_arrow_left</i></a></center>
              </div>
              <div class="mdl-cell mdl-cell--6-col">
                <center><button type="button" class="mdl-button primary" onclick="terminarTutorial()">Finalizar</button></center>             
              </div>
          </div>
        </div>
      </div>
    <!-- step-3 -->
  </div>
  <!-- <div class="mdl-dialog__actions">
    <center><button type="button" class="mdl-button primary">Vale, entiendo</button></center>
  </div> -->
</dialog>
<!-- MODAL -->

<!-- SNACKBAR -->
<div id="demo-toast-example" class="mdl-js-snackbar mdl-snackbar">
  <div class="mdl-snackbar__text"></div>
  <button class="mdl-snackbar__action" type="button"></button>
</div>
<!-- SNACKBAR -->
    <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/scripts/modal.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <!-- Dialog -->
  <script src="<?=URL?>js/dialog/dialog-polyfill.js"></script>
  <!-- Toastr -->
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script>
    // FUNCION PARA CICLOS
    var fecha=new Date();
    var hora=fecha.getHours();
    function saberHora(){
      validar();
    }

    function validar(){
      if (hora<12) {
        $(".dia").show();
      }
      if(hora>=12 && hora<18){
        $(".tarde").show();
      }
      if(hora>=18 && hora<24){
        $(".noche").show();
      }
    }

    function tutorial(){
      var url="<?php echo URL ?>";
      $.ajax({
        method:"POST",
        url:url+"Home/consultartutorial",
        datatype:'json'
      })
      .done(function(rpta){
        var datos=$.parseJSON(rpta);
        if (datos.tutorial_usuario == "Activo") {
          modal();
        }else{

        }
      })
      .fail(function(error){
        alert(error);
      })
    }

    function terminarTutorial(){
      var url="<?php echo URL ?>";
      $.ajax({
        method:"POST",
        url:url+"Home/terminarTutorial",
        datatype:'json'
      })
      .done(function(rpta){
        if (rpta!=null) {
          snakbar();
        }else{
          toastr.error("No se pudo completar la operación");
        }
      })
      .fail(function(error){
        alert(error);
      })
    }

    function snakbar(){
      var snackbarContainer = document.querySelector('#demo-toast-example');
      var data = {message: 'Tutorial Finalizado'};
      snackbarContainer.MaterialSnackbar.showSnackbar(data);
  }
  </script>
</body>
</html>