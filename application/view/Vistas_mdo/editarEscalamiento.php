<?php
if (!isset($_SESSION["id_usu"])) {
    header("location: " . URL . "home");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Editar</title>
  <link rel="short icon" href="<?=URL?>img/manual.png">
  <!-- Material css y otros -->
  <!-- Material css y otros -->
    <link rel="stylesheet" href="<?=URL?>css/material.min.css">
    <link rel="stylesheet" href="<?=URL?>css/Nativos.css">
    <link rel="stylesheet" href="<?=URL?>css/pace.css">
    <link rel="stylesheet" href="<?=URL?>css/modal.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Google fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?=URL?>libs/Datatables/datatables.css">
    <link rel="stylesheet" href="<?=URL?>libs/toastrjs/build/toastr.min.css">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
</head>
<style>
  body,h1,h2,h3,h4,h5,h6,a,p,.mdl-layout-title{
    font-family: 'Questrial', sans-serif;
  }
</style>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Editar escalamiento</span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
      <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="<?=URL?>Home/iniciar" id="inicio"><i class="material-icons">home</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="inicio">
        Inicio
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newUser" id="usuarios"><i class="material-icons">how_to_reg</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="usuarios">
        Nuevo Usuario
      </div>
        <a class="mdl-navigation__link" href="<?=URL?>Home/newProcess" id="proceso"><i class="material-icons">insert_comment</i></a>
          <div class="mdl-tooltip mdl-tooltip--large" for="proceso">
        Nuevo Proceso
      </div>
    <a href="" class="mdl-navigation__link"> <img src="<?=URL?>img/logo2.png" alt="" style="max-width:150px;"></a>
      </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <center><img src="<?=URL?>img/LOGO-MDO.png" style="max-width:70px;"></center>
    <center>
      <span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
      <img class="mdl-chip__contact mdl-color--indigo" src="https://image.flaticon.com/icons/svg/417/417777.svg"></img>
      <span class="mdl-chip__text"><?=$_SESSION["nombre"]?></span>
      <a href="<?=URL?>Login/cerrarSesion" class="mdl-chip__action"><i class="material-icons">keyboard_backspace</i></a>
  </span>
  <hr>
    </center>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="<?=URL?>Home/procesos">Procesos</a>
      <a class="mdl-navigation__link" href="<?=URL?>Home/turnos">Turnos</a>
      <?php
$encriptacion = base64_encode($_SESSION["id_usu"])
?>
      <a class="mdl-navigation__link" href="<?=URL?>Home/config/<?=$encriptacion?>">Configuración</a>
    </nav>
  </div>
  <main class="mdl-layout__content">
      <div class="page-content">
        <!-- GRID -->
          <form action="<?=URL?>Home/modificarEscalamiento" method="post">
            <div class="mdl-grid">
              <div class="mdl-cell mdl-cell--6-col">
                <div class="mdl-card mdl-shadow--2dp" style="width:100%">
                  <div class="mdl-card__supporting-text">
                    <center><h4>Editar Primer Contacto</h4></center>
                    <input type="hidden" name="txtcodescalamiento" value="<?=$datos->cod_escalamiento?>">
                    <label>Área</label><br>
                    <select name="txtarea" id="selectarea" style="width:100%">
                      <option value="<?=$datos->area_encargada?>"><?=$datos->area_encargada?> (actual)</option>
                      <option value="BASE DE DATOS">BASE DE DATOS</option>
                      <option value="BILLING DISPONIBLE">BILLING DISPONIBLE</option>
                      <option value="BRM">BRM</option>
                      <option value="CARTERA">CARTERA</option>
                      <option value="DWH">DWH</option>
                      <option value="FACTURACIÓN Y COBRANZA">FACTURACIÓN Y COBRANZA</option>
                      <option value="LOGÍSTICA Y OPERACIÓN">LOGÍSTICA Y OPERACIÓN</option>
                      <option value="NOC">NOC</option>
                      <option value="NOC crear incidente">NOC crear incidente</option>
                      <option value="PITNEY DISPONIBLE">PITNEY DISPONIBLE</option>
                      <option value="PROYECTOS FACTURACIÓN">PROYECTOS FACTURACIÓN</option>
                      <option value="SOPORTE BRM">SOPORTE BRM</option>
                    </select><br><br>
                    <label>Proceso</label><br>
                    <select name="txtproceso" id="selectprocesos" style="width:100%">
                        <option value="<?=$proceso->idProceso?>"><?=$proceso->nombre_proceso?> (actual)</option>
                        <?php foreach($listarProcesos as $value): ?>
                            <option value="<?=$value->idProceso?>"><?=$value->nombre_proceso?></option>
                        <?php endforeach; ?>
                    </select><br>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                        <input class="mdl-textfield__input" type="text" id="sample3" name="txtprimercontacto" value="<?=$datos->primer_contacto?>">
                        <label class="mdl-textfield__label" for="sample3">Nombre y Apellidos del primer contacto</label>
                    </div><br>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                        <input class="mdl-textfield__input" type="email" id="sample3" name="txtprimercorreo" value="<?=$datos->email_primer_contacto?>">
                        <label class="mdl-textfield__label" for="sample3">Correo primer contacto</label>
                    </div><br>
                     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                        <input class="mdl-textfield__input" type="text" id="sample3" name="txtprimertelefono" value="<?=$datos->telefono_primer_contacto?>">
                        <label class="mdl-textfield__label" for="sample3">Teléfono primer contacto</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mdl-cell mdl-cell--6-col">
                <div class="mdl-card mdl-shadow--2dp" style="width:100%">
                  <div class="mdl-card__supporting-text">
                    <center><h4>Editar Segundo Contacto</h4></center>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                      <input class="mdl-textfield__input" type="text" id="sample3" name="txtsegundocontacto" value="<?=$datos->segundo_contacto?>">
                      <label class="mdl-textfield__label" for="sample3">Nombre y Apellidos del primer contacto</label>
                    </div>
                    <br>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                      <input class="mdl-textfield__input" type="email" id="sample3" name="txtsegundocorreo" value="<?=$datos->email_segundo_contacto?>">
                      <label class="mdl-textfield__label" for="sample3">Correo segundo contacto</label>
                    </div>
                    <br>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                        <input class="mdl-textfield__input" type="text" id="sample3" name="txtsegundotelefono" value="<?=$datos->telefono_segundo_contacto?>">
                        <label class="mdl-textfield__label" for="sample3">Teléfono segundo contacto</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <center><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">Guardar</button></center>
          </form>
        <!-- GRID -->
      </div>
    </main>
  </div>
</form>
   <!-- Jquery -->
  <script src="<?=URL?>/js/jquery.js"></script>
  <script src="<?=URL?>/js/pace.js"></script>
  <script src="<?=URL?>/js/modal.js"></script>
  <!-- Material js -->
  <script src="<?=URL?>/js/material.js"></script>
  <script src="<?=URL?>libs/toastrjs/build/toastr.min.js"></script>
  <script src="<?=URL?>libs/Datatables/datatables.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script>
      $("#selectarea").select2();
      $("#selectprocesos").select2();

      $("#activarSegundaPersona").click(function(){
      $("#form-segundo-contacto").toggle();
      })


       $(document).ready( function () {
      $('#tablaescalonamiento').DataTable();
      } );
  </script>
<?php
if (isset($_SESSION['mensaje'])) {
    echo $_SESSION['mensaje'];
    $_SESSION['mensaje'] = null;
}
?>
</body>
</html>

