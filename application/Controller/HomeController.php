<?php
/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Model\mdo;

class HomeController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        // require APP . 'view/Vistas_mdo/Login.php';
        require APP . 'view/Vistas_mdo/Login.php';
    }

    public function iniciar()
    {
        // load views
        $mdo              = new mdo();
        $procesosContados = $mdo->contarProcesos();
        $tutorial=$mdo->consultarToturial();
        require APP . 'view/Vistas_mdo/Index.php';
    }

    public function editProceso($cod_proceso){
        $mdo = new mdo();
        $mdo->__SET("id_proceso", $cod_proceso);
        $proceso         = $mdo->detalleProceso();
        $articulos       = $mdo->listarArticulosProceso();
        $detalleArticulo = $mdo->listarDetalleArticulo();
        require APP . 'view/Vistas_mdo/Manuales/editarProceso.php';
    }

    public function consultartutorial(){
        $mdo=new mdo();
        $mdo->__SET("id_usuario",$_SESSION["id_usu"]);
        $tutorial=$mdo->consultarToturial();
        echo json_encode($tutorial);
        // require APP . 'view/Vistas_mdo/Index.php';
    }

    public function terminarTutorial(){
        $mdo= new mdo();
        $mdo->__SET("id_usuario",$_SESSION["id_usu"]);
        $resultado=$mdo->FinalizarTuto();
    }


    // =========================================================================
    // FUNCIÓN QUE LISTA EL DETALLE DEL ARTÍICULO PARA AJAX
    // =========================================================================
    public function listarDetalleArticulo(){
        $cod_pr=$_POST["op"];
        $mdo= new mdo();
        $mdo->__SET("id_articulo",$cod_pr);
        $rtm=$mdo->detalleArticulo();
        echo json_encode($rtm);
    }

    // =========================================================================
    // FUNCIÓN QUE ACTUALIZA EL ENCABEZADO , ES DECIR TÍTULO E INTRO DEL PROCESO
    // =========================================================================
    public function actualizarEncabezadoProceso(){
        $mdo= new mdo();
        $mdo->__SET("nombre_proceso",$_POST["nombreP"]);
        $mdo->__SET("descripcion_proceso",$_POST["descripcionP"]);
        $mdo->__SET("id_proceso",$_POST["codP"]);
        $resultado=$mdo->actualizarEncabezadoProceso();
        // echo json_encode($rtm);
    }

    public function actualizarArticulo(){
        $mdo=new mdo();
        $mdo->__SET("nombre_articulo",$_POST["nombreA"]);
        $mdo->__SET("descripcion_articulo",$_POST["descripcionA"]);
        $mdo->__SET("id_articulo",$_POST["codA"]);
        $respuesta=$mdo->actualizarArticulo();
        var_dump($respuesta);
    }


    public function actualizarImagenArticulo(){
        $mdo=new mdo();
        $carpeta       = ROOT . "public" . DIRECTORY_SEPARATOR . "galeria//";
        $nombre_subido = basename($_FILES['imagen']['name']);
        $Subida        = move_uploaded_file($_FILES['imagen']['tmp_name'], $carpeta . $nombre_subido);
        $mdo->__SET("nombre_imagen",$nombre_subido);
        $mdo->__SET("id_imagen",$_POST["cod_img"]);
        $resultado=$mdo->actualizarImagenArticulo();  
        if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se actualizo la imagen')</script>";
            header("location: " . URL . "Home/editProceso/".$_POST["cod_p"]);
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/editProceso/".$_POST["cod_p"]);
        }
    }

    public function eliminarImagenArticulo(){
        $mdo=new mdo();
        $cod_imagen=$_POST["codI"];
        $mdo->__SET("id_imagen",$cod_imagen);
        $resultado=$mdo->eliminarImagenArticulo();
    }

    public function procesos()
    {
        $mdo             = new mdo();
        $procesos        = $mdo->listarProcesos();
        $procesos_pitney = $mdo->listarProcesosPitney();
        $semanales       = $mdo->listarProcesosSemanales();
        $mensuales       = $mdo->listarProcesosMensuales();
        $diarios         = $mdo->listarProcesosDiarios();
            require APP . 'view/Vistas_mdo/Manuales/procesos.php';

    }

    public function consultarUsuariosOnline(){
        $mdo=new mdo();
        $rpta=$mdo->consultarUsuariosOnline();
        echo json_encode($rpta);
    }

    public function servidores(){
        $mdo=new mdo();
        $servidores=$mdo->listarServidores();
        $UsuariosOnline=$mdo->contarUsuariosOline()->total;
        require APP . 'view/Vistas_mdo/servidores.php';
    }

    public function registrarServidor(){
        $mdo=new mdo();
        $mdo->__SET("nombre_servidor",$_POST["servidor"]);
        $mdo->__SET("nota_servidor",$_POST["nota"]);
        $mdo->__SET("id_usuario",$_SESSION["id_usu"]);
        $resultado=$mdo->registrarServidor();
    }

    public function ocuparServidor(){
        $mdo=new mdo();
        $mdo->__SET("id_usuario",$_SESSION["id_usu"]);
        $mdo->__SET("cod_servidor",$_POST["servidor"]);
        $rpta=$mdo->ocuparServidor();
    }

    public function desocuparServidor(){
        $mdo=new mdo();
        $mdo->__SET("cod_servidor",$_POST["servidor"]);
        $resultado=$mdo->desocuparServidor();
    }

    public function MantenimientoOn(){
        $mdo=new mdo();
        $mdo->__SET("cod_servidor",$_POST["servidor"]);
        $resultado=$mdo->MantenimientoOn();
    }

    public function MantenimientoOf(){
        $mdo=new mdo();
        $mdo->__SET("cod_servidor",$_POST["servidor"]);
        $resultado=$mdo->MantenimientoOf();
    }

    public function consultarServidor(){
        $mdo=new mdo();
        $rtm=$mdo->consultarServidor();
        echo json_encode($rtm);
    }

    public function escalamiento(){
    	$mdo=new mdo();
    	$listarProcesos=$mdo->listarProcesos();
    	$listaEscalamiento=$mdo->listarEscalamiento();
        require APP . 'view/Vistas_mdo/escalamiento.php';
    }

    public function config($usuario)
    {
        $mdo                  = new mdo();
        $usuarioDesencriptado = base64_decode($usuario);
        $mdo->__SET("id_usuario", $usuarioDesencriptado);
        $resultado = $mdo->listarDatosUsuario();
        require APP . 'view/Vistas_mdo/configuraciones.php';
    }

    public function about(){
        require APP . 'view/Vistas_mdo/about.php';
    }

    public function turnos()
    {
        $mdo     = new mdo();
        $morning = $mdo->listarTurnoManana();
        $after   = $mdo->listarTurnoTarde();
        $night   = $mdo->listarTurnoNoche();
        $ambps   = $mdo->listarAmbosHorarios();
        require APP . 'view/Vistas_mdo/Manuales/turnos.php';
    }

    public function newUser()
    {
        $mdo=new mdo();
        $roles=$mdo->consultarRoles();
        $usuarios=$mdo->listarUsuarios();
        require APP . 'view/Vistas_mdo/nuevoUsuario.php';
    }

    public function bloquearUsuario(){
        $mdo=new mdo();
        $mdo->__SET("id_usuario",$_POST["usuario"]);
        $rpta=$mdo->bloquearUsuario();
    }
    
    public function ActivarUsuario(){
        $mdo=new mdo();
        $mdo->__SET("id_usuario",$_POST["usuario"]);
        $rpta=$mdo->ActivarUsuario();
    }

    public function eliminarArticulo(){
        $mdo=new mdo();
        $mdo->__SET("id_articulo",$_POST["txtcodarticulo"]);
        $resultado=$mdo->eliminarArticulo();
    }

    public function editarEscalamiento($cod_escalamiento){
    	$mdo=new mdo();
    	$mdo->__SET("cod_escalamiento",$cod_escalamiento);
    	$datos=$mdo->editarEscalamiento();
    	$area=$mdo->listarAreaEscalamiento();
    	$listarProcesos=$mdo->listarProcesos();
    	$proceso=$mdo->listarProcesoEscalamiento();
        require APP . 'view/Vistas_mdo/editarEscalamiento.php';
    }

    public function modificarEscalamiento(){
    	$mdo= new mdo();
    	$mdo->__SET("area_encargada",$_POST["txtarea"]);
    	$mdo->__SET("id_proceso",$_POST["txtproceso"]);
    	$mdo->__SET("primer_contacto",$_POST["txtprimercontacto"]);
    	$mdo->__SET("email_primer_contacto",$_POST["txtprimercorreo"]);
    	$mdo->__SET("telefono_primer_contacto",$_POST["txtprimertelefono"]);
    	$mdo->__SET("segundo_contacto",$_POST["txtsegundocontacto"]);
    	$mdo->__SET("email_segundo_contacto",$_POST["txtsegundocorreo"]);
    	$mdo->__SET("telefono_segundo_contacto",$_POST["txtsegundotelefono"]);
    	$mdo->__SET("cod_escalamiento",$_POST["txtcodescalamiento"]);
    	$resultado=$mdo->modificarEscalonamiento();
    	if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se actualizo el registro')</script>";
            header("location: " . URL . "Home/escalamiento");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/escalamiento");
        }
    }

    public function registrarEscalonamiento(){
    	$mdo=new mdo();
    	$mdo->__SET("area_encargada",$_POST["txtarea"]);
    	$mdo->__SET("primer_contacto",$_POST["txtprimercontacto"]);
    	$mdo->__SET("email_primer_contacto",$_POST["txtprimercorreo"]);
    	$mdo->__SET("telefono_primer_contacto",$_POST["txtprimertelefono"]);
    	$mdo->__SET("segundo_contacto",$_POST["txtsegundocontacto"]);
    	$mdo->__SET("email_segundo_contacto",$_POST["txtsegundocorreo"]);
    	$mdo->__SET("telefono_segundo_contacto",$_POST["txtsegundotelefono"]);
    	$mdo->__SET("id_proceso",$_POST["txtproceso"]);
    	$resultado=$mdo->registrarEscalonamiento();	
    	if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Registro Satisfactorio')</script>";
            header("location: " . URL . "Home/escalamiento");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/escalamiento");
        }
    }

    public function eliminarEscalamiento($cod_escalamiento){
    	$mdo=new mdo();
    	$mdo->__SET("cod_escalamiento",$cod_escalamiento);
    	$resultado=$mdo->eliminarEscalamiento();
    	if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Registro Eliminado')</script>";
            header("location: " . URL . "Home/escalamiento");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/escalamiento");
        }
    }

    public function newProcess()
    {
        $mdo    = new mdo();
        $turnos = $mdo->listarTurnos();
        $tipo   = $mdo->listarTipoProceso();
        require APP . 'view/Vistas_mdo/nuevoProceso.php';
    }

    public function nuevosuario()
    {
        $mdo = new mdo();
        $mdo->__SET("nombre_usuario", $_POST["txtnombre"]);
        $mdo->__SET("apellido_usuario", $_POST["txtapellido"]);
        $mdo->__SET("correo_usuario", $_POST["txtcorreo"]);
        $mdo->__SET("cod_seguridad", $_POST["txtcod"]);
        $mdo->__SET("cod_rol", $_POST["txtrol"]);

        $resultado = $mdo->registrarUsuario();

        if ($resultado) {
            $titulo  = "INGRESO MDOSOFT";
            $cuerpo  = '
				<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Mail</title>
                    </head>
                    <body style="background-color:#BDC3C7">
                        <center><img src="https://i.postimg.cc/NFcRvShr/LOGO-_MDO.png" style="max-width:70px"></center>
                        <center>
                                <div class="polaroid" style="width:250px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); text-align:center; background-color:white">
                                    <img src="https://i.postimg.cc/1tDdG0tP/detail_google_adwords_image8.png" style="width:100%">
                                    <div class="container" style="padding:10px; background-color:white">
                                        <p>Hola, le damos la bienvenida a MDOSOFT, un sistema en la nube hecho para la documentacion de los manuales en los procesos de facturacion y PITNEY, utilice el siguiente codigo para ingresar:  <strong>'.$_POST["txtcod"].'</strong> y vaya  el siguiente link <a href="https://mdosoft.000webhostapp.com">MDOSOFT</a></p>
                                    </div>
                                    </div><br><br>
                        </center>
                    </body>
                </html>
            ';
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            if (mail($_POST['txtcorreo'], $titulo, $cuerpo, $headers)) {
                $_SESSION["mensaje"] = "<script>toastr.success('Se ha registrado el usuario')</script>";
                header("location: " . URL . "Home/newUser");
            } else {
                $_SESSION["mensaje"] = "<script>toastr.error('No se ha podido hacer la solicitud')</script>";
                header("location: " . URL . "Home/newUser");
            }
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/newUser");
        }
    }

    public function cambiarPass()
    {
        $mdo       = new mdo();
        $txtContra = base64_encode($_POST["txtpass"]);
        $mdo->__SET("passusu", $txtContra);
        $mdo->__SET("cod_seguridad", $_POST["txtcod"]);
        $mdo->__SET("id_usuario", $_POST["cod_usuario"]);
        $encriptacion = base64_encode($_POST["cod_usuario"]);
        $resultado    = $mdo->cambiarContra();
        if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se cambió la contraseña')</script>";
            header("location: " . URL . "Home/config/" . $encriptacion . "");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/config/" . $encriptacion . "");
        }
    }

    public function actualizarInfoUsuario()
    {
        $mdo = new mdo();
        $mdo->__SET("nombre_usuario", $_POST["txtnombre"]);
        $mdo->__SET("apellido_usuario", $_POST["txtapellido"]);
        $mdo->__SET("correo_usuario", $_POST["txtcorreo"]);
        $mdo->__SET("id_usuario", $_POST["txtpersona"]);
        $encriptacion = base64_encode($_POST["txtpersona"]);
        $resultado    = $mdo->actualizarInfoUsuario();
        if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se guardaron los cambios')</script>";
            header("location: " . URL . "Home/config/" . $encriptacion . "");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/config/" . $encriptacion . "");
        }

    }

    // Métodos para turno
    // ------------------------------------------------------------------------------------------------------
    public function cargarTurno($idturno)
    {
        $mdo = new mdo();
        $mdo->__SET("id_turno", $idturno);
        $turno = $mdo->detalleTurno();
        require APP . 'view/Vistas_mdo/Manuales/turno.php';
    }
    // ----------------------------------------------------------------------------------------------------

    // Métodos para procesos
    // ----------------------------------------------------------------------------------------------------
    public function cargarProceso($id_proceso)
    {
        $mdo = new mdo();
        $mdo->__SET("id_proceso", $id_proceso);
        $proceso         = $mdo->detalleProceso();
        $articulos       = $mdo->listarArticulosProceso();
        $detalleArticulo = $mdo->listarDetalleArticulo();
        require APP . 'view/Vistas_mdo/Manuales/proceso.php';
    }

    public function descartarProceso($estado, $proceso)
    {
        $mdo = new mdo();
        $mdo->__SET("estado_proceso", $estado);
        $mdo->__SET("id_proceso", $proceso);
        $resultado = $mdo->cambiarEstadoProceso();
        if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se actualizó el estado')</script>";
            header("location: " . URL . "Home/procesos");
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/procesos");
        }

    }

    public function editarProceso()
    {
        $mdo = new mdo();
        $mdo->__SET("descripcion_proceso", $_POST["descripcion_proceso"]);
        $mdo->__SET("id_proceso", $_POST["id_proceso"]);
        $resultado = $mdo->editarProceso();
        if ($resultado) {
            $_SESSION["mensaje"] = "<script>toastr.success('Se guardaron los cambios')</script>";
            header("location: " . URL . "Home/cargarProceso/" . $_POST["id_proceso"]);
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('Hubo un error en la operación')</script>";
            header("location: " . URL . "Home/cargarProceso/" . $_POST["id_proceso"]);
        }
    }

    public function registrarProcess()
    {
        $mdo = new mdo();
        $mdo->__SET("nombre_proceso", $_POST["txtnombreProceso"]);
        $mdo->__SET("descripcion_proceso", $_POST["txtdescripcionProceso"]);
        $mdo->__SET("id_turno", $_POST["turnoProceso"]);
        $mdo->__SET("tipo_proceso", $_POST["tipoProceso"]);
        $proceso = $mdo->registrarProceso();
        if ($proceso) {
            $cantidad = count($_FILES["imagen"]["tmp_name"]);
            // var_dump($cantidad);
            for ($i = 0; $i < $cantidad; $i++) {
                $carpeta       = ROOT . "public" . DIRECTORY_SEPARATOR . "galeria//";
                $nombre_subido = basename($_FILES['imagen']['name'][$i]);
                $Subida        = move_uploaded_file($_FILES['imagen']['tmp_name'][$i], $carpeta . $nombre_subido);
                $mdo->__SET("nombre_imagen", $_FILES['imagen']['name'][$i]);
                $imagen        = $mdo->registrarImagen();
                $ultimaImagen  = $mdo->obtenerultimaImagen()->ultimaImagen;
                $ultimoProceso = $mdo->obtenerultimoProceso()->ultimoProceso;
                $mdo->__SET("nombre_articulo", $_POST["txtnombreArticulo"][$i]);
                $mdo->__SET("descripcion_articulo", $_POST["txtdescripcionArticulo"][$i]);
                $mdo->__SET("id_proceso", $ultimoProceso);
                $mdo->__SET("id_imagen", $ultimaImagen);
                $articulos = $mdo->registrarArticulo();
            }
        }
        $_SESSION["mensaje"] = "<script>toastr.success('Se registró el proceso')</script>";
        header("location: " . URL . "Home/procesos");

    }


    public function registrarArticulo(){
        $mdo= new mdo();
        $carpeta       = ROOT . "public" . DIRECTORY_SEPARATOR . "galeria//";
        $nombre_subido = basename($_FILES['imagennueva']['name']);
        $Subida        = move_uploaded_file($_FILES['imagennueva']['tmp_name'], $carpeta . $nombre_subido);
        $mdo->__SET("nombre_imagen",$nombre_subido);
        $respuesta=$mdo->registrarImagen();
        if ($respuesta) {
            $ultimaImagen  = $mdo->obtenerultimaImagen()->ultimaImagen;
            $mdo->__SET("nombre_articulo",$_POST["txtnuevotituloproceso"]);
            $mdo->__SET("descripcion_articulo",$_POST["txtdesxcripcionarticulonuevo"]);
            $mdo->__SET("id_proceso",$_POST["txtcodproceso"]);
            $mdo->__SET("id_imagen",$ultimaImagen);
            $resultado=$mdo->registrarArticulo();
            if ($resultado) {
                $_SESSION["mensaje"] = "<script>toastr.success('Registro exitoso')</script>";
                header("location: " . URL . "Home/editProceso/".$_POST["txtcodproceso"]);
            }else{
                $_SESSION["mensaje"] = "<script>toastr.error('Error al registrar')</script>";
                header("location: " . URL . "Home/editProceso/".$_POST["txtcodproceso"]);
            }
        }
    }
    // public function registrarProcess()
    // {
    //     $mdo = new mdo();
    //     $mdo->__SET("nombre_proceso", $_POST["txtnombreProceso"]);
    //     $mdo->__SET("descripcion_proceso", $_POST["txtdescripcionProceso"]);
    //     $mdo->__SET("id_turno", $_POST["turnoProceso"]);
    //     $mdo->__SET("tipo_proceso", $_POST["tipoProceso"]);
    //     $proceso = $mdo->registrarProceso();
    //     if ($proceso) {
    //         $cantidad = count($_FILES["imagen"]["tmp_name"]);
    //         $imagen;
    //         var_dump($cantidad);
    //         for ($i = 0; $i < $cantidad; $i++) {
    //             $carpeta       = ROOT . "public" . DIRECTORY_SEPARATOR . "galeria//";
    //             $nombre_subido = basename($_FILES['imagen']['name'][$i]);
    //             $Subida        = move_uploaded_file($_FILES['imagen']['tmp_name'][$i], $carpeta . $nombre_subido);
    //             $mdo->__SET("nombre_imagen", $_FILES['imagen']['name'][$i]);
    //             $imagen        = $mdo->registrarImagen();
    //             $ultimaImagen  = $mdo->obtenerultimaImagen()->ultimaImagen;
    //             $ultimoProceso = $mdo->obtenerultimoProceso()->ultimoProceso;
    //             $mdo->__SET("nombre_articulo", $_POST["txtnombreArticulo"][$i]);
    //             $mdo->__SET("descripcion_articulo", $_POST["txtdescripcionArticulo"][$i]);
    //             $mdo->__SET("id_proceso", $ultimoProceso);
    //             $mdo->__SET("id_imagen", $ultimaImagen);
    //             $articulos = $mdo->registrarArticulo();
    //             if (headers_sent()) {
    //                 header("location: " . URL . "Home/newProcess");
    //                 $_SESSION["mensaje"] = "<script>toastr.success('Se registró el proceso')</script>";
    //             }
    //         }
    //     }

    // }
    // -------------------------------------------------------------------------------------------------------
    // Métodos para ciclos
    // -----------------------------------------------------------------------------------------------------
    public function cargarCiclo()
    {
        // $mdo=new mdo();
        // $procesos=$mdo->listarProcesos();
        // require APP . 'view/Vistas_mdo/newciclo.php';
        require APP . 'view/Vistas_mdo/wrk.php';
    }

// public function registrarCiclo(){
    //    $mdo=new mdo();
    //    $mdo->__SET("nombre_ciclo",$_POST["txtnombreciclo"]);
    //    $mdo->__SET("descripcion_ciclo",$_POST["txtdescripcion"]);
    //    $mdo->__SET("id_usuario",$_POST["usuario"]);
    //    var_dump($_POST["cod_proceso"]);
    //    exit;
    //    for ($i=0; $i < count($_POST["cod_proceso"]); $i++) {
    //        $mdo->__SET("id_proceso", $_POST["cod_proceso"][$i]);
    //        $resultado=$mdo->registrarCiclo();
    //        var_dump($resultado);
    //        exit;
    //    }

// }
}
