<?php

/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Model\Login;

class LoginController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function validarL()
    {
        $login = new login();
        $login->__SET("correo_usuario", addslashes($_POST["txtcorreo"]));
        $clave = base64_encode(addslashes($_POST["txtpass"]));
        $login->__SET("pass_usuario", $clave);
        $login->__SET("cod_seguridad", addslashes($_POST["txtpass"]));
        // // recaptcha
        // $claveS         = "6LfrdmMUAAAAAFfbeAvDIt0I1iZGWM9eldGkPlt6";
        // $captcha        = $_POST["g-recaptcha-response"];
        // $url            = "https://www.google.com/recaptcha/api/siteverify";
        // $respuestaFinal = file_get_contents($url . "?secret=" . $claveS . "&response=" . $captcha);
        // $jsonrstp       = json_decode($respuestaFinal);
        // Se valida si $jsonrstp->success
        // $bloqueo=$login->validarBloqueo();
        $resultado = $login->validar();
        if ($resultado) {
        	 #// session_start();
        		if ($resultado->estado_usuario=="Bloqueado") {
        			$_SESSION["mensaje"] = "<script>toastr.error('Usuario Bloqueado, notifíquese para averiguar el por qué')</script>";
                	header("location: " . URL . "Home");
        		}else{
        			$_SESSION['id_usu'] = $resultado->idusuario;
                	$_SESSION['nombre'] = $resultado->nombre_usuario . " " . $resultado->apellido_usuario;
                	$_SESSION["alias"]  = $resultado->nombre_usuario;
                	$_SESSION["rol"]=$resultado->rol_cod_rol;
                	$persona=$resultado->idusuario;
                	$login->__SET("cod_usuario",$persona);
                	$rpta=$login->online();
                	header("location: " . URL . "Home/iniciar");
        		}
               
        }else{
        	 $_SESSION["mensaje"] = "<script>toastr.error('Contraseña o correo incorrecto')</script>";
                header("location: " . URL . "Home");
        }
    }

    public function cerrarSesion()
    {
        $login=new Login();
        $login->__SET("cod_usuario",$_SESSION['id_usu']);
        $rpta=$login->outline();
        session_destroy();
        header("location: " . URL . "Home");
    }

    public function recupass()
    {
        $login = new Login();
        $login->__SET("correo_usuario", $_POST["txtcorreo"]);
        $resultado = $login->recuperarPass();
        if ($resultado) {
            //para el envío en formato HTML
            //dirección del remitente
            // $headers .= "From: ".$_POST['nombre']." <".$_POST['emisor'].">\r\n";
            //Direcciones que recibián copia
            //$headers .= "Cc: ejemplo2@gmail.com\r\n";
            //direcciones que recibirán copia oculta
            //$headers .= "Bcc: ejemplo3@yahoo.com\r\n";
            $cod_seguridad = $login->recuperarPass()->cod_seguridad;
            $titulo        = "CONTRASEÑA MDOSOFT";
            $cuerpo        = '
                <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Mail</title>
                    </head>
                    <body style="background-color:#BDC3C7">
                        <center><img src="https://i.postimg.cc/NFcRvShr/LOGO-_MDO.png" style="max-width:70px"></center>
                        <center>
                                <div class="polaroid" style="width:250px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); text-align:center; background-color:white">
                                    <img src="https://i.stack.imgur.com/jP2Mc.jpg" alt="Norway" style="width:100%">
                                    <div class="container" style="padding:10px; background-color:white">
                                        <p>¡Hola!, usted ha solicitado recuperar su contraseña para MDO, utilice el siguiente código para ingresar:  <strong>'.$cod_seguridad.'</strong></p>
                                    </div>
                                    </div><br><br>
                        </center>
                    </body>
                </html>
            ';
            $headers       = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            if (mail($_POST['txtcorreo'], $titulo, $cuerpo, $headers)) {
                $_SESSION["mensaje"] = "<script>toastr.success('Se ha enviado la contraseña al correo ingresado')</script>";
                header("location: " . URL . "Home");
            } else {
                $_SESSION["mensaje"] = "<script>toastr.error('No se ha podido hacer la solicitud')</script>";
                header("location: " . URL . "Home");
            }
        } else {
            $_SESSION["mensaje"] = "<script>toastr.error('El correo no se ha encontrado')</script>";
            header("location: " . URL . "Home");
        }
    }
}
