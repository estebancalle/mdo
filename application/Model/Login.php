<?php 
namespace Mini\Model;
use Mini\Core\Model;


class login extends Model{
 // usuario
 private $correo_usuario;
 private $pass_usuario;
 private $cod_seguridad;
 private $cod_usuario;

 function __SET($Atributo, $Valor){
    $this->$Atributo=$Valor;
}

function __GET($Atributo){
    return $this->$Atributo;
}

public function validar(){
    $sql="SELECT * FROM usuario WHERE correo_usuario=? AND pass_usuario=? OR cod_seguridad=? AND estado_usuario!='Bloqueado'";
    $stm=$this->db->prepare($sql);
    $stm->bindParam(1, $this->correo_usuario);
    $stm->bindParam(2, $this->pass_usuario);
    $stm->bindParam(3, $this->cod_seguridad);
    $stm->execute();
    return $stm->fetch();
}

public function validarBloqueo(){
    $sql="SELECT * FROM usuario WHERE correo_usuario=? AND pass_usuario=? OR cod_seguridad=? AND  estado_usuario='Bloqueado'";
    $stm=$this->db->prepare($sql);
    $stm->bindParam(1, $this->correo_usuario);
    $stm->bindParam(2, $this->pass_usuario);
    $stm->bindParam(3, $this->cod_seguridad);
    $stm->execute();
    return $stm->fetch();
}

public function recuperarPass(){
	$sql="SELECT correo_usuario,cod_seguridad FROM usuario WHERE correo_usuario=?";
	$stm=$this->db->prepare($sql);
    $stm->bindParam(1, $this->correo_usuario);
    $stm->execute();
    return $stm->fetch();
}

public function online(){
    $sql="UPDATE usuario SET estado_usuario='Online' WHERE idusuario=?";
    $stm=$this->db->prepare($sql);
    $stm->bindParam(1,$this->cod_usuario);
    return $stm->execute();
}

public function outline(){
    $sql="UPDATE usuario SET estado_usuario='Activo' WHERE idusuario=?";
    $stm=$this->db->prepare($sql);
    $stm->bindParam(1,$this->cod_usuario);
    return $stm->execute();
}
  
}

?>