<?php
namespace Mini\Model;

use Mini\Core\Model;

class mdo extends Model
{

    // =========================================================================
    // ATRIBUTOS
    // =========================================================================

    // Atributos de proceso
    private $id_proceso;
    private $nombre_proceso;
    private $descripcion_proceso;
    private $tipo_proceso;
    private $estado_proceso;

    // Servidor
    private $nombre_servidor;
    private $cod_servidor;
    private $nota_servidor;
    private $estado_servidor;
    
// Turno
    private $id_turno;
    private $nombre_turno;

    // ciclo
    private $nombre_ciclo;
    private $descripcion_ciclo;

    // Usuario
    private $id_usuario;
    private $passusu;
    private $nombre_usuario;
    private $apellido_usuario;
    private $correo_usuario;
    private $cod_seguridad;
    private $tutorial;
    private $cod_rol;

    // Artículo
    private $id_articulo;
    private $nombre_articulo;
    private $descripcion_articulo;
    private $id_imagen;
    private $nombre_imagen;

    // escalamiento
    private $cod_escalamiento;
    private $area_encargada;
    private $primer_contacto;
    private $email_primer_contacto;
    private $telefono_primer_contacto;
    private $segundo_contacto;
    private $email_segundo_contacto;
    private $telefono_segundo_contacto;

    // Imagen


    // =========================================================================
    // ENCAPSULACIÓN
    // =========================================================================
    public function __SET($Atributo, $Valor)
    {
        $this->$Atributo = $Valor;
    }

    public function __GET($Atributo)
    {
        return $this->$Atributo;
    }

    // =========================================================================
    // FUNCIÓN PARA CAMBIAR LA CONTRASEÑA
    // =========================================================================
    public function cambiarContra()
    {
        $sql = "UPDATE usuario SET pass_usuario=?,cod_seguridad=? WHERE idusuario=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->passusu);
        $stm->bindParam(2, $this->cod_seguridad);
        $stm->bindParam(3, $this->id_usuario);
        return $stm->execute();
    }

    // =========================================================================
    // FNCIÓN QUE CONSULTA EL ESTADO DEL TUTORIAL DEL USUARIO
    // =========================================================================
    public function consultarToturial(){
        $sql="SELECT * FROM usuario WHERE idusuario=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_usuario);
        $stm->execute();
        return $stm->fetch();
    }

    // =========================================================================
    // FUNCIÓN QUE DA POR FINALIZADO EL TUTORIAL
    // =========================================================================
    public function FinalizarTuto(){
        $sql="UPDATE usuario SET tutorial_usuario='Finalizado' WHERE idusuario=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_usuario);
        return $stm->execute();

    }

    // =========================================================================
    // FUNCIÓN PARA ACTUALIZAR LA INFORMACIÓN DEL USUARIO
    // =========================================================================
    public function actualizarInfoUsuario()
    {
        $sql = "UPDATE usuario SET nombre_usuario=?,apellido_usuario=?,correo_usuario=? WHERE idusuario=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->nombre_usuario);
        $stm->bindParam(2, $this->apellido_usuario);
        $stm->bindParam(3, $this->correo_usuario);
        $stm->bindParam(4, $this->id_usuario);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN PARA REGISTRAR UN USUARIO
    // =========================================================================
    public function registrarUsuario()
    {
        $sql = "INSERT INTO usuario (nombre_usuario,apellido_usuario,correo_usuario,cod_seguridad,rol_cod_rol) VALUES (?,?,?,?,?)";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->nombre_usuario);
        $stm->bindParam(2, $this->apellido_usuario);
        $stm->bindParam(3, $this->correo_usuario);
        $stm->bindParam(4, $this->cod_seguridad);
        $stm->bindParam(5, $this->cod_rol);
        return $stm->execute();
    }

    public function consultarRoles(){
        $sql="SELECT * FROM rol";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LA INFORMACIÓN DEPENDIENDO AL USUARIO LOGUEADO
    // =========================================================================
    public function listarDatosUsuario()
    {
        $sql = "SELECT * FROM usuario where idusuario=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_usuario);
        $stm->execute();
        return $stm->fetch();
    }

// ESCALAMIENTO
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    
    // =========================================================================
    // FUNCIÓN PARA REGISTRAR UN ESCALAMIENTO
    // =========================================================================
    public function registrarEscalonamiento(){
        $sql="INSERT INTO escalamiento (area_encargada,primer_contacto,email_primer_contacto,telefono_primer_contacto,segundo_contacto,
        email_segundo_contacto,telefono_segundo_contacto,proceso_idProceso) VALUES (?,?,?,?,?,?,?,?)";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->area_encargada);
        $stm->bindParam(2,$this->primer_contacto);
        $stm->bindParam(3,$this->email_primer_contacto);
        $stm->bindParam(4,$this->telefono_primer_contacto);
        $stm->bindParam(5,$this->segundo_contacto);
        $stm->bindParam(6,$this->email_segundo_contacto);
        $stm->bindParam(7,$this->telefono_segundo_contacto);
        $stm->bindParam(8,$this->id_proceso);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN PARA MODIFICAR EL ESCALAMIENTO
    // =========================================================================
    public function modificarEscalonamiento(){
        $sql="UPDATE escalamiento SET area_encargada=?, proceso_idProceso=?, primer_contacto=?, email_primer_contacto=?, telefono_primer_contacto=?
        , segundo_contacto=?, email_segundo_contacto=?, telefono_segundo_contacto=? WHERE cod_escalamiento=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->area_encargada);
        $stm->bindParam(2,$this->id_proceso);
        $stm->bindParam(3,$this->primer_contacto);
        $stm->bindParam(4,$this->email_primer_contacto);
        $stm->bindParam(5,$this->telefono_primer_contacto);
        $stm->bindParam(6,$this->segundo_contacto);
        $stm->bindParam(7,$this->email_segundo_contacto);
        $stm->bindParam(8,$this->telefono_segundo_contacto);
        $stm->bindParam(9,$this->cod_escalamiento);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN PARA ELIMINAR EL ESCALAMIENTO
    // =========================================================================
    public function eliminarEscalamiento(){
        $sql="DELETE FROM escalamiento WHERE cod_escalamiento=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_escalamiento);
        return $stm->execute();
    }

    // =========================================================================
    // LISTAR EL ESCALAMIENTO
    // =========================================================================
    public function listarEscalamiento(){
        $sql="SELECT * FROM escalamiento as e INNER JOIN proceso as p ON e.proceso_idProceso=p.idProceso";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // LISTAR EL ÁREA DEL ESCALAMIENTO
    // =========================================================================
    public function listarAreaEscalamiento(){
        $sql="SELECT area_encargada FROM escalamiento WHERE cod_escalamiento=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_escalamiento);
        $stm->execute();
        return $stm->fetch();
    }

    // =========================================================================
    // LISTAR PROCESOS PARA REGISTRO DEL ESCALAMIENTO
    // =========================================================================
    public function listarProcesoEscalamiento(){
        $sql="SELECT * FROM proceso as p INNER JOIN escalamiento as e ON p.idProceso=e.proceso_idProceso WHERE e.cod_escalamiento=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_escalamiento);
        $stm->execute();
        return $stm->fetch();
    }

    // =========================================================================
    // EDITAR EL ESCALAMIENTO
    // =========================================================================
    public function editarEscalamiento(){
        $sql="SELECT * FROM escalamiento WHERE cod_escalamiento=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_escalamiento);
        $stm->execute();
        return $stm->fetch();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------
// TURNOS
    // ----------------------------------------------------------------------------------------------------
    
    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DE LA NOCHE
    // =========================================================================
    public function listarTurnoNoche()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE t.nombre_turno='Noche' AND estado_proceso='Activo'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DE LA TARDE 
    // =========================================================================
    public function listarTurnoTarde()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE t.nombre_turno='Tarde'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DE LA MAÑANA
    // =========================================================================
    public function listarTurnoManana()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE t.nombre_turno='Mañana' AND estado_proceso='Activo'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTA LOS PROCESOS CON HORARIO DE AMBOS
    // =========================================================================
    public function listarAmbosHorarios()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE t.nombre_turno='Ambos Horarios'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTAR EL DETALLE DEL TURNO
    // =========================================================================
    public function detalleTurno()
    {
        $sql = "SELECT  * FROM tipo_proceso as tp INNER JOIN proceso as p ON tp.idtipo_proceso=p.tipo_proceso_idtipo_proceso INNER JOIN turno as t ON p.turno_idturno=t.idturno WHERE p.turno_idturno=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_turno);
        $stm->execute();
        return $stm->fetchAll();
    }

// public function listarNombreTurnoDetalle(){
    //     $sql="SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno WHERE t.idturno=? ";
    //     $stm=$this->db->prepare($sql);
    //     $stm->bindParam(1, $this->id_turno);
    //     $stm->execute();
    //     return $stm->fetch();
    // }
    // -------------------------------------------------------------------------------------------------------

// PROCESOS
    // ------------------------------------------------------------------------------------------------
    
    // =========================================================================
    // FUNCIÓN QUE LISTA LOS TURNOS DE FORMA GENERAL
    // =========================================================================
    public function listarTurnos()
    {
        $sql = "SELECT * FROM turno";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // LISTAR PROCESOS CON SUS CARACTERISTICAS
    // =========================================================================
    public function listarProcesos()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso ORDER BY idProceso DESC";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DE PITNEY
    // =========================================================================
    public function listarProcesosPitney()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE tp.nombre_tipo_proceso='Pitney'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DE LA SEMANA
    // =========================================================================
    public function listarProcesosSemanales()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE tp.nombre_tipo_proceso='Semanal'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS MENSUALES
    // =========================================================================
    public function listarProcesosMensuales()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE tp.nombre_tipo_proceso='Mensual'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR LOS PROCESOS DIARIOS
    // =========================================================================
    public function listarProcesosDiarios()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE tp.nombre_tipo_proceso='Diario'";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA CAMBIAR EL ESTADO DEL PROCESO SELECCIONADO
    // =========================================================================
    public function cambiarEstadoProceso()
    {
        $sql = "UPDATE proceso SET estado_proceso=? WHERE idProceso=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->estado_proceso);
        $stm->bindParam(2, $this->id_proceso);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN PARA MOSTRAR EL DETALLE DEL PROCESO, ES DECIR LA INFORMACIÓN DE EL
    // =========================================================================
    public function detalleProceso()
    {
        $sql = "SELECT * FROM turno as t INNER JOIN proceso as p ON t.idturno=p.turno_idturno INNER JOIN tipo_proceso as tp ON p.tipo_proceso_idtipo_proceso=tp.idtipo_proceso WHERE p.idProceso=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_proceso);
        $stm->execute();
        return $stm->fetch();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTA LOS ARTÍCULOS DEL PROCESO SELECCIONADO
    // =========================================================================
    public function listarArticulosProceso()
    {
        $sql = "SELECT * FROM articulo WHERE proceso_idProceso=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_proceso);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN PARA LISTAR EL DETALLE DE EL ARTÍCULO
    // =========================================================================
    public function listarDetalleArticulo()
    {
        $sql = "SELECT * FROM imagen as i INNER JOIN articulo as a ON i.idimagen=a.imagen_idimagen WHERE a.proceso_idProceso=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_proceso);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTA EL DETALLE DEL ARTÍCULO PARA AJAX
    // =========================================================================
    public function detalleArticulo(){
        $sql = "SELECT * FROM imagen as i INNER JOIN articulo as a ON i.idimagen=a.imagen_idimagen WHERE a.idarticulo=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->id_articulo);
        $stm->execute();
        return $stm->fetch();
    }
    

    // =========================================================================
    // FUNCIÓN QUE PERMITE EDITAR EL PROCESO
    // =========================================================================
    public function editarProceso()
    {
        $sql = "UPDATE proceso SET descripcion_proceso=? WHERE idProceso=?";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->descripcion_proceso);
        $stm->bindParam(2, $this->id_proceso);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTA LOS PROCESOS REGISTRADOS DE FORMA GENERAL
    // =========================================================================
    public function contarProcesos()
    {
        $sql = "SELECT COUNT(idProceso) as procesos FROM proceso";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetch();
    }

    // =========================================================================
    // FUNCIÓN QUE LISTA EL TIPO DE PROCESO
    // =========================================================================
    public function listarTipoProceso()
    {
        $sql = "SELECT * FROM tipo_proceso";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    // =========================================================================
    // FUNCIÓN QUE PERMITE REGISTRAR EL PROCESO
    // =========================================================================
    public function registrarProceso()
    {
        $sql = "INSERT INTO proceso (nombre_proceso,intro_proceso,turno_idturno,tipo_proceso_idtipo_proceso) VALUES (?,?,?,?)";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->nombre_proceso);
        $stm->bindParam(2, $this->descripcion_proceso);
        $stm->bindParam(3, $this->id_turno);
        $stm->bindParam(4, $this->tipo_proceso);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN QUE REGISTRA EL ARTÍCULO
    // =========================================================================
    public function registrarArticulo()
    {
        $sql = "INSERT INTO articulo (nombre_articulo,descripcion_articulo,proceso_idProceso,imagen_idimagen) VALUES (?,?,?,?)";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->nombre_articulo);
        $stm->bindParam(2, $this->descripcion_articulo);
        $stm->bindParam(3, $this->id_proceso);
        $stm->bindParam(4, $this->id_imagen);
        return $stm->execute();
    }
    

    // =========================================================================
    // FUNCIÓN QUE ACTUALIZA EL ENCABEZADO DEL PROCESO
    // =========================================================================
    public function actualizarEncabezadoProceso(){
        $sql="UPDATE proceso SET nombre_proceso=?, intro_proceso=? WHERE idProceso=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->nombre_proceso);
        $stm->bindParam(2,$this->descripcion_proceso);
        $stm->bindParam(3,$this->id_proceso);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN QUE ACTUALIZA LA IMAGEN DEL ARTICULO
    // =========================================================================
    public function actualizarImagenArticulo(){
        $sql="UPDATE imagen SET nombre_imagen=? WHERE idimagen=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->nombre_imagen);
        $stm->bindParam(2,$this->id_imagen);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN QUE "ELIMINA" LA IMAGEN DEL ARTICULO
    // =========================================================================
    public function eliminarImagenArticulo(){
        $sql="UPDATE imagen SET nombre_imagen='' WHERE idimagen=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_imagen);
        return $stm->execute();
    }

    public function eliminarArticulo(){
        $sql="DELETE FROM articulo WHERE idarticulo=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1, $this->id_articulo);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN PARA ACTUALIZAR EL ARTÍCULO
    // =========================================================================
    public function actualizarArticulo(){
        $sql="UPDATE articulo SET nombre_articulo=?, descripcion_articulo=? WHERE idarticulo=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->nombre_articulo);
        $stm->bindParam(2,$this->descripcion_articulo);
        $stm->bindParam(3,$this->id_articulo);
        return $stm->execute();
    }
    // =========================================================================
    // FUNCIÓN QUE PERMITE REGISTRAR LA IMAGEN
    // =========================================================================
    public function registrarImagen()
    {
        $sql = "INSERT INTO imagen (nombre_imagen) VALUE (?)";
        $stm = $this->db->prepare($sql);
        $stm->bindParam(1, $this->nombre_imagen);
        return $stm->execute();
    }

    // =========================================================================
    // FUNCIÓN QUE OBTIENE EL ÚLTIMO PROCESO REGISTRADO 
    // =========================================================================
    public function obtenerultimoProceso()
    {
        $sql = "SELECT MAX(idProceso) as ultimoProceso FROM proceso";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetch();

    }

    // =========================================================================
    // FUNCIÓN QUE OBTIENE LA ÚLTIMA IMAGEN
    // =========================================================================
    public function obtenerultimaImagen()
    {
        $sql = "SELECT MAX(idimagen) as ultimaImagen FROM imagen";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetch();

    }
// ----------------------------------------------------------------------------------------------------

// SERVIDORES
    public function listarServidores(){
        $sql="SELECT * FROM servidor as s INNER JOIN usuario as u ON s.usuario_idusuario=u.idusuario";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(); 
    }

    public function registrarServidor(){
        $sql="INSERT INTO servidor (nombre_servidor,nota_servidor,usuario_idusuario) VALUES (?,?,?)";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->nombre_servidor);
        $stm->bindParam(2,$this->nota_servidor);
        $stm->bindParam(3,$this->id_usuario);
        return $stm->execute();
    }


    public function ocuparServidor(){
        $sql="UPDATE servidor SET estado_servidor='Ocupado',  usuario_idusuario=? WHERE cod_servidor=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_usuario);
        $stm->bindParam(2,$this->cod_servidor);
        return $stm->execute();
    }

    public function desocuparServidor(){
        $sql="UPDATE servidor SET estado_servidor='Activo', usuario_idusuario=1 WHERE cod_servidor=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_servidor);
        return $stm->execute();
    }

    public function MantenimientoOn(){
        $sql="UPDATE servidor SET estado_servidor='Mantenimiento', usuario_idusuario=1 WHERE cod_servidor=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_servidor);
        return $stm->execute();
    }

    public function MantenimientoOf(){
        $sql="UPDATE servidor SET estado_servidor='Activo', usuario_idusuario=1 WHERE cod_servidor=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->cod_servidor);
        return $stm->execute();
    }

    public function consultarServidor(){
        $sql="SELECT * FROM servidor GROUP BY estado_servidor";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    public function consultarUsuariosOnline(){
        $sql="SELECT * FROM usuario WHERE estado_usuario='Online'";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    public function contarUsuariosOline(){
        $sql="SELECT COUNT(idusuario) as total FROM usuario WHERE estado_usuario='Online'";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetch();
    }

    public function listarUsuarios(){
        $sql="SELECT  * FROM rol as r INNER JOIN usuario as u ON r.cod_rol=u.rol_cod_rol";
        $stm=$this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    public function bloquearUsuario(){
        $sql="UPDATE usuario SET estado_usuario='Bloqueado' WHERE idusuario=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_usuario);
        return $stm->execute();
    }

    public function ActivarUsuario(){
        $sql="UPDATE usuario SET estado_usuario='Activo' WHERE idusuario=?";
        $stm=$this->db->prepare($sql);
        $stm->bindParam(1,$this->id_usuario);
        return $stm->execute();
    }
}
